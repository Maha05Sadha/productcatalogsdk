package com.absyz.kotlin.viewmodel

data class Score(
    val name:String,
    val score: Int,
)