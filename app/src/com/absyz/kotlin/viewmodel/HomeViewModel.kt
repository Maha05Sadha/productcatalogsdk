package com.checkin.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.absyz.kotlin.fragments.AccountFragment
import com.absyz.kotlin.fragments.LandingScreenFragment
import com.absyz.kotlin.fragments.SalesTargetFragment
import com.absyz.kotlin.model.RequestModel.*
import com.absyz.kotlin.model.ResponseModel.*
import com.absyz.kotlin.model.RequestModel.SearchItemRequest
import com.checkin.network.ApiResponse
import com.checkin.network.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    private val disposables = CompositeDisposable()

    lateinit var homeRepository: Repository

    companion object {
        const val TAG = "CommonViewModel"
    }

    /*User Detail API*/
    private val getUserdetailResponse = MutableLiveData<ApiResponse<*>>()
    private val getProductsearchResponse = MutableLiveData<ApiResponse<*>>()

    fun searchproductresponse(): MutableLiveData<ApiResponse<*>>? {
        return getProductsearchResponse
    }

    /*Account Search API*/
    private val getAccountsearchResponse = MutableLiveData<ApiResponse<*>>()

    fun searchAccountresponse(): MutableLiveData<ApiResponse<*>>? {
        return getAccountsearchResponse
    }

    fun SearchAccountAPI(
        context: Context,
        itemsearch: AccountSearchRequest,
        auth_token: String,
    ) {
        Log.e(TAG, "SearchAccountAPI()")
        try {
            repository.getAccountSearch(auth_token,itemsearch)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getAccountsearchResponse.setValue(ApiResponse.loading<CustomerValidationResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getAccountsearchResponse.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getAccountsearchResponse.setValue(
                                    ApiResponse.error<SearchItemRequest>(
                                        throwable
                                    )
                                )
                            }
                        ))}
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    fun getuserdetailsResponse(): MutableLiveData<ApiResponse<*>>? {
        return getUserdetailResponse
    }

    fun UserDetails(
        context: Context,
        auth_token: String,
    ) {
        Log.e(TAG, "customerValidation()")
        try {
            repository.getUserdetails(auth_token)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getUserdetailResponse.setValue(ApiResponse.loading<UserDetailResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getUserdetailResponse.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getUserdetailResponse.setValue(
                                    ApiResponse.error<UserDetailResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    /*Current Visit List API*/
    private val getCurrentVisitData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getCurrentVisitResponse(): MutableLiveData<ApiResponse<*>>{
        return getCurrentVisitData
    }

    fun SendCurrentVisitListData(
        postData: CurrentVisitListRequest,
        auth_token: String) {
        Log.e(TAG, "SendCurrentVisitListData()")
        try {
            repository.getCurrentVisitListDetails(auth_token,postData)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getCurrentVisitData.setValue(ApiResponse.loading<SalesTargetResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getCurrentVisitData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getCurrentVisitData.setValue(
                                    ApiResponse.error<SalesTargetResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    /*Visitor Creation API*/
    private val getvisitorCreationData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getVisitorCreationResponse(): MutableLiveData<ApiResponse<*>>{
        return getvisitorCreationData
    }

    fun SendVisitorCreationListData(
        postData: VisitorCreationRequest,
        auth_token: String) {
        Log.e(TAG, "SendVisitorCreationListData()")
        try {
            repository.getVisitorCreationDetails(auth_token,postData)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getvisitorCreationData.setValue(ApiResponse.loading<SalesTargetResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getvisitorCreationData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getvisitorCreationData.setValue(
                                    ApiResponse.error<SalesTargetResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    /*Visitor Creation API*/
    private val getcustomerCheckInData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getcustomerCheckInResponse(): MutableLiveData<ApiResponse<*>>{
        return getcustomerCheckInData
    }

    fun SendcustomerCheckInData(
        postData: CustomerCheckInRequest,
        auth_token: String) {
        Log.e(TAG, "SendcustomerCheckInData()")
        try {
            repository.getCustomerCheciInDetails(auth_token,postData)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getcustomerCheckInData.setValue(ApiResponse.loading<SalesTargetResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getcustomerCheckInData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getcustomerCheckInData.setValue(
                                    ApiResponse.error<SalesTargetResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    /*Recent Customer API*/
    private val getRecentCustomerResponse = MutableLiveData<ApiResponse<*>>()

    fun getRecentCustomerResponse(): MutableLiveData<ApiResponse<*>>? {
        return getRecentCustomerResponse
    }

    fun recentCustomerDetails(
        auth_token: String,
    ) {
        Log.e(TAG, "recentCustomerDetails()")
        try {
            repository.getRecentCustomerdetails(auth_token)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getRecentCustomerResponse.setValue(ApiResponse.loading<UserDetailResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getRecentCustomerResponse.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getRecentCustomerResponse.setValue(
                                    ApiResponse.error<UserDetailResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    /*Sales Target API*/
    private val getSalesTargetLiveData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getSalesTargetResponse(): MutableLiveData<ApiResponse<*>>{
        return getSalesTargetLiveData
    }

    fun SendSalesTargetData(
        context: SalesTargetFragment,
        postData: SalesTargetRequestModel,
        auth_token: String,
        pagenumer: String
    ) {
        Log.e(TAG, "SendSalesTargetData()")
        try {
            repository.getSalesTargetDetails(auth_token,pagenumer,postData)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getSalesTargetLiveData.setValue(ApiResponse.loading<SalesTargetResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getSalesTargetLiveData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getSalesTargetLiveData.setValue(
                                    ApiResponse.error<SalesTargetResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    /*Landing Screen Api*/
    private val getLandingLiveData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getLandingResponse(): MutableLiveData<ApiResponse<*>>{
        return getLandingLiveData
    }

    fun LandingData(
        context: LandingScreenFragment,
        auth_token: String
    ) {
        Log.e(TAG, "LandingData()")
        try {
            repository.getLandingDetails(auth_token)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getLandingLiveData.setValue(ApiResponse.loading<LandingMainResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getLandingLiveData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getLandingLiveData.setValue(
                                    ApiResponse.error<LandingMainResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*LandingScreen Product Category Api*/
    private val getProductCategoryDetailsData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getProductCategoryDetailsResponse(): MutableLiveData<ApiResponse<*>>{
        return getProductCategoryDetailsData
    }

    fun ProductCategoryDetailsData(
        productDetailsReqBody: ProductCategoryDetailsRequestModel,
        auth_token: String
    ) {
        Log.e(TAG, "ProductCategoryDetailsDataData()")
        try {
            repository.getProductCategoryDetails(auth_token,productDetailsReqBody)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getProductCategoryDetailsData.setValue(ApiResponse.loading<LandingMainResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getProductCategoryDetailsData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getProductCategoryDetailsData.setValue(
                                    ApiResponse.error<LandingMainResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*LandingScreen Brand Category Api*/
    private val getBrandCategoryDetailsData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getBrandCategoryDetailsResponse(): MutableLiveData<ApiResponse<*>>{
        return getBrandCategoryDetailsData
    }

    fun BrandCategoryDetailsData(
        brandDetailsReqBody: BrandProductsRequest,
        auth_token: String
    ) {
        Log.e(TAG, "BrandCategoryDetailsData()")
        try {
            repository.getBrandCategoryDetails(auth_token,brandDetailsReqBody)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getBrandCategoryDetailsData.setValue(ApiResponse.loading<LandingMainResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getBrandCategoryDetailsData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getBrandCategoryDetailsData.setValue(
                                    ApiResponse.error<LandingMainResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*Instock Products Api*/
    private val getInstockProductsData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getInstockProductDetailsResponse(): MutableLiveData<ApiResponse<*>>{
        return getInstockProductsData
    }

    fun InstockProductDetailsData(
        instockreqBody: InstockRequestModel,
        auth_token: String
    ) {
        Log.e(TAG, "InstockProductDetailsData()")
        try {
            repository.getInstockProductDetails(auth_token,instockreqBody)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getInstockProductsData.setValue(ApiResponse.loading<LandingMainResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getInstockProductsData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getInstockProductsData.setValue(
                                    ApiResponse.error<LandingMainResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*Filter Details Api*/
    private val getFilterData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getFilterDetailsResponse(): MutableLiveData<ApiResponse<*>>{
        return getFilterData
    }

    fun FilterDetailsData(
        filterreqBody: FilterRequestModel,
        auth_token: String
    ) {
        Log.e(TAG, "FilterDetailsData()")
        try {
            repository.getFilterDetails(auth_token,filterreqBody)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getFilterData.setValue(ApiResponse.loading<LandingMainResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getFilterData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getFilterData.setValue(
                                    ApiResponse.error<LandingMainResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*Filtered Products Api*/
    private val getFilterproductData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getFilteredProductsResponse(): MutableLiveData<ApiResponse<*>>{
        return getFilterproductData
    }

    fun FilteredProductsData(
        filterdprodReq: FilterProductListRequest,
        auth_token: String
    ) {
        Log.e(TAG, "FilteredProductsData()")
        try {
            repository.getFiltedProducts(auth_token,filterdprodReq)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getFilterproductData.setValue(ApiResponse.loading<FilterProductListResponse>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getFilterproductData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getFilterproductData.setValue(
                                    ApiResponse.error<LandingMainResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*LandingScreen ProductView Callout Api*/
    private val getProductViewCAlloutData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getProductViewCAlloutResponse(): MutableLiveData<ApiResponse<*>>{
        return getProductViewCAlloutData
    }

    fun ProductViewCalloutData(
        productviewcalloutBody: ProductViewCallOutRequest,
        auth_token: String
    ) {
        Log.e(TAG, "ProductViewCalloutData()")
        try {
            repository.getProductViewCallOut(auth_token,productviewcalloutBody)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getProductViewCAlloutData.setValue(ApiResponse.loading<LandingMainResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getProductViewCAlloutData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getProductViewCAlloutData.setValue(
                                    ApiResponse.error<LandingMainResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*Colors View Details Api*/
    private val getProductColorDetailsData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getProductColorDetailsResponse(): MutableLiveData<ApiResponse<*>>{
        return getProductColorDetailsData
    }

    fun ProductColorDetailsData(
        productcolorReqBody: ProductColorViewRequest,
        auth_token: String
    ) {
        Log.e(TAG, "ProductColorDetailsData()")
        try {
            repository.getProductColorDetails(auth_token,productcolorReqBody)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getProductColorDetailsData.setValue(ApiResponse.loading<LandingMainResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getProductColorDetailsData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getProductColorDetailsData.setValue(
                                    ApiResponse.error<LandingMainResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*Wishlist creation Api*/
    private val getWishlistCreationDetailsData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getWishlistCreationDetailsResponse(): MutableLiveData<ApiResponse<*>>{
        return getWishlistCreationDetailsData
    }

    fun WishlistCreationDetailsData(
        wishlistReqBody: WishlistCreationRequest,
        auth_token: String
    ) {
        Log.e(TAG, "WishlistCreationDetailsData()")
        try {
            repository.getWishlistCreationDetails(auth_token,wishlistReqBody)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getWishlistCreationDetailsData.setValue(ApiResponse.loading<WishlistCreationResponse>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getWishlistCreationDetailsData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getWishlistCreationDetailsData.setValue(
                                    ApiResponse.error<WishlistCreationResponse>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*Location View Details Api*/
    private val getProductLocationDetailsData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getProductLocationDetailsResponse(): MutableLiveData<ApiResponse<*>>{
        return getProductLocationDetailsData
    }

    fun ProductLocationDetailsData(
        productcolorReqBody: ProductColorViewRequest,
        auth_token: String
    ) {
        Log.e(TAG, "ProductLocationDetailsData()")
        try {
            repository.getProductLocationDetails(auth_token,productcolorReqBody)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getProductLocationDetailsData.setValue(ApiResponse.loading<LandingMainResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getProductLocationDetailsData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getProductLocationDetailsData.setValue(
                                    ApiResponse.error<LandingMainResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }


    /*Add to Cart Api*/
    private val getAddtocartData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getAddtoCartResponse(): MutableLiveData<ApiResponse<*>>{
        return getAddtocartData
    }

    fun AddtoCartData(
        addtocartReq: AddtoCartRequest,
        auth_token: String
    ) {
        Log.e(TAG, "AddtoCartData()")
        try {
            repository.getAddtoCartDetails(auth_token,addtocartReq)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe {

                    getAddtocartData.setValue(ApiResponse.loading<AddtoCartResponse>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getAddtocartData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getAddtocartData.setValue(
                                    ApiResponse.error<AddtoCartResponse>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*Add to Compare Api*/
    private val getAddtocompareData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getAddtoCompareResponse(): MutableLiveData<ApiResponse<*>>{
        return getAddtocompareData
    }

    fun AddtoComapreData(
        addtocomapreReq: AddtoCompareRequest,
        auth_token: String
    ) {
        Log.e(TAG, "AddtoComapreData()")
        try {
            repository.getAddtoCompareDetails(auth_token,addtocomapreReq)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe {

                    getAddtocompareData.setValue(ApiResponse.loading<AddtoCompareResponse>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getAddtocompareData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getAddtocompareData.setValue(
                                    ApiResponse.error<AddtoCompareResponse>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*Remove Comparison Api*/
    private val getRemovecompareData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getRemoveCompareResponse(): MutableLiveData<ApiResponse<*>>{
        return getRemovecompareData
    }

    fun RemoveComapreData(
        removeReq: RemoveComparisonRequest,
        auth_token: String
    ) {
        Log.e(TAG, "RemoveComapreData()")
        try {
            repository.getRemoveCompareDetails(auth_token,removeReq)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe {

                    getRemovecompareData.setValue(ApiResponse.loading<AddtoCompareResponse>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getRemovecompareData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getRemovecompareData.setValue(
                                    ApiResponse.error<AddtoCompareResponse>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*Remove Wishlist Api*/
    private val getRemovewishlistData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getRemoveWishlistResponse(): MutableLiveData<ApiResponse<*>>{
        return getRemovewishlistData
    }

    fun RemoveWishlistData(
        removeReq: RemoveWishlistRequest,
        auth_token: String
    ) {
        Log.e(TAG, "RemoveWishlistData()")
        try {
            repository.getRemoveWishlistDetails(auth_token,removeReq)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe {

                    getRemovewishlistData.setValue(ApiResponse.loading<WishlistRemoveResponse>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getRemovewishlistData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getRemovewishlistData.setValue(
                                    ApiResponse.error<WishlistRemoveResponse>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*Increase Qty Api*/
    private val getIncreaseQtyData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getIncreaseQtyResponse(): MutableLiveData<ApiResponse<*>>{
        return getIncreaseQtyData
    }

    fun IncreaseQtyData(
        removeReq: IncreaseProductQtyRequest,
        auth_token: String
    ) {
        Log.e(TAG, "IncreaseQtyData()")
        try {
            repository.getIncreaseQtyDetails(auth_token,removeReq)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe {

                    getIncreaseQtyData.setValue(ApiResponse.loading<WishlistRemoveResponse>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getIncreaseQtyData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getIncreaseQtyData.setValue(
                                    ApiResponse.error<WishlistRemoveResponse>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*In Active Customer Api*/
    private val getInactiveData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getInActiveCustomerResponse(): MutableLiveData<ApiResponse<*>>{
        return getInactiveData
    }

    fun InactiveCustomerData(
        removeReq: InActiveCustomerRequest,
        auth_token: String
    ) {
        Log.e(TAG, "InactiveCustomerData()")
        try {
            repository.getInActiveCustomerDetails(auth_token,removeReq)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe {

                    getInactiveData.setValue(ApiResponse.loading<AddtoCompareResponse>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getInactiveData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getInactiveData.setValue(
                                    ApiResponse.error<AddtoCompareResponse>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*Cart Products API*/
    private val getCartProductDetailsResponseData = MutableLiveData<ApiResponse<*>>()

    fun getCartProductDetailsResponse(): MutableLiveData<ApiResponse<*>>? {
        return getCartProductDetailsResponseData
    }

    fun CartProductDetails(
        cartprodReq: CartProductRequest,
        auth_token: String,
    ) {
        Log.e(TAG, "CartProductDetails()")
        try {
            repository.getCartProductDetails(auth_token,cartprodReq)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getCartProductDetailsResponseData.setValue(ApiResponse.loading<CustomerValidationResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getCartProductDetailsResponseData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getCartProductDetailsResponseData.setValue(
                                    ApiResponse.error<CustomerValidationResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    /*Remove Cart API*/
    private val getRemoveCartDetailsResponseData = MutableLiveData<ApiResponse<*>>()

    fun getRemoveCartProductDetailsResponse(): MutableLiveData<ApiResponse<*>>? {
        return getRemoveCartDetailsResponseData
    }

    fun RemoveCartProductDetails(
        removecartReq: RemoveCartItemRequest,
        auth_token: String,
    ) {
        Log.e(TAG, "RemoveCartProductDetails()")
        try {
            repository.getRemoveCartProductDetails(auth_token,removecartReq)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getRemoveCartDetailsResponseData.setValue(ApiResponse.loading<CustomerValidationResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getRemoveCartDetailsResponseData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getRemoveCartDetailsResponseData.setValue(
                                    ApiResponse.error<CustomerValidationResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    /*Remove simgleComparison API*/
    private val getRemoveSingleComparisonDetailsResponseData = MutableLiveData<ApiResponse<*>>()

    fun getRemoveSingleComparisonProductDetailsResponse(): MutableLiveData<ApiResponse<*>>? {
        return getRemoveSingleComparisonDetailsResponseData
    }

    fun RemoveSingleComparisonProductDetails(
        removecartReq: RemoveSingleComparisonRequest,
        auth_token: String,
    ) {
        Log.e(TAG, "RemoveSingleComparisonProductDetails()")
        try {
            repository.getRemoveSingleProductProductDetails(auth_token,removecartReq)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getRemoveSingleComparisonDetailsResponseData.setValue(ApiResponse.loading<CustomerValidationResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getRemoveSingleComparisonDetailsResponseData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getRemoveSingleComparisonDetailsResponseData.setValue(
                                    ApiResponse.error<CustomerValidationResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    /*Compare Products API*/
    private val getCompareDetailsResponseData = MutableLiveData<ApiResponse<*>>()

    fun getCompareDetailsResponse(): MutableLiveData<ApiResponse<*>>? {
        return getCompareDetailsResponseData
    }

    fun CompareProductDetails(
        compareprodReq: ComparisonRequest,
        auth_token: String,
    ) {
        Log.e(TAG, "CompareProductDetails()")
        try {
            repository.getCompareDetails(auth_token,compareprodReq)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getCompareDetailsResponseData.setValue(ApiResponse.loading<CustomerValidationResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getCompareDetailsResponseData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getCompareDetailsResponseData.setValue(
                                    ApiResponse.error<CustomerValidationResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    /*Similar Products API*/
    private val getSimilarProductDetailsResponseData = MutableLiveData<ApiResponse<*>>()

    fun getSimilarProductDetailsResponse(): MutableLiveData<ApiResponse<*>>? {
        return getSimilarProductDetailsResponseData
    }

    fun SimilarProductDetails(
        similarprod_req: SimilarProductRequest,
        auth_token: String,
    ) {
        Log.e(TAG, "SimilarProductDetails()")
        try {
            repository.getSimilarProductDetails(auth_token,similarprod_req)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getSimilarProductDetailsResponseData.setValue(ApiResponse.loading<CustomerValidationResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getSimilarProductDetailsResponseData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getSimilarProductDetailsResponseData.setValue(
                                    ApiResponse.error<CustomerValidationResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }


    /*Accounts Screen Api*/
    private val getAccountsLiveData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getAccountsResponse(): MutableLiveData<ApiResponse<*>>{
        return getAccountsLiveData
    }

    fun AccountsData(
        context: AccountFragment,
        auth_token: String,
        pagenumer: String
    ) {
        Log.e(TAG, "AccountsData()")
        try {
            repository.getAccountsDetails(auth_token,pagenumer)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getAccountsLiveData.setValue(ApiResponse.loading<AccountsResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getAccountsLiveData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getAccountsLiveData.setValue(
                                    ApiResponse.error<AccountsResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*VisitLogs Screen Api*/
    private val getVisitLogsData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getVisitLogsResponse(): MutableLiveData<ApiResponse<*>>{
        return getVisitLogsData
    }

    fun VisitLogsData(
        auth_token: String,
        postReq : VisitLogsRequestModel
    ) {
        Log.e(TAG, "VisitLogsData()")
        try {
            repository.getVisitorLogDetails(auth_token,postReq)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getVisitLogsData.setValue(ApiResponse.loading<AccountsResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getVisitLogsData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getVisitLogsData.setValue(
                                    ApiResponse.error<AccountsResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "LandingData()-Exception-" + ex.message)
        }
    }

    /*Wishlist Api*/
    private val getwishlistLiveData: MutableLiveData<ApiResponse<*>> = MutableLiveData<ApiResponse<*>>()

    fun getWishlistResponse(): MutableLiveData<ApiResponse<*>>{
        return getwishlistLiveData
    }

    fun WishlistData(
        context: LandingScreenFragment,
        auth_token: String,
    ) {
        Log.e(TAG, "SendSalesTargetData()")
        try {
            repository.wishlistProductsRequest(auth_token)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getwishlistLiveData.setValue(ApiResponse.loading<WishlistResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getwishlistLiveData.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getwishlistLiveData.setValue(
                                    ApiResponse.error<WishlistResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    /*Validation API*/
    private val getCustomerValidationResponse = MutableLiveData<ApiResponse<*>>()

    fun getCustomerValidationResponse(): MutableLiveData<ApiResponse<*>>? {
        return getCustomerValidationResponse
    }

    fun customerValidation(
        context: Context,
        validationString: CustomerValidationRequestModel,
        auth_token: String,
    ) {
        Log.e(TAG, "customerValidation()")
        try {
            repository.getCustomerValidation(auth_token,validationString)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getCustomerValidationResponse.setValue(ApiResponse.loading<CustomerValidationResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getCustomerValidationResponse.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getCustomerValidationResponse.setValue(
                                    ApiResponse.error<CustomerValidationResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }


    fun SearchproductAPI(
        context: Context,
        itemsearch: SearchItemRequest,
        auth_token: String,
    ) {
        Log.e(TAG, "customerValidation()")
        try {
            repository.getProductSearch(auth_token,itemsearch)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getProductsearchResponse.setValue(ApiResponse.loading<CustomerValidationResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getProductsearchResponse.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getProductsearchResponse.setValue(
                                    ApiResponse.error<SearchItemRequest>(
                                        throwable
                                    )
                                )
                            }
                        ))}
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    /*Registration API*/

    private val getCustomerRegistrationResponse = MutableLiveData<ApiResponse<*>>()

    fun getCustomerRegistrationResponse(): MutableLiveData<ApiResponse<*>>? {
        return getCustomerRegistrationResponse
    }

    fun customerRegistration(
        context: Context,
        registrationString: CustomerRegistrationRequest,
        auth_token: String,
    ) {
        Log.e(TAG, "customerValidation()")
        try {
            repository.getCustomerRegistration(auth_token,registrationString)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getCustomerRegistrationResponse.setValue(ApiResponse.loading<CustomerRegistrationResponse>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getCustomerRegistrationResponse.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getCustomerRegistrationResponse.setValue(
                                    ApiResponse.error<CustomerRegistrationResponse>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    /*Accounts Related Wishlist API*/
    private val getCustomerRelatedWishlistResponse = MutableLiveData<ApiResponse<*>>()

    fun getCustomerRelatedWishlistResponse(): MutableLiveData<ApiResponse<*>>? {
        return getCustomerRelatedWishlistResponse
    }

    fun customerRelatedWishlist(
        context: Context,
        validationString: CustomerRelatedRequestModel,
        auth_token: String,
    ) {
        Log.e(TAG, "customerRelatedWishlist()")
        try {
            repository.getCustomerRelatedwishlist(auth_token,validationString)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getCustomerRelatedWishlistResponse.setValue(ApiResponse.loading<CustomerValidationResponseModel>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getCustomerRelatedWishlistResponse.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getCustomerRelatedWishlistResponse.setValue(
                                    ApiResponse.error<CustomerValidationResponseModel>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    /*Accounts Update API*/

    private val getAccountsUpdateResponse = MutableLiveData<ApiResponse<*>>()

    fun getCAccountsUpdateResponse(): MutableLiveData<ApiResponse<*>>? {
        return getAccountsUpdateResponse
    }

    fun accountsUpdation(
        context: Context,
        updationString: AccountsUpdateRequestModel,
        auth_token: String,
    ) {
        Log.e(TAG, "accountsUpdation()")
        try {
            repository.getAccountsUpdate(auth_token,updationString)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.doOnSubscribe { getAccountsUpdateResponse.setValue(ApiResponse.loading<CustomerRegistrationResponse>()) }
                ?.let {
                    disposables.add(it
                        .subscribe(
                            { result ->
                                Log.e(TAG, "response()")
                                getAccountsUpdateResponse.setValue(ApiResponse.success(result))
                            },
                            { throwable ->
                                //val responseCode = (throwable as HttpException).code()
                                //Log.e(TAG, "responseCode() - $responseCode")
                                getAccountsUpdateResponse.setValue(
                                    ApiResponse.error<CustomerRegistrationResponse>(
                                        throwable
                                    )
                                )
                            }
                        ))
                }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "validateUser()-Exception-" + ex.message)
        }
    }

    override fun onCleared() {
        disposables.clear()
    }



}

