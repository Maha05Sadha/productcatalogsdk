package com.checkin.dagger.component

import android.content.Context
import com.absyz.kotlin.activities.ui.SplashscreenActivity
import com.absyz.kotlin.activities.ui.*
import com.absyz.kotlin.fragments.*
import com.checkin.helpers.SessionStore
import com.checkin.helpers.Utility
import com.checkin.dagger.module.ApplicationModule
import com.checkin.dagger.module.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, NetworkModule::class])
interface ApplicationComponent {

    fun context(): Context

    fun sessionStore(): SessionStore // can inject util/object in another helper class by using this

    fun utility(): Utility // can inject util/object in another helper class

    /*activities*/
    fun inject(activity: BaseActivity)
    fun inject(activity: CreateVisit)
    fun inject(activity: VisitExists)
    fun inject(activity: LandingpageActivity)
    fun inject(activity: VisitCreation)
    fun inject(activity: AccountUpdateActivity)
    fun inject(activity: BrandCategoryDetailActivity)
    fun inject(activity: CartActivity)
    fun inject(activity: ComparisonActivity)
    fun inject(activity: ProductCategoryDetailActivity)
    fun inject(activity: ProductDetailActivity)
    fun inject(activity: SearchProductActivity)
    fun inject(activity: SplashscreenActivity)

    /*fragments*/
    fun inject(fragment: BaseFragment)
    fun inject(fragment: AccountFragment)
    fun inject(fragment: LandingScreenFragment)
    fun inject(fragment: OrderFragment)
    fun inject(fragment: SalesTargetFragment)
    fun inject(fragment: SettingsFragment)
    fun inject(fragment: PieChartFragment)
    fun inject(fragment: ViewLogsFragment)
}

