package com.checkin.dagger.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.absyz.kotlin.dagger.factory.ViewModelFactory
import com.checkin.dagger.scope.ViewModelKey
import com.checkin.viewmodel.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {


 /*@Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ViewModel::class)
    abstract fun bindCountryViewModel(repoViewModel: ViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SignUpViewModel::class)
    abstract fun bindSignUpViewModel(repoViewModel: SignUpViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CommonViewModel::class)
    abstract fun bindCommonViewModel(repoViewModel: CommonViewModel): ViewModel*/

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel :: class)
    abstract fun bindHomeViewModel(repoViewModel: HomeViewModel) : ViewModel

}
