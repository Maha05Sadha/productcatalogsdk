package com.monsterbrain.recyclerviewtableview

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.TypedValue
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.absyz.kotlin.R
import com.absyz.kotlin.databinding.CartlistCviewBinding
import com.absyz.kotlin.databinding.QtyListItemBinding
import com.absyz.kotlin.model.ResponseModel.ProductCartDetails
import com.checkin.helpers.AppConstants
import com.checkin.helpers.Utility

class QuantityAdapter(val context: Context, val utility: Utility) : RecyclerView.Adapter<QuantityAdapter.RowViewHolder>() {

    var full_qtylist = ArrayList<Int>()

    private lateinit var mListerner: onItemClickListener

    interface onItemClickListener{
        fun onItemCLick(position: Int)
    }

    fun setOnItemCLickListerner(listner: onItemClickListener){
        mListerner = listner
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowViewHolder {
        val binding = DataBindingUtil.inflate<QtyListItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.qty_list_item,
            parent,
            false
        )
        return RowViewHolder(binding,mListerner)
    }

    override fun onBindViewHolder(holder: RowViewHolder, position: Int) {
        val qty_values = full_qtylist!![position]
        holder.binding.qtyVal.text = qty_values.toString()
    }

    override fun getItemCount(): Int {
        return if (full_qtylist != null) full_qtylist!!.size else 0
    }

    fun updateList(newList: ArrayList<Int>) {
        try {
            full_qtylist = newList
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }


    inner class RowViewHolder(var binding: QtyListItemBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
        init{
            itemView.setOnClickListener {
                listner.onItemCLick(adapterPosition)
            }

        }
    }
}


