package com.absyz.kotlin.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.absyz.kotlin.R
import com.absyz.kotlin.activities.ui.LandingpageActivity
import com.absyz.kotlin.databinding.AccountListitemBinding
import com.absyz.kotlin.databinding.SimilarproductCviewBinding
import com.absyz.kotlin.databinding.VisitersCardBinding
import com.absyz.kotlin.model.RequestModel.InActiveCustomerRequest
import com.absyz.kotlin.model.ResponseModel.CustomerListDetails
import com.absyz.kotlin.model.ResponseModel.SimilarProductDetails
import com.checkin.helpers.AppConstants
import com.checkin.helpers.Utility
import com.monsterbrain.recyclerviewtableview.AccountsAdapter
import com.squareup.picasso.Picasso

class CurrentVisitAdapter(val context: Context, val utility: Utility, val onClickDelete: (Int) -> Unit) : RecyclerView.Adapter<CurrentVisitAdapter.RowViewHolder>() {

      var CustomerVisitorFullList : HashMap<String,String> ?= null
    private var customerList: ArrayList<CustomerListDetails> = ArrayList()

    var customer_ids : Set<String> ?= null
    var selected_customer_key : String = ""

    lateinit var landingpageactivity: LandingpageActivity

    var inactiveReq : InActiveCustomerRequest?=null

    private lateinit var mListerner: onItemClickListener

    interface onItemClickListener{
        fun onItemCLick(position: Int)
    }

    fun setOnItemCLickListerner(listner: onItemClickListener){
        mListerner = listner
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowViewHolder {
        val binding = DataBindingUtil.inflate<VisitersCardBinding>(
            LayoutInflater.from(parent.context),
            R.layout.visiters_card,
            parent,
            false
        )
        return RowViewHolder(binding,mListerner)
    }

    override fun onBindViewHolder(holder: RowViewHolder, position: Int) {
        try {
//            customer_ids = CustomerVisitorFullList?.keys
//            selected_customer_key = customer_ids!!.elementAt(position)
//
//            holder.binding.name2.text = CustomerVisitorFullList?.get(selected_customer_key)
//            holder.binding.endBtn2.setOnClickListener {
//                //Call end session api
//                val current_id = customer_ids!!.elementAt(position)
//                Toast.makeText(context, current_id, Toast.LENGTH_SHORT).show()
//                //holder.binding.onlineIndicator.setBackgroundResource(R.drawable.offline_indicator)
//            }

            holder.binding.name2.text = customerList[position].Name
            if(customerList[position].Active.equals("Yes")){
                holder.binding.onlineIndicator.setBackgroundResource(R.drawable.online_indicator)
            } else if(customerList[position].Active.equals("No")){
                holder.binding.onlineIndicator.setBackgroundResource(R.drawable.offline_indicator)
            }
            holder.binding.endBtn2.setOnClickListener {
                val current_id = customerList[position].Id
                //Toast.makeText(context, current_id, Toast.LENGTH_SHORT).show()
                holder.binding.onlineIndicator.setBackgroundResource(R.drawable.offline_indicator)
                onClickDelete(position)
            }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    override fun getItemCount(): Int {
        return if (customerList != null) customerList!!.size else 0
    }

//    fun updateList(CustomerVisitorList : HashMap<String,String>) {
//        try {
//            CustomerVisitorFullList = CustomerVisitorList as HashMap<String, String>
//
//            notifyDataSetChanged()
//        } catch (ex: Exception) {
//            ex.printStackTrace()
//        }
//
//    }

    fun updateList(CustomerListDetails : ArrayList<CustomerListDetails>) {
        try {
            customerList = CustomerListDetails as ArrayList<CustomerListDetails>

            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun setItems(CustomerListDetails : ArrayList<CustomerListDetails>) {
        try {
            customerList = CustomerListDetails as ArrayList<CustomerListDetails>

            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    inner class RowViewHolder(var binding: VisitersCardBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
        init{
            itemView.setOnClickListener {
                listner.onItemCLick(adapterPosition)
            }
        }
    }
}

