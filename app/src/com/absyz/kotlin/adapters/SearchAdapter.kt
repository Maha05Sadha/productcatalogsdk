package com.absyz.kotlin.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.absyz.kotlin.R
import com.absyz.kotlin.databinding.ItemSearchProductBinding
import com.absyz.kotlin.model.ResponseModel.SearchProductDetails
import com.bumptech.glide.Glide
import com.checkin.helpers.AppConstants
import com.checkin.helpers.Utility

class SearchAdapter constructor(
    val context: Context,
    private val itemClickListener: ItemClickListener,
    val utility: Utility
    ) :
        RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    private var itemArrayList: List<SearchProductDetails>? = null
    var image_id:String = ""
    var image_url : String = ""


    companion object {
        const val TAG = "SearchAdapter"
    }


    interface ItemClickListener {
        fun onItemClick(item: SearchProductDetails?)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ItemSearchProductBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_search_product,
            parent,
            false
        )
        return ViewHolder(binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            Log.e(TAG, "onBindViewHolder()")
            val searchModel = itemArrayList!![position]
            holder.binding.categoryTitle.text = searchModel.Name


            image_id = searchModel.ProductImage
            if(image_id.equals(null)){
                image_id = "0685g000002AcTIAA0"
                image_url = AppConstants.BaseImageURL+image_id+"/VersionData"

                Glide
                    .with(context)
                    .load(image_url)
                    .centerCrop()
                    .placeholder(R.drawable.brand5)
                    .into(holder.binding.imageView)

            } else{
                image_url = AppConstants.BaseImageURL+image_id+"/VersionData"
                utility.loadGlide(context,holder.binding.imageView,image_url,0)
            }
            /* utility!!.loadGlideWithCircleSkipCache(
                 context,
                 holder.binding.userImage,
                 "",
                 R.drawable.profile_default
             )*/

            holder.binding.cardView.setOnClickListener {
                itemClickListener.onItemClick(searchModel)
            }


            /*holder.binding.tvSlotTime.text = Utility.getDesiredDateFormatFromUTC(
                AppConstants.SIMPLE_DATE_FORMAT_REQUEST_UTC,
                AppConstants.SIMPLE_DATE_FORMAT_WITH_TIME,
                searchModel.slotTime
            )*/

        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "onBindViewHolder() Exception - ${ex.message}")
        }

    }

    override fun getItemCount(): Int {
        return if (itemArrayList != null) itemArrayList!!.size else 0
    }

    fun updateList(newList: List<SearchProductDetails>) {
        try {
            itemArrayList = newList
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "updateList() Exception - ${ex.message}")
        }

    }

    inner class ViewHolder(internal var binding: ItemSearchProductBinding) :
        RecyclerView.ViewHolder(binding.root)
}