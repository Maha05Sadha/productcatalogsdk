package com.absyz.kotlin.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.absyz.kotlin.R
import com.absyz.kotlin.databinding.LandingProductCviewBinding
import com.absyz.kotlin.model.ResponseModel.ProductCategory
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.checkin.helpers.AppConstants
import com.checkin.helpers.Utility
import com.monsterbrain.recyclerviewtableview.TargetSalesAdapter


class ProductCategoriesAdapter constructor(val context: Context,
                                               val utility: Utility
                                               ) : RecyclerView.Adapter<ProductCategoriesAdapter.ViewHolder>() {

        private var productcategoryDetails: ArrayList<ProductCategory> = ArrayList()

        var image_id:String = ""
        var image_url : String = ""

    private lateinit var mListerner: onItemClickListener

    interface onItemClickListener{
        fun onItemCLick(position: Int)
    }

    fun setOnItemCLickListerner(listner: onItemClickListener){
        mListerner = listner
    }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val binding = DataBindingUtil.inflate<LandingProductCviewBinding>(
                LayoutInflater.from(parent.context),
                R.layout.landing_product_cview,
                parent,
                false
            )
            return ViewHolder(binding,mListerner)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {

            val productListmodel = productcategoryDetails!![position]
            holder.binding.categoryTitle.text = productListmodel.ProductCategory
            image_id = productListmodel.productDetails
            if(image_id.equals(null)){
                image_id = "0685g000002AcTIAA0"
                image_url = AppConstants.BaseImageURL+image_id+"/VersionData"

                Glide
                    .with(context)
                    .load(image_url)
                    .centerCrop()
                    .placeholder(R.drawable.brand5)
                    .into(holder.binding.imageView)

            } else{
                image_url = AppConstants.BaseImageURL+image_id+"/VersionData"
                utility.loadGlide(context,holder.binding.imageView,image_url,0)
            }
            //image_url = AppConstants.BaseImageURL+image_id+"/VersionData"


//            utility.loadGlideurl(context, image_url)

//            GlideApp.with(context)
//                .load(glideUrl)
//                .into(imageView)

            //utility.loadGlide(context,holder.binding.imageView,image_url,0)

//            Glide
//                .with(context)
//                .load(image_url)
//                .centerCrop()
//
//            .placeholder(R.drawable.furniture3)
//                .into(holder.binding.imageView)


        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

        override fun getItemCount(): Int {
            return if (productcategoryDetails != null) productcategoryDetails!!.size else 0
        }

        fun updateList(newList: List<ProductCategory>) {
            try {
                productcategoryDetails = newList as ArrayList<ProductCategory>
                notifyDataSetChanged()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }

        inner class ViewHolder(internal var binding: LandingProductCviewBinding, listner: onItemClickListener) :
            RecyclerView.ViewHolder(binding.root){
            init{
                itemView.setOnClickListener {
                    listner.onItemCLick(adapterPosition)
                }
            }
            }


    }

