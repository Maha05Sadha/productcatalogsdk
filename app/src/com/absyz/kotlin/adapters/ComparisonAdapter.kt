package com.absyz.kotlin.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.absyz.kotlin.R
import com.absyz.kotlin.databinding.ComparisonCviewBinding
import com.absyz.kotlin.model.ResponseModel.AccountSearchUserDetails
import com.absyz.kotlin.model.ResponseModel.ComparisonDetails
import com.absyz.kotlin.model.ResponseModel.ProductCartDetails
import com.checkin.helpers.AppConstants
import com.checkin.helpers.Utility

class ComparisonAdapter(val context: Context, val utility: Utility, private val itemClickListener: ComparisonAdapter.ItemClickListener,
                        val onClickDelete: (Int) -> Unit) : RecyclerView.Adapter<ComparisonAdapter.ViewHolder>() {

    private var productComparisonDetails: ArrayList<ComparisonDetails> = ArrayList()

    var image_id:String = ""
    var image_url : String = ""
    var color_code : String = ""
    var prod_added : String = ""


    interface ItemClickListener {
        fun onItemClick(item: ComparisonDetails)
    }

    private lateinit var mListerner: onItemClickListener

    interface onItemClickListener{
        fun onItemCLick(position: Int)
    }

    fun setOnItemCLickListerner(listner: onItemClickListener){
        mListerner = listner
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ComparisonCviewBinding>(
            LayoutInflater.from(parent.context),
            R.layout.comparison_cview,
            parent,
            false
        )
        return ViewHolder(binding,mListerner)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {

                val productCompareListmodel = productComparisonDetails!![position]

                image_id = productCompareListmodel.ProductImageId
                image_url = AppConstants.BaseImageURL + image_id + "/VersionData"
                utility.loadGlide(context, holder.binding.prodImg, image_url, 0)

                color_code = "#"+productCompareListmodel.Colour

                holder.binding.prodname.text = productCompareListmodel.ProductName
                holder.binding.prodPrice.text = productCompareListmodel.Price.toString()
                holder.binding.prodmat.text = productCompareListmodel.Material
                holder.binding.colorpalletVal.setBackgroundColor(Color.parseColor(color_code));

                holder.binding.remove.setOnClickListener {
                    onClickDelete(position)
                }
                holder.binding.addtocart.setOnClickListener {

                    //onAddtoCart(position)
                    itemClickListener.onItemClick(productCompareListmodel)
                    if(prod_added.equals("yes")){
                        holder.binding.addedcartlayout.visibility = View.VISIBLE
                        holder.binding.addtocart.visibility = View.GONE
                    }
                }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    override fun getItemCount(): Int {
        return productComparisonDetails.size
    }

    fun updateList(newList: List<ComparisonDetails>) {
        try {
            productComparisonDetails = newList as ArrayList<ComparisonDetails>
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun setItems(newList: List<ComparisonDetails>) {
        try {
            productComparisonDetails = newList as ArrayList<ComparisonDetails>
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun addtocart(productadded: String) {
        prod_added = productadded

    }

    inner class ViewHolder(internal var binding: ComparisonCviewBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
        init{
            itemView.setOnClickListener {
                listner.onItemCLick(adapterPosition)
            }
        }
    }

}

