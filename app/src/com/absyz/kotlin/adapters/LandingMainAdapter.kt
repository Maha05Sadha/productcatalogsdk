package com.absyz.kotlin.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.absyz.kotlin.R
import com.absyz.kotlin.databinding.AccountListitemBinding
import com.absyz.kotlin.databinding.LandingFragmentCardviewBinding
import com.absyz.kotlin.model.ResponseModel.ContentItem
import com.absyz.kotlin.model.ResponseModel.ContentMainModel

class LandingMainAdapter (private val context: Context, private val landimgRecyclerView: List<ContentMainModel>) : RecyclerView.Adapter<LandingMainAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<LandingFragmentCardviewBinding>(
            LayoutInflater.from(parent.context),
            R.layout.landing_fragment_cardview,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.contentTitle?.text = landimgRecyclerView[position].itemname
        holder.binding.recyclerView?.let { setProductItemsRecycler(it,landimgRecyclerView[position].contentItem) }
    }

    override fun getItemCount() = landimgRecyclerView.size

    private fun setProductItemsRecycler(recyclerView: RecyclerView,contentitems: List<ContentItem>){
        val contentRecyclerAdapter = RenameAdapter(context,contentitems)
        recyclerView.layoutManager = LinearLayoutManager(context,RecyclerView.HORIZONTAL,false)
        recyclerView.adapter = contentRecyclerAdapter
    }

    inner class ViewHolder(var binding: LandingFragmentCardviewBinding) : RecyclerView.ViewHolder(binding.root)


}