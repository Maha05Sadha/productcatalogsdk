package com.absyz.kotlin.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.absyz.kotlin.R
import com.absyz.kotlin.databinding.FiltercolorPalletItemsBinding
import com.absyz.kotlin.databinding.LandingBrandCviewBinding
import com.absyz.kotlin.model.ResponseModel.Brands
import com.absyz.kotlin.model.ResponseModel.FilterColors
import com.absyz.kotlin.model.ResponseModel.ProductColorCodes
import com.absyz.kotlin.model.ResponseModel.SalesTargetListDetails
import com.bumptech.glide.Glide
import com.checkin.helpers.AppConstants
import com.checkin.helpers.Utility
import com.monsterbrain.recyclerviewtableview.TargetSalesAdapter
import com.squareup.picasso.Picasso

    class FilterColorPalletAdapter constructor(val context: Context, val utility: Utility) : RecyclerView.Adapter<FilterColorPalletAdapter.RowViewHolder>() {

        private var filtercolorPalletList: ArrayList<FilterColors> = ArrayList()

        private lateinit var mListerner: onItemClickListener

        var colorname : String = "ffffff"
        var finale_color : String = ""
        var color_selected : Boolean = false

        interface onItemClickListener{
            fun onItemCLick(position: Int)
        }

        fun setOnItemCLickListerner(listner: onItemClickListener){
            mListerner = listner
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowViewHolder {
            val binding = DataBindingUtil.inflate<FiltercolorPalletItemsBinding>(
                LayoutInflater.from(parent.context),
                R.layout.filtercolor_pallet_items,
                parent,
                false
            )
            return RowViewHolder(binding,mListerner)
    }


    override fun onBindViewHolder(holder: RowViewHolder, position: Int) {

        try {

            val colorsList = filtercolorPalletList!![position]
            holder.binding.colorpalletVal.setBackgroundColor(Color.parseColor("#"+colorsList.Colour))
//            if(colorname.equals("")){
//                Toast.makeText(context,"Choose a color", Toast.LENGTH_SHORT).show()
//            } else{
//                holder.binding.colorView.setBackgroundColor(Color.parseColor(finale_color))
//            }
            holder.binding.colorView.setOnClickListener {
                if(colorname.equals("ffffff")){
                    colorname = "34b347"
                    holder.binding.colorView.setBackgroundColor(Color.parseColor("#34b347"))
                } else if(colorname.equals("34b347")){
                    colorname = "ffffff"
                    holder.binding.colorView.setBackgroundResource(R.drawable.colorpanel_view)
                }
            }



        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

        override fun getItemCount(): Int {
            return if (filtercolorPalletList != null) filtercolorPalletList!!.size else 0
        }

        fun updateList(newList: List<FilterColors>) {
            try {
                filtercolorPalletList = newList as ArrayList<FilterColors>
                notifyDataSetChanged()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }

        fun changebgcolor( colorName : String) {
            colorname = colorName
        }

        inner class RowViewHolder(internal var binding: FiltercolorPalletItemsBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
            init{
                itemView.setOnClickListener {
                    listner.onItemCLick(adapterPosition)
                }
            }
        }

    }

