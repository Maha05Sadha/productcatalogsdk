package com.monsterbrain.recyclerviewtableview

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.absyz.kotlin.R
import com.absyz.kotlin.databinding.LandingContentCviewBinding
import com.absyz.kotlin.databinding.TargetsalesCardviewBinding
import com.absyz.kotlin.model.ResponseModel.SalesTargetListDetails
import com.bumptech.glide.Glide
import com.checkin.helpers.AppConstants
import com.checkin.helpers.Utility
import com.squareup.picasso.Picasso

class TargetSalesAdapter(val context: Context,val utility: Utility) : RecyclerView.Adapter<TargetSalesAdapter.RowViewHolder>() {

    private var targetSalesmodelArrayList: List<SalesTargetListDetails>? = null
    var image_id:String = ""
    var image_url : String = ""
    var instock_val : String = ""

    private lateinit var mListerner: onItemClickListener

    interface onItemClickListener{
        fun onItemCLick(position: Int)
    }

    fun setOnItemCLickListerner(listner: onItemClickListener){
        mListerner = listner
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowViewHolder {
        val binding = DataBindingUtil.inflate<TargetsalesCardviewBinding>(
            LayoutInflater.from(parent.context),
            R.layout.targetsales_cardview,
            parent,
            false
        )
        return RowViewHolder(binding,mListerner)
    }

    override fun onBindViewHolder(holder: RowViewHolder, position: Int) {
        try {

            val targetListmodel = targetSalesmodelArrayList!![position]
            holder.binding.itemTitle.text = targetListmodel.ProductType
            holder.binding.contentDesc.text = targetListmodel.Dimentions
            holder.binding.price.text = targetListmodel.Price.toString()
            holder.binding.totalCount.text = targetListmodel.Total.toString()
            holder.binding.remainCount.text = targetListmodel.Remaining.toString()

            instock_val = targetListmodel.ProductStock
            Log.d("instock_val ->",instock_val)
            if (instock_val.equals("true")) {
                holder.binding.instock.text = "INSTOCK"
                holder.binding.instock.setTextColor(ContextCompat.getColor(context, R.color.validation_green));
            } else if (instock_val.equals("false")) {
                holder.binding.instock.text = "NOSTOCK"
                holder.binding.instock.setTextColor(ContextCompat.getColor(context, R.color.validation_red));
            }

            image_id = targetListmodel.ProductImage
            if(image_id.equals(null)){
                image_id = "0685g000002AcTIAA0"
                image_url = AppConstants.BaseImageURL+image_id+"/VersionData"

                Glide
                    .with(context)
                    .load(image_url)
                    .centerCrop()
                    .placeholder(R.drawable.brand5)
                    .into(holder.binding.itemImg1)

            } else{
                image_url = AppConstants.BaseImageURL+image_id+"/VersionData"
                utility.loadGlide(context,holder.binding.itemImg1,image_url,0)

            }

//            Glide
//                .with(context)
//                .load(image_url)
//                .centerCrop()
//                .placeholder(R.drawable.furniture4)
//                .into(holder.binding.itemImg1)

            //holder.item_img_1.setBackgroundResource(R.drawable.furniture4)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    override fun getItemCount(): Int {
        return if (targetSalesmodelArrayList != null) targetSalesmodelArrayList!!.size else 0
    }

    fun updateList(newList: List<SalesTargetListDetails>) {
        try {
            targetSalesmodelArrayList = newList as ArrayList<SalesTargetListDetails>
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    inner class RowViewHolder(var binding: TargetsalesCardviewBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
        init{
            itemView.setOnClickListener {
                listner.onItemCLick(adapterPosition)
            }
        }
    }
}

