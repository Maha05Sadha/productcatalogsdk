package com.monsterbrain.recyclerviewtableview

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.absyz.kotlin.R
import com.absyz.kotlin.R.drawable.edit_24
import com.absyz.kotlin.databinding.AccountListitemBinding
import com.absyz.kotlin.databinding.VisitListitemBinding
import com.absyz.kotlin.model.ResponseModel.AccountsUserDetails
import com.absyz.kotlin.model.ResponseModel.ProductCartDetails
import com.absyz.kotlin.model.ResponseModel.VisitorDetails
import com.checkin.helpers.Utility
import kotlinx.android.synthetic.main.visit_listitem.view.*

class VisitLogsAdapter(val context: Context, val utility: Utility,val onClickDelete: (Int) -> Unit) : RecyclerView.Adapter<VisitLogsAdapter.RowViewHolder>() {

    private var visitordetailsArrayList: List<VisitorDetails>? = null

    private lateinit var mListerner: onItemClickListener

    interface onItemClickListener{
        fun onItemCLick(position: Int)
    }

    fun setOnItemCLickListerner(listner: onItemClickListener){
        mListerner = listner
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowViewHolder {
        val binding = DataBindingUtil.inflate<VisitListitemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.visit_listitem,
            parent,
            false
        )
        return RowViewHolder(binding,mListerner)
    }

    private fun setHeaderBg(view: View) {
        view.setBackgroundResource(R.drawable.accounts_header_cell)
    }

    private fun setContentBg(view: View) {
        view.setBackgroundResource(R.drawable.accounts_content_cell)
    }

    override fun onBindViewHolder(holder: RowViewHolder, position: Int) {
//        val rowPos = holder.adapterPosition
//        val modal = accountsmodelArrayList?.get(rowPos - 1)

        var status : String = "Inactive"

        holder.binding.visitName.text = visitordetailsArrayList?.get(position)?.Name
        holder.binding.visitStatus.text = visitordetailsArrayList?.get(position)?.Status
        holder.binding.visitSalesperson.text = visitordetailsArrayList?.get(position)?.SalesPerson
        holder.binding.endBtn.setOnClickListener {
            holder.binding.visitStatus.text = status
            //Call end session api
            onClickDelete(position)
        }
    }

    override fun getItemCount(): Int {
        return if (visitordetailsArrayList != null) visitordetailsArrayList!!.size else 0
    }

    fun updateList(newList: List<VisitorDetails>) {
        try {
            visitordetailsArrayList = newList as ArrayList<VisitorDetails>
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun setItems(newList: List<VisitorDetails>) {
        try {
            visitordetailsArrayList = newList as ArrayList<VisitorDetails>
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    inner class RowViewHolder(var binding: VisitListitemBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
        init{
            itemView.setOnClickListener {
                listner.onItemCLick(adapterPosition)
            }

        }
    }
}

