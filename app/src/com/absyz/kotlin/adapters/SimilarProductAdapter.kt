package com.absyz.kotlin.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.absyz.kotlin.R
import com.absyz.kotlin.activities.ui.ComparisonActivity
import com.absyz.kotlin.databinding.SimilarproductCviewBinding
import com.absyz.kotlin.model.ResponseModel.SimilarProductDetails
import com.checkin.helpers.AppConstants
import com.checkin.helpers.Utility

class SimilarProductAdapter(val context: Context, val utility: Utility, private val itemClickListener: ComparisonActivity) : RecyclerView.Adapter<SimilarProductAdapter.RowViewHolder>() {

    private var similarProdArrayList: List<SimilarProductDetails>? = null
    var image_id:String = ""
    var image_url : String = ""
    var instock_val : String = ""

    interface ItemClickListener {
        fun onItemClick(item: SimilarProductDetails?)
    }

    private lateinit var mListerner: onItemClickListener

    interface onItemClickListener{
        fun onItemCLick(position: Int)
    }

    fun setOnItemCLickListerner(listner: onItemClickListener){
        mListerner = listner
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowViewHolder {
        val binding = DataBindingUtil.inflate<SimilarproductCviewBinding>(
            LayoutInflater.from(parent.context),
            R.layout.similarproduct_cview,
            parent,
            false
        )
        return RowViewHolder(binding,mListerner)
    }

    override fun onBindViewHolder(holder: RowViewHolder, position: Int) {
        try {

            val similarProdListmodel = similarProdArrayList!![position]
            holder.binding.itemTitle.text = similarProdListmodel.ProductName
            holder.binding.contentDesc.text = similarProdListmodel.Dimentions
            holder.binding.price.text = similarProdListmodel.Price.toString()

            image_id = similarProdListmodel.ProductImageId
                image_url = AppConstants.BaseImageURL+image_id+"/VersionData"
                utility.loadGlide(context,holder.binding.itemImg1,image_url,0)

            holder.binding.addtocompareBtn.setOnClickListener {
                //addToCompare(position)
                holder.binding.addedcomparelayout.visibility = View.VISIBLE
                holder.binding.addtocompareBtn.visibility = View.GONE
                itemClickListener.onItemClick(similarProdListmodel)
            }

//            Glide
//                .with(context)
//                .load(image_url)
//                .centerCrop()
//                .placeholder(R.drawable.furniture4)
//                .into(holder.binding.itemImg1)

            //holder.item_img_1.setBackgroundResource(R.drawable.furniture4)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    override fun getItemCount(): Int {
        return if (similarProdArrayList != null) similarProdArrayList!!.size else 0
    }

    fun updateList(newList: List<SimilarProductDetails>) {
        try {
            similarProdArrayList = newList as ArrayList<SimilarProductDetails>
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    inner class RowViewHolder(var binding: SimilarproductCviewBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
        init{
            itemView.setOnClickListener {
                listner.onItemCLick(adapterPosition)
            }
        }
    }
}

