package com.absyz.kotlin.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.absyz.kotlin.R
import com.absyz.kotlin.databinding.AccountListitemBinding
import com.absyz.kotlin.databinding.LandingBrandCviewBinding
import com.absyz.kotlin.model.ResponseModel.Brands
import com.absyz.kotlin.model.ResponseModel.SalesTargetListDetails
import com.bumptech.glide.Glide
import com.checkin.helpers.AppConstants
import com.checkin.helpers.Utility
import com.monsterbrain.recyclerviewtableview.TargetSalesAdapter
import com.squareup.picasso.Picasso

    class BrandAdapter constructor(val context: Context, val utility: Utility) : RecyclerView.Adapter<BrandAdapter.RowViewHolder>() {

        private var brandDetails: ArrayList<Brands> = ArrayList()

        var image_id:String = ""
        var image_url : String = ""

        private lateinit var mListerner: onItemClickListener

        interface onItemClickListener{
            fun onItemCLick(position: Int)
        }

        fun setOnItemCLickListerner(listner: onItemClickListener){
            mListerner = listner
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowViewHolder {
            val binding = DataBindingUtil.inflate<LandingBrandCviewBinding>(
                LayoutInflater.from(parent.context),
                R.layout.landing_brand_cview,
                parent,
                false
            )
            return RowViewHolder(binding,mListerner)
    }


    override fun onBindViewHolder(holder: RowViewHolder, position: Int) {

        try {

            val brandListmodel = brandDetails!![position]

            image_id = brandListmodel.BrandImage
            //image_url = AppConstants.BaseImageURL+image_id+"/VersionData"

            if(image_id.equals(null)){
               image_id = "0685g000002AcTIAA0"
                image_url = AppConstants.BaseImageURL+image_id+"/VersionData"

                Glide
                .with(context)
                .load(image_url)
                .centerCrop()
                .placeholder(R.drawable.brand5)
                .into(holder.binding.imageView)

            } else{
                image_url = AppConstants.BaseImageURL+image_id+"/VersionData"
                utility.loadGlide(context,holder.binding.imageView,image_url,0)

            }


//            Glide
//                .with(context)
//                .load(image_url)
//                .centerCrop()
//                .placeholder(R.drawable.brand5)
//                .into(holder.binding.imageView)
            //holder.imageView.setBackgroundResource(R.drawable.brand5)


        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

        override fun getItemCount(): Int {
            return if (brandDetails != null) brandDetails!!.size else 0
        }

        fun updateList(newList: List<Brands>) {
            try {
                brandDetails = newList as ArrayList<Brands>
                notifyDataSetChanged()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }
        inner class RowViewHolder(internal var binding: LandingBrandCviewBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
            init{
                itemView.setOnClickListener {
                    listner.onItemCLick(adapterPosition)
                }
            }
        }

    }

