package com.absyz.kotlin.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.absyz.kotlin.R
import com.absyz.kotlin.databinding.LandingContentCviewBinding
import com.absyz.kotlin.model.ResponseModel.SalesTargetListDetails
import com.absyz.kotlin.model.ResponseModel.TrendingProductsForTheStore
import com.bumptech.glide.Glide
import com.checkin.helpers.AppConstants
import com.checkin.helpers.Utility
import com.monsterbrain.recyclerviewtableview.TargetSalesAdapter
import com.squareup.picasso.Picasso

    class TrendingProductsAdapter constructor(val context: Context, val utility: Utility) : RecyclerView.Adapter<TrendingProductsAdapter.ViewHolder>() {

        private var trendingproductsDetails: ArrayList<TrendingProductsForTheStore> = ArrayList()

        var image_id:String = ""
        var image_url : String = ""

        private lateinit var mListerner: onItemClickListener

        interface onItemClickListener{
            fun onItemCLick(position: Int)
        }

        fun setOnItemCLickListerner(listner: onItemClickListener){
            mListerner = listner
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val binding = DataBindingUtil.inflate<LandingContentCviewBinding>(
                LayoutInflater.from(parent.context),
                R.layout.landing_content_cview,
                parent,
                false
            )
            return ViewHolder(binding,mListerner)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {

            val trendingListmodel = trendingproductsDetails!![position]
            holder.binding.itemTitle.text = trendingListmodel.Name
            holder.binding.contentDesc.text = trendingListmodel.Dimentions
            holder.binding.price.text = trendingListmodel.Price.toString()

            image_id = trendingListmodel.ProductImageId
            image_url = AppConstants.BaseImageURL+image_id+"/VersionData"
            utility.loadGlide(context,holder.binding.imageView,image_url,0)


//            if(image_id.equals(null)){
//                image_id = "0685g000002AcTIAA0"
//                image_url = AppConstants.BaseImageURL+image_id+"/VersionData"
//
//                Glide
//                    .with(context)
//                    .load(image_url)
//                    .centerCrop()
//                    .placeholder(R.drawable.brand5)
//                    .into(holder.binding.imageView)
//
//            } else{
//                image_url = AppConstants.BaseImageURL+image_id+"/VersionData"
//                utility.loadGlide(context,holder.binding.imageView,image_url,0)
//            }

            //            Glide
//                .with(context)
//                .load(image_url)
//                .centerCrop()
//                .placeholder(R.drawable.furniture3)
//                .into(holder.binding.imageView)
            //holder.imageView.setBackgroundResource(R.drawable.furniture3)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

        override fun getItemCount(): Int {
            return trendingproductsDetails.size
        }

        fun updateList(newList: List<TrendingProductsForTheStore>) {
            try {
                trendingproductsDetails = newList as ArrayList<TrendingProductsForTheStore>
                notifyDataSetChanged()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }
        inner class ViewHolder( internal var binding: LandingContentCviewBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
            init{
                itemView.setOnClickListener {
                    listner.onItemCLick(adapterPosition)
                }
            }
        }

    }

