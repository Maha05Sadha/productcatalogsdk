package com.monsterbrain.recyclerviewtableview

import android.content.Context
import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.absyz.kotlin.R
import com.absyz.kotlin.R.drawable.edit_24
import com.absyz.kotlin.databinding.AccountListitemBinding
import com.absyz.kotlin.databinding.AddressItemsBinding
import com.absyz.kotlin.model.ResponseModel.AccountsUserDetails
import com.absyz.kotlin.model.ResponseModel.ProductStorageList
import com.checkin.helpers.Utility

class AddressAdapter(val context: Context, val utility: Utility) : RecyclerView.Adapter<AddressAdapter.RowViewHolder>() {

    private var addressArrayList: List<ProductStorageList>? = null

    private lateinit var mListerner: onItemClickListener

    interface onItemClickListener{
        fun onItemCLick(position: Int)
    }

    fun setOnItemCLickListerner(listner: onItemClickListener){
        mListerner = listner
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowViewHolder {
        val binding = DataBindingUtil.inflate<AddressItemsBinding>(
            LayoutInflater.from(parent.context),
            R.layout.address_items,
            parent,
            false
        )
        return RowViewHolder(binding,mListerner)
    }

    override fun onBindViewHolder(holder: RowViewHolder, position: Int) {
        holder.binding.address.text = addressArrayList?.get(position)?.Address
        holder.binding.storeInfoProdcount.text = addressArrayList?.get(position)?.StockValue.toString()
        val stockval: Int = addressArrayList?.get(position)?.StockValue!!

        if(stockval <=2){
            holder.binding.storeInfo1.setTextColor(Color.parseColor("#ff0000"))
            holder.binding.storeInfo3.setTextColor(Color.parseColor("#ff0000"))
            holder.binding.storeInfoProdcount.setTextColor(Color.parseColor("#ff0000"))
        } else{
            holder.binding.storeInfo1.setTextColor(Color.parseColor("#757575"))
            holder.binding.storeInfo3.setTextColor(Color.parseColor("#757575"))
            holder.binding.storeInfoProdcount.setTextColor(Color.parseColor("#757575"))
        }
    }

    override fun getItemCount(): Int {
        return if (addressArrayList != null) addressArrayList!!.size else 0
    }

    fun updateList(newList: List<ProductStorageList>) {
        try {
            addressArrayList = newList as ArrayList<ProductStorageList>
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    inner class RowViewHolder(var binding: AddressItemsBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
        init{
            itemView.setOnClickListener {
                listner.onItemCLick(adapterPosition)
            }
        }
    }
}

