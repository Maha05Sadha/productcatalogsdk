package com.absyz.kotlin.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.absyz.kotlin.R
import com.absyz.kotlin.databinding.AccountListitemBinding
import com.absyz.kotlin.databinding.ColorPalletItemsBinding
import com.absyz.kotlin.databinding.LandingBrandCviewBinding
import com.absyz.kotlin.model.ResponseModel.Brands
import com.absyz.kotlin.model.ResponseModel.ProductColorCodes
import com.absyz.kotlin.model.ResponseModel.SalesTargetListDetails
import com.bumptech.glide.Glide
import com.checkin.helpers.AppConstants
import com.checkin.helpers.Utility
import com.monsterbrain.recyclerviewtableview.TargetSalesAdapter
import com.squareup.picasso.Picasso

    class ColorPalletAdapter constructor(val context: Context, val utility: Utility) : RecyclerView.Adapter<ColorPalletAdapter.RowViewHolder>() {

        private var colorPalletList: ArrayList<ProductColorCodes> = ArrayList()

        var image_id:String = ""
        var image_url : String = ""

        private lateinit var mListerner: onItemClickListener

        interface onItemClickListener{
            fun onItemCLick(position: Int)
        }

        fun setOnItemCLickListerner(listner: onItemClickListener){
            mListerner = listner
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowViewHolder {
            val binding = DataBindingUtil.inflate<ColorPalletItemsBinding>(
                LayoutInflater.from(parent.context),
                R.layout.color_pallet_items,
                parent,
                false
            )
            return RowViewHolder(binding,mListerner)
    }


    override fun onBindViewHolder(holder: RowViewHolder, position: Int) {

        try {

            val colorsList = colorPalletList!![position]
            holder.binding.colorpalletVal.setBackgroundColor(Color.parseColor("#"+colorsList.ProductColourCode))

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

        override fun getItemCount(): Int {
            return if (colorPalletList != null) colorPalletList!!.size else 0
        }

        fun updateList(newList: List<ProductColorCodes>) {
            try {
                colorPalletList = newList as ArrayList<ProductColorCodes>
                notifyDataSetChanged()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }
        inner class RowViewHolder(internal var binding: ColorPalletItemsBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
            init{
                itemView.setOnClickListener {
                    listner.onItemCLick(adapterPosition)
                }
            }
        }

    }

