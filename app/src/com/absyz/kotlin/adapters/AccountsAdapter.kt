package com.monsterbrain.recyclerviewtableview

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.absyz.kotlin.R
import com.absyz.kotlin.R.drawable.edit_24
import com.absyz.kotlin.databinding.AccountListitemBinding
import com.absyz.kotlin.model.ResponseModel.AccountsUserDetails
import com.checkin.helpers.Utility

class AccountsAdapter(val context: Context,val utility: Utility) : RecyclerView.Adapter<AccountsAdapter.RowViewHolder>() {

    private var accountsmodelArrayList: ArrayList<AccountsUserDetails> = ArrayList()

    private lateinit var mListerner: onItemClickListener

    interface onItemClickListener{
        fun onItemCLick(position: Int)
    }

    fun setOnItemCLickListerner(listner: onItemClickListener){
        mListerner = listner
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowViewHolder {
        val binding = DataBindingUtil.inflate<AccountListitemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.account_listitem,
            parent,
            false
        )
        return RowViewHolder(binding,mListerner)
    }

    private fun setHeaderBg(view: View) {
        view.setBackgroundResource(R.drawable.accounts_header_cell)
    }

    private fun setContentBg(view: View) {
        view.setBackgroundResource(R.drawable.accounts_content_cell)
    }

    override fun onBindViewHolder(holder: RowViewHolder, position: Int) {
//        val rowPos = holder.adapterPosition
//        val modal = accountsmodelArrayList?.get(rowPos - 1)

        holder.binding.accName.text = accountsmodelArrayList[position].Name
        if(accountsmodelArrayList[position].Phone.isNullOrBlank()){
            holder.binding.mobileNum.text = "-----"
        } else {
            holder.binding.mobileNum.text = accountsmodelArrayList[position].Phone
        }
        if(accountsmodelArrayList[position].Email.isNullOrBlank()){
            holder.binding.emailId.text = "-----"
        } else {
            holder.binding.emailId.text = accountsmodelArrayList[position].Email
        }
        //if(accountsmodelArrayList?.get(position)?.BillingCity.isNullOrBlank() && accountsmodelArrayList?.get(position)?.BillingState.isNullOrBlank()){

        if(accountsmodelArrayList[position].Address.isNullOrBlank()){
            holder.binding.address.text = "-----"
        } else {
            holder.binding.address.text = accountsmodelArrayList[position].Address
        }

        if(accountsmodelArrayList[position].Address.isNullOrBlank()){
            holder.binding.address.text = "-----"
        } else {
            holder.binding.address.text = accountsmodelArrayList[position].Address
        }

        if(accountsmodelArrayList[position].CreatedDate.isNullOrBlank()){
            holder.binding.createdDate.text = "-----"
        } else {
            holder.binding.createdDate.text = accountsmodelArrayList[position].CreatedDate
        }


        holder.binding.wishlist.text = accountsmodelArrayList[position].WishlistCount.toString()


//                holder.binding.createdDate.text = "Null"
//                holder.binding.wishlist.text = "Null"
        holder.binding.editIconView.setImageResource(R.drawable.edit_24)
        //Picasso.get().load(modal.editicon).into(holder.edit_icon)

        /*if (rowPos == 0) {
            // Header Cells. Main Headings appear here
            holder.itemView.apply {
                setHeaderBg(holder.binding.nameLay)
                setHeaderBg(holder.binding.accName)
                setHeaderBg(holder.binding.numLay)
                setHeaderBg(holder.binding.mobileNum)
                setHeaderBg(holder.binding.emailLay)
                setHeaderBg(holder.binding.emailId)
                setHeaderBg(holder.binding.addressLay)
                setHeaderBg(holder.binding.address)
//                setHeaderBg(holder.binding.createdDate)
//                setHeaderBg(holder.binding.wishlist)
                setHeaderBg(holder.binding.viewLay)

                holder.binding.accName.text = "Account Name"
                holder.binding.mobileNum.text = "Mobile Number "
                holder.binding.emailId.text = "Email ID"
                holder.binding.address.text = "Address"
//                holder.binding.createdDate.text = "Created Date"
//                holder.binding.wishlist.text = "Wishlist"
                holder.binding.editIconView.setImageResource(R.drawable.edit_24)
            }

        } else {
            val modal = accountsmodelArrayList?.get(rowPos - 1)
            holder.itemView.apply {
                setContentBg(holder.binding.nameLay)
                setContentBg(holder.binding.numLay)
                setContentBg(holder.binding.emailLay)
                setContentBg(holder.binding.addressLay)
//                setContentBg(holder.binding.createdDate)
//                setContentBg(holder.binding.wishlist)
                setContentBg(holder.binding.editIconView)
                setContentBg(holder.binding.viewLay)

                holder.binding.accName.text = modal?.Name
                if(modal?.Phone.isNullOrBlank()){
                    holder.binding.mobileNum.text = "-----"
                } else {
                    holder.binding.mobileNum.text = modal?.Phone
                }
                if(modal?.Email.isNullOrBlank()){
                    holder.binding.emailId.text = "-----"
                } else {
                    holder.binding.emailId.text = modal?.Email
                }
                if(modal?.BillingCity.isNullOrBlank() && modal?.BillingState.isNullOrBlank()){
                    holder.binding.address.text = "-----"
                } else {
                    holder.binding.address.text = modal?.BillingCity +", "+ modal?.BillingState
                }
//                holder.binding.createdDate.text = "Null"
//                holder.binding.wishlist.text = "Null"
                holder.binding.editIconView.setImageResource(R.drawable.edit_24)
                //Picasso.get().load(modal.editicon).into(holder.edit_icon)

            }
        }*/
    }

    override fun getItemCount(): Int {
        return if (accountsmodelArrayList != null) accountsmodelArrayList!!.size else 0
    }

    fun updateList(newList: List<AccountsUserDetails>) {
        try {
            accountsmodelArrayList = newList as ArrayList<AccountsUserDetails>
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    inner class RowViewHolder(var binding: AccountListitemBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
        init{
            itemView.setOnClickListener {
                listner.onItemCLick(adapterPosition)
            }
        }
    }
}

