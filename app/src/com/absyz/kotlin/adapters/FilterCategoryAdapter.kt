package com.monsterbrain.recyclerviewtableview

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.TypedValue
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.absyz.kotlin.R
import com.absyz.kotlin.databinding.CartlistCviewBinding
import com.absyz.kotlin.databinding.FiltercategoryListitemBinding
import com.absyz.kotlin.databinding.QtyListItemBinding
import com.absyz.kotlin.model.ResponseModel.ProductCartDetails
import com.checkin.helpers.AppConstants
import com.checkin.helpers.Utility

class FilterCategoryAdapter(val context: Context, val utility: Utility) : RecyclerView.Adapter<FilterCategoryAdapter.RowViewHolder>() {

    var spinner_category : ArrayList<String> = ArrayList()

    private lateinit var mListerner: onItemClickListener

    interface onItemClickListener{
        fun onItemCLick(position: Int)
    }

    fun setOnItemCLickListerner(listner: onItemClickListener){
        mListerner = listner
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowViewHolder {
        val binding = DataBindingUtil.inflate<FiltercategoryListitemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.filtercategory_listitem,
            parent,
            false
        )
        return RowViewHolder(binding,mListerner)
    }

    override fun onBindViewHolder(holder: RowViewHolder, position: Int) {
        val category_values = spinner_category!![position]
        holder.binding.qtyVal.text = category_values
    }

    override fun getItemCount(): Int {
        return if (spinner_category != null) spinner_category!!.size else 0
    }

    fun updateList(newList:  ArrayList<String>) {
        try {
            spinner_category = newList
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }


    inner class RowViewHolder(var binding: FiltercategoryListitemBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
        init{
            itemView.setOnClickListener {
                listner.onItemCLick(adapterPosition)
            }

        }
    }
}


