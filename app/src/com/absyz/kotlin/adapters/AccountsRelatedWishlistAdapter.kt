package com.monsterbrain.recyclerviewtableview

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.absyz.kotlin.R
import com.absyz.kotlin.R.drawable.edit_24
import com.absyz.kotlin.adapters.ComparisonAdapter
import com.absyz.kotlin.databinding.AccountListitemBinding
import com.absyz.kotlin.databinding.AccountsWishlistCviewBinding
import com.absyz.kotlin.model.ResponseModel.AccountsUserDetails
import com.absyz.kotlin.model.ResponseModel.ComparisonDetails
import com.absyz.kotlin.model.ResponseModel.RelatedWishlistDetails
import com.checkin.helpers.AppConstants
import com.checkin.helpers.Utility

class AccountsRelatedWishlistAdapter(val context: Context, val utility: Utility,private val itemClickListener: AccountsRelatedWishlistAdapter.ItemClickListener, val onClickDelete: (Int) -> Unit) : RecyclerView.Adapter<AccountsRelatedWishlistAdapter.RowViewHolder>() {

    private var accountsrelatedList: List<RelatedWishlistDetails>? = null

    interface ItemClickListener {
        fun onItemClick(item: RelatedWishlistDetails)
    }

    private lateinit var mListerner: onItemClickListener
    var image_id:String = ""
    var image_url : String = ""
    var prod_added : String = ""

    interface onItemClickListener{
        fun onItemCLick(position: Int)
    }

    fun setOnItemCLickListerner(listner: onItemClickListener){
        mListerner = listner
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowViewHolder {
        val binding = DataBindingUtil.inflate<AccountsWishlistCviewBinding>(
            LayoutInflater.from(parent.context),
            R.layout.accounts_wishlist_cview,
            parent,
            false
        )
        return RowViewHolder(binding,mListerner)
    }

    override fun onBindViewHolder(holder: RowViewHolder, position: Int) {
//        val rowPos = holder.adapterPosition
//        val modal = accountsmodelArrayList?.get(rowPos - 1)

        val relatedwishlistListmodel = accountsrelatedList!![position]

        image_id = relatedwishlistListmodel.ProductImage
        image_url = AppConstants.BaseImageURL+image_id+"/VersionData"
        utility.loadGlide(context,holder.binding.image,image_url,0)

        holder.binding.prodName.text = relatedwishlistListmodel.ProductType
        holder.binding.prodDesc.text = relatedwishlistListmodel.ProductDescription
        holder.binding.price.text = relatedwishlistListmodel.Price.toString()

        holder.binding.addtocardBtn.setOnClickListener {

            itemClickListener.onItemClick(relatedwishlistListmodel)
            if(prod_added.equals("yes")){
                holder.binding.addedcartlayout.visibility = View.VISIBLE
                holder.binding.addtocardBtn.visibility = View.GONE
            }
        }

        holder.binding.delete.setOnClickListener {
            onClickDelete(position)
        }
    }

    override fun getItemCount(): Int {
        return if (accountsrelatedList != null) accountsrelatedList!!.size else 0
    }

    fun updateList(newList: List<RelatedWishlistDetails>) {
        try {
            accountsrelatedList = newList as ArrayList<RelatedWishlistDetails>
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun setItems(newList: List<RelatedWishlistDetails>) {
        try {
            accountsrelatedList = newList as ArrayList<RelatedWishlistDetails>
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun addtocart(productadded: String) {
        prod_added = productadded
    }

    inner class RowViewHolder(var binding: AccountsWishlistCviewBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
        init{
            itemView.setOnClickListener {
                listner.onItemCLick(adapterPosition)
            }
        }
    }
}

