package com.absyz.kotlin.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.absyz.kotlin.R
import com.absyz.kotlin.databinding.LandingContentCviewBinding
import com.absyz.kotlin.model.ResponseModel.ContentItem
import com.squareup.picasso.Picasso

class RenameAdapter(val context: Context, val content_item: List<ContentItem>): RecyclerView.Adapter<RenameAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<LandingContentCviewBinding>(
            LayoutInflater.from(parent.context),
            R.layout.landing_content_cview,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.binding.imageView?.setImageResource(content_item[position].imageUrl)
        holder.binding.itemTitle?.text = content_item[position].itemname
        holder.binding.contentDesc?.text = content_item[position].itemdec
        holder.binding.price?.text = content_item[position].itemrate


    }

    override fun getItemCount() = content_item.size

    inner class ViewHolder(var binding: LandingContentCviewBinding) : RecyclerView.ViewHolder(binding.root)

}