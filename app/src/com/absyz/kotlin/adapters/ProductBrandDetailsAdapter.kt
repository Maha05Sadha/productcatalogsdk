package com.absyz.kotlin.adapters

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.absyz.kotlin.R
import com.absyz.kotlin.databinding.LandingProdCatCviewBinding
import com.absyz.kotlin.model.ResponseModel.FilterProductDetails
import com.absyz.kotlin.model.ResponseModel.InstockProductDetails
import com.absyz.kotlin.model.ResponseModel.ProductBrandDetails
import com.absyz.kotlin.model.ResponseModel.ProductCategoryDetails
import com.checkin.helpers.AppConstants
import com.checkin.helpers.Utility

class ProductBrandDetailsAdapter constructor(val context: Context, val utility: Utility) : RecyclerView.Adapter<ProductBrandDetailsAdapter.ViewHolder>() {

        private var productbrandCategoryDetails: ArrayList<ProductBrandDetails> = ArrayList()

        var image_id:String = ""
        var image_url : String = ""
        var instock_val : String = ""

        private lateinit var mListerner: onItemClickListener

        interface onItemClickListener{
            fun onItemCLick(position: Int)
        }

        fun setOnItemCLickListerner(listner: onItemClickListener){
            mListerner = listner
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val binding = DataBindingUtil.inflate<LandingProdCatCviewBinding>(
                LayoutInflater.from(parent.context),
                R.layout.landing_prod_cat_cview,
                parent,
                false
            )
            return ViewHolder(binding,mListerner)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
                val productCatListmodel = productbrandCategoryDetails!![position]
                holder.binding.itemTitle.text = productCatListmodel.Name
                holder.binding.contentDesc.text = productCatListmodel.Dimentions
                holder.binding.price.text = productCatListmodel.Price.toString()
                instock_val = productCatListmodel.IsProductAvailable.toString()
                Log.d("instock_val ->",instock_val)
                if (instock_val.equals("true")) {
                    holder.binding.instock.text = "INSTOCK"
                    holder.binding.instock.setTextColor(ContextCompat.getColor(context, R.color.validation_green));
                } else if (instock_val.equals("false")) {
                    holder.binding.instock.text = "NOSTOCK"
                    holder.binding.instock.setTextColor(ContextCompat.getColor(context, R.color.validation_red));
                }

                image_id = productCatListmodel.ProductImage
                image_url = AppConstants.BaseImageURL + image_id + "/VersionData"
                utility.loadGlide(context, holder.binding.imageView, image_url, 0)


        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

        override fun getItemCount(): Int {
            return productbrandCategoryDetails.size
        }

        fun updateList(newList: List<ProductBrandDetails>) {
            try {
                productbrandCategoryDetails = newList as ArrayList<ProductBrandDetails>
                notifyDataSetChanged()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }
        inner class ViewHolder(internal var binding: LandingProdCatCviewBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
            init{
                itemView.setOnClickListener {
                    listner.onItemCLick(adapterPosition)
                }
            }
        }

    }

