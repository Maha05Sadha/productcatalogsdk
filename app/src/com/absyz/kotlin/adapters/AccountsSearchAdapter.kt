package com.absyz.kotlin.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.absyz.kotlin.R
import com.absyz.kotlin.databinding.AccountListitemBinding
import com.absyz.kotlin.model.ResponseModel.AccountSearchUserDetails
import com.absyz.kotlin.model.ResponseModel.SimilarProductDetails
import com.checkin.helpers.Utility

class AccountsSearchAdapter constructor(
    val context: Context,
    private val itemClickListener: ItemClickListener,
    val utility: Utility
    ) :
        RecyclerView.Adapter<AccountsSearchAdapter.ViewHolder>() {

    private var itemArrayList: List<AccountSearchUserDetails>? = null
    var image_id:String = ""
    var image_url : String = ""


    companion object {
        const val TAG = "SearchAdapter"
    }

    interface ItemClickListener {
        fun onItemClick(item: AccountSearchUserDetails)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<AccountListitemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.account_listitem,
            parent,
            false
        )
        return ViewHolder(binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            Log.e(TAG, "onBindViewHolder()")
            val searchModel = itemArrayList!![position]

            holder.binding.cardLayout.setOnClickListener {
                itemClickListener.onItemClick(searchModel)
            }

            holder.binding.accName.text = searchModel.Name
            if(searchModel.Phone.isNullOrBlank()){
                holder.binding.mobileNum.text = "-----"
            } else {
                holder.binding.mobileNum.text = searchModel.Phone
            }
            if(searchModel.Email.isNullOrBlank()){
                holder.binding.emailId.text = "-----"
            } else {
                holder.binding.emailId.text = searchModel.Email
            }
            //if(accountsmodelArrayList?.get(position)?.BillingCity.isNullOrBlank() && accountsmodelArrayList?.get(position)?.BillingState.isNullOrBlank()){

            if(searchModel.Address.isNullOrBlank()){
                holder.binding.address.text = "-----"
            } else {
                holder.binding.address.text = searchModel.Address
            }

            if(searchModel.Address.isNullOrBlank()){
                holder.binding.address.text = "-----"
            } else {
                holder.binding.address.text = searchModel.Address
            }

//            if(searchModel.CreatedDate.isNullOrBlank()){
//                holder.binding.createdDate.text = "-----"
//            } else {
//                holder.binding.createdDate.text = searchModel.CreatedDate
//            }

            holder.binding.createdDate.text = " "


//                holder.binding.createdDate.text = "Null"
//                holder.binding.wishlist.text = "Null"
            holder.binding.editIconView.setImageResource(R.drawable.edit_24)


        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "onBindViewHolder() Exception - ${ex.message}")
        }

    }

    override fun getItemCount(): Int {
        return if (itemArrayList != null) itemArrayList!!.size else 0
    }

    fun updateList(newList: List<AccountSearchUserDetails>) {
        try {
            itemArrayList = newList
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "updateList() Exception - ${ex.message}")
        }

    }

    inner class ViewHolder(internal var binding: AccountListitemBinding) :
        RecyclerView.ViewHolder(binding.root)
}