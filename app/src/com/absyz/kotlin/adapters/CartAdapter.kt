package com.monsterbrain.recyclerviewtableview

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.TypedValue
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.absyz.kotlin.R
import com.absyz.kotlin.activities.ui.ProductDetailActivity
import com.absyz.kotlin.adapters.AccountsSearchAdapter
import com.absyz.kotlin.databinding.CartlistCviewBinding
import com.absyz.kotlin.model.ResponseModel.AccountSearchUserDetails
import com.absyz.kotlin.model.ResponseModel.ProductCartDetails
import com.absyz.kotlin.model.ResponseModel.RelatedWishlistDetails
import com.checkin.helpers.AppConstants
import com.checkin.helpers.Utility

class CartAdapter(val context: Context, val utility: Utility, private val itemClickListener: ItemClickListener,private val qtyitemClickListener: QtyItemClickListener, val onClickDelete: (Int) -> Unit) : RecyclerView.Adapter<CartAdapter.RowViewHolder>() {

    private var cartList: List<ProductCartDetails>? = null

    private lateinit var mListerner: onItemClickListener
    var image_id:String = ""
    var image_url : String = ""
    var max_qty : Int = 0
    var current_qty : Int = 0
    var gross_amt : Int = 0
    var price_amt : Int = 0
    var removedPosition : Int ? = null
    var stock_val : Int = 0
    var full_qtylist = ArrayList<Int>()

    private lateinit var qtyAdapter: QuantityAdapter

    interface ItemClickListener {
        fun onItemClick(item: ProductCartDetails?)
    }

    interface QtyItemClickListener {
        fun onQtyItemClick(selected_qty: Int)
    }

    interface onItemClickListener{
        fun onItemCLick(position: Int)
    }

    fun setOnItemCLickListerner(listner: onItemClickListener){
        mListerner = listner
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowViewHolder {
        val binding = DataBindingUtil.inflate<CartlistCviewBinding>(
            LayoutInflater.from(parent.context),
            R.layout.cartlist_cview,
            parent,
            false
        )
        return RowViewHolder(binding,mListerner)
    }

    override fun onBindViewHolder(holder: RowViewHolder, position: Int) {
        val cartProductDetails = cartList!![position]

        image_id = cartProductDetails.ProductImage
        image_url = AppConstants.BaseImageURL+image_id+"/VersionData"
        utility.loadGlide(context,holder.binding.image,image_url,0)

        max_qty = cartProductDetails.TotalProductQuantity
        price_amt = cartProductDetails.Price
        current_qty = cartProductDetails.Quantity

        holder.binding.qtyRv.layoutManager = LinearLayoutManager(context)
        qtyAdapter = QuantityAdapter(context,utility!!)
        holder.binding.qtyRv.adapter = qtyAdapter

        holder.binding.prodName.text = cartProductDetails.ProductName
        holder.binding.prodId.text = cartProductDetails.Name
        holder.binding.price.text = cartProductDetails.Price.toString()
        holder.binding.qty.text = current_qty.toString()
        holder.binding.grossPrice.text = cartProductDetails.GrossTotal.toString()

        holder.binding.qty.setOnClickListener {
            holder.binding.qtyList.visibility = View.VISIBLE
            itemClickListener.onItemClick(cartProductDetails)
            qtyAdapter.updateList(full_qtylist)
            qtyAdapter.setOnItemCLickListerner(object : QuantityAdapter.onItemClickListener{
                override fun onItemCLick(position: Int) {
                    //Toast.makeText(this@CartActivity, cartProductdetails[position].Name, Toast.LENGTH_SHORT).show()
                    current_qty = full_qtylist[position]
                    holder.binding.qty.text = current_qty.toString()
                    qtyitemClickListener.onQtyItemClick(current_qty)

                    holder.binding.qtyList.visibility = View.GONE
                }
            } )
        }

        holder.binding.dropdownIcon.setOnClickListener {
            holder.binding.qtyList.visibility = View.VISIBLE
            itemClickListener.onItemClick(cartProductDetails)
            qtyAdapter.updateList(full_qtylist)
            qtyAdapter.setOnItemCLickListerner(object : QuantityAdapter.onItemClickListener{
                override fun onItemCLick(position: Int) {
                    //Toast.makeText(this@CartActivity, cartProductdetails[position].Name, Toast.LENGTH_SHORT).show()
                    current_qty = full_qtylist[position]
                    holder.binding.qty.text = current_qty.toString()
                    qtyitemClickListener.onQtyItemClick(current_qty)

                    holder.binding.qtyList.visibility = View.GONE
                }
            } )
        }

        holder.binding.qtyView.setOnClickListener {
            holder.binding.qtyList.visibility = View.VISIBLE
            itemClickListener.onItemClick(cartProductDetails)
            qtyAdapter.updateList(full_qtylist)
            qtyAdapter.setOnItemCLickListerner(object : QuantityAdapter.onItemClickListener{
                override fun onItemCLick(position: Int) {
                    //Toast.makeText(this@CartActivity, cartProductdetails[position].Name, Toast.LENGTH_SHORT).show()
                    current_qty = full_qtylist[position]
                    holder.binding.qty.text = current_qty.toString()
                    qtyitemClickListener.onQtyItemClick(current_qty)

                    holder.binding.qtyList.visibility = View.GONE
                }
            } )
        }

        holder.binding.closeBtn.setOnClickListener {
            onClickDelete(position)
        }

        /*Calculate gross*/
        //gross_amt = price_amt * current_qty
        //holder.binding.grossPrice.text = gross_amt.toString()

    }

    override fun getItemCount(): Int {
        return if (cartList != null) cartList!!.size else 0
    }

    fun updateList(newList: List<ProductCartDetails>) {
        try {
            cartList = newList as ArrayList<ProductCartDetails>
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun setItems(newList: List<ProductCartDetails>) {
        try {
            cartList = newList as ArrayList<ProductCartDetails>
            notifyDataSetChanged()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun qtyItems(newList: ArrayList<Int>){
        try {
            full_qtylist = newList
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    inner class RowViewHolder(var binding: CartlistCviewBinding, listner: onItemClickListener) : RecyclerView.ViewHolder(binding.root){
        init{
            itemView.setOnClickListener {
                listner.onItemCLick(adapterPosition)
            }

        }
    }
}


