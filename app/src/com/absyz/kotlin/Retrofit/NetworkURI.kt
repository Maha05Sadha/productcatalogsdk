package com.absyz.kotlin.Retrofit

object NetworkURI {

    /*POST*/
    const val customValidationAPI = "customerValidationAPI/"
    const val customRegistartionAPI = "CustomerRegistrationAPI/"
    const val salesTargetApi = "getSalesTagetList?PageNumber=1"
    const val customRelatedwishlistAPI = "CustomerRelatedWishListAPI/"
    const val accountsUpdateAPI = "CustomerUpdateAPI/"
    const val productCategoryDetailsAPI = "getCategoryBasedProductDetails?ProductPageNumber=1"
    const val productViewcalloutApi = "ProductsView/"
    const val productColorviewdetailsApi = "ProductColourViewDetails/"
    const val productLocationviewdetailsApi = "ProductColourViewDetails/"
    const val instockProductsApi = "InStockProductsDetails?ProductPageNumber=1"
    const val filterDetailsApi = "FilterProductDisplayDetails/"
    const val addtoCartApi = "AddToCartAPI/"
    const val cartProductrequestApi = "CustomerCartDetails/"
    const val filterProductsDetailsApi = "ProductDetailsForFilters/"
    const val compareProductsApi = "ComparisionListAPI/"
    const val similarProductApi = "ComparisonSimilarProducts/"
    const val currentVisitListApi = "CurrentVisitList/"
    const val inactiveAPi = "InActiveCustomerAPI/"
    const val visitorCreationApi = "VisitorCreationAPI/"
    const val customerCheckInApi = "CustomerCheckInCallOut/"
    const val visitorListCalloutApi = "VisitorListcallOut/"
    const val addtoCompareApi = "AddToComparison/"
    const val brandCategoryDetailsApi = "BrandProducts?ProductPageNumber=1"
    const val removeCompareApi = "RemoveProductComparison/"
    const val removeCartItemAPi = "RemoveCart"
    const val wishlistCreationApi = "CustomerWishlistCreation"
    const val wishlistRemoveApi = "RemoveWishlist/"
    const val searchAccountsAPI = "SearchAccountsAPI/"
    const val removesingleComparisonApi = "RemoveSingleComparison"
    const val increaseQtyApi = "IncreaseCartQuantity"

    /*GET*/
    const val productListCallOut= "getProductListCallOut"
    const val wishListCallOut= "getWishListCallOut"
    const val userdetails = "userLoginDetailsAPI"
    const val accountsListCallOut = "AccountsAPI?PageNumber="
    const val recentLoggedInCustomer = "RecentLoginCustomerDetails"

    const val searchproductapi = "SearchProductAPI/"

    /*Image URL*/
    const val Img_URL = "https://absyz-ab-dev-ed.my.salesforce.com/services/data/v52.0/sobjects/ContentVersion/"

}