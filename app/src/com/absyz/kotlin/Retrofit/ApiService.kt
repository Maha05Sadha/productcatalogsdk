package com.checkin.network

import com.absyz.kotlin.Retrofit.NetworkURI
import com.absyz.kotlin.model.RequestModel.*
import com.absyz.kotlin.model.ResponseModel.*
import com.absyz.kotlin.model.RequestModel.SearchItemRequest
import com.absyz.kotlin.model.ResponseModel.SearchproductResponse
import io.reactivex.Observable
import retrofit2.http.*

interface ApiService {

    @GET()
    fun userLogindetails(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
    ): Observable<UserDetailResponseModel?>?

    @POST(NetworkURI.customValidationAPI)
    fun customerValidationRequest(
        @Header("Authorization") authToken: String?,
        @Body body: CustomerValidationRequestModel
    ): Observable<CustomerValidationResponseModel?>?

    @POST(NetworkURI.customRegistartionAPI)
    fun customerRegistrationRequest(
        @Header("Authorization") authToken: String?,
        @Body body: CustomerRegistrationRequest
    ): Observable<CustomerRegistrationResponse?>?

    @POST(NetworkURI.salesTargetApi)
    fun salesTargetRequest(
        @Header("Authorization") authToken: String?,
        @Body body: SalesTargetRequestModel
    ): Observable<SalesTargetResponseModel?>?

    @POST(NetworkURI.currentVisitListApi)
    fun currentVisitRequest(
        @Header("Authorization") authToken: String?,
        @Body body: CurrentVisitListRequest
    ): Observable<CurrentVistListResponse?>?

    @POST(NetworkURI.visitorCreationApi)
    fun visitCreationRequest(
        @Header("Authorization") authToken: String?,
        @Body body: VisitorCreationRequest
    ): Observable<VisitorCreationResponse?>?

    @POST(NetworkURI.customerCheckInApi)
    fun customerCheckInRequest(
        @Header("Authorization") authToken: String?,
        @Body body: CustomerCheckInRequest
    ): Observable<CustomerCheckInResponse?>?

    @POST(NetworkURI.inactiveAPi)
    fun inActiveRequest(
        @Header("Authorization") authToken: String?,
        @Body body: InActiveCustomerRequest
    ): Observable<InActiveCustomerResponse?>?

    @GET()
    fun landingProductsRequest(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
    ): Observable<LandingMainResponseModel?>?

    @GET()
    fun wishlistProductsRequest(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
    ): Observable<WishlistResponseModel?>?

    @GET()
    fun accountsRequest(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
    ): Observable<AccountsResponseModel?>?

    @POST(NetworkURI.customRelatedwishlistAPI)
    fun customerRelatedWishlistRequest(
        @Header("Authorization") authToken: String?,
        @Body body: CustomerRelatedRequestModel
    ): Observable<CustomerRelatedWishlistResponse?>?

    @POST(NetworkURI.visitorListCalloutApi)
    fun visitorListCalloutRequest(
        @Header("Authorization") authToken: String?,
        @Body body: VisitLogsRequestModel
    ): Observable<VisitLogsResponseModel?>?

    @GET()
    fun recentCustomerdetails(
        @Url s: String?,
        @Header("Authorization") authToken: String?,
    ): Observable<RecentLoginCustomerResponse?>?

    @POST(NetworkURI.searchproductapi)
    fun productSearchitem(
        @Header("Authorization") authToken: String?,
        @Body body: SearchItemRequest
    ): Observable<SearchproductResponse?>?

    @POST(NetworkURI.searchAccountsAPI)
    fun accountSearchitem(
        @Header("Authorization") authToken: String?,
        @Body body: AccountSearchRequest
    ): Observable<AccountSearchResponse?>?

    @POST(NetworkURI.accountsUpdateAPI)
    fun accountsUpdateRequest(
        @Header("Authorization") authToken: String?,
        @Body body: AccountsUpdateRequestModel
    ): Observable<AccountsUpdateResponseModel?>?

    @POST(NetworkURI.productCategoryDetailsAPI)
    fun productCategoryDetailsRequest(
        @Header("Authorization") authToken: String?,
        @Body body: ProductCategoryDetailsRequestModel
    ): Observable<ProductCategoryDetailsResponseModel?>?

    @POST(NetworkURI.brandCategoryDetailsApi)
    fun brandCategoryDetailsRequest(
        @Header("Authorization") authToken: String?,
        @Body body: BrandProductsRequest
    ): Observable<BrandProductsResponse?>?

    @POST(NetworkURI.instockProductsApi)
    fun inStockProductDetailsRequest(
        @Header("Authorization") authToken: String?,
        @Body body: InstockRequestModel
    ): Observable<InstockResponseModel?>?

    @POST(NetworkURI.productViewcalloutApi)
    fun productViewCalloutRequest(
        @Header("Authorization") authToken: String?,
        @Body body: ProductViewCallOutRequest
    ): Observable<ProductViewCalloutResponse?>?

    @POST(NetworkURI.productColorviewdetailsApi)
    fun productColorDetailsRequest(
        @Header("Authorization") authToken: String?,
        @Body body: ProductColorViewRequest
    ): Observable<ProductColorViewDetailResponse?>?

    @POST(NetworkURI.wishlistCreationApi)
    fun wishlistCreationDetailsRequest(
        @Header("Authorization") authToken: String?,
        @Body body: WishlistCreationRequest
    ): Observable<WishlistCreationResponse?>?

    @POST(NetworkURI.productLocationviewdetailsApi)
    fun productLocationDetailsRequest(
        @Header("Authorization") authToken: String?,
        @Body body: ProductColorViewRequest
    ): Observable<ProductColorViewDetailResponse?>?

    @POST(NetworkURI.filterDetailsApi)
    fun filterDetailsRequest(
        @Header("Authorization") authToken: String?,
        @Body body: FilterRequestModel
    ): Observable<FilterDropDownResponseModel?>?

    @POST(NetworkURI.addtoCartApi)
    fun addToCartRequest(
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
        @Body body: AddtoCartRequest
    ): Observable<AddtoCartResponse>?


    @POST(NetworkURI.addtoCompareApi)
    fun addToComapreRequest(
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
        @Body body: AddtoCompareRequest
    ): Observable<AddtoCompareResponse>?

    @POST(NetworkURI.removeCompareApi)
    fun removeComapreRequest(
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
        @Body body: RemoveComparisonRequest
    ): Observable<RemoveComparisonResponse>?

    @POST(NetworkURI.wishlistRemoveApi)
    fun removeWishlistRequest(
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
        @Body body: RemoveWishlistRequest
    ): Observable<RemoveWishlistResponse>?

    @POST(NetworkURI.increaseQtyApi)
    fun increaseQtyRequest(
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
        @Body body: IncreaseProductQtyRequest
    ): Observable<IncreaseProductQtyResponse>?

    @POST(NetworkURI.inactiveAPi)
    fun InActiveCustomerRequest(
        @Header("Authorization") authToken: String?,
        @Header("Content-Type") contentType: String?,
        @Body body: InActiveCustomerRequest
    ): Observable<InActiveCustomerResponse>?

    @POST(NetworkURI.cartProductrequestApi)
    fun cartProductDetailRequest(
        @Header("Authorization") authToken: String?,
        @Body body: CartProductRequest
    ): Observable<CartProductResponse?>?

    @POST(NetworkURI.removeCartItemAPi)
    fun removecartProductDetailRequest(
        @Header("Authorization") authToken: String?,
        @Body body: RemoveCartItemRequest
    ): Observable<RemoveCartItemResponse?>?

    @POST(NetworkURI.removesingleComparisonApi)
    fun removesingleProductComparisonDetailRequest(
        @Header("Authorization") authToken: String?,
        @Body body: RemoveSingleComparisonRequest
    ): Observable<RemoveSingleComparisonResponse?>?

    @POST(NetworkURI.compareProductsApi)
    fun compareProductDetailRequest(
        @Header("Authorization") authToken: String?,
        @Body body: ComparisonRequest
    ): Observable<ComparisonResponse?>?

    @POST(NetworkURI.similarProductApi)
    fun similarProductDetailRequest(
        @Header("Authorization") authToken: String?,
        @Body body: SimilarProductRequest
    ): Observable<SimilarProductResponse?>?


    @POST(NetworkURI.filterProductsDetailsApi)
    fun filteredProductRequest(
        @Header("Authorization") authToken: String?,
        @Body body: FilterProductListRequest
    ): Observable<FilterProductListResponse?>?

}
