package com.checkin.network

import com.absyz.kotlin.Retrofit.NetworkURI
import com.absyz.kotlin.model.RequestModel.*
import com.absyz.kotlin.model.ResponseModel.*
import com.absyz.kotlin.model.RequestModel.SearchItemRequest
import com.absyz.kotlin.model.ResponseModel.SearchproductResponse
import com.checkin.helpers.AppConstants
import io.reactivex.Observable

class Repository(private val apiService: ApiService) {

    fun getUserdetails(authtoken: String?): Observable<UserDetailResponseModel?>? =
        apiService.userLogindetails(AppConstants.BaseURL + NetworkURI.userdetails,
            authtoken)

    fun getCustomerValidation(authtoken: String?,
                              sendCustomerValidationBody: CustomerValidationRequestModel): Observable<CustomerValidationResponseModel?>? =
         apiService.customerValidationRequest(authtoken,sendCustomerValidationBody)

    fun getProductSearch(authtoken: String?, senditemsearch: SearchItemRequest): Observable<SearchproductResponse?>? =
         apiService.productSearchitem(authtoken,senditemsearch)

    fun getAccountSearch(authtoken: String?, senditemsearch: AccountSearchRequest): Observable<AccountSearchResponse?>? =
        apiService.accountSearchitem(authtoken,senditemsearch)


    fun getCustomerRegistration(authtoken: String?,
                              sendCustomerRegistrationBody: CustomerRegistrationRequest): Observable<CustomerRegistrationResponse?>? =
        apiService.customerRegistrationRequest(
            authtoken,sendCustomerRegistrationBody)

    fun getRecentCustomerdetails(authtoken: String?): Observable<RecentLoginCustomerResponse?>? =
        apiService.recentCustomerdetails(AppConstants.BaseURL + NetworkURI.recentLoggedInCustomer,
            authtoken)

    fun getSalesTargetDetails(
        authtoken: String,
        pagenumer: String,
        salesTargetRequestModel:SalesTargetRequestModel): Observable<SalesTargetResponseModel?> ?=
        apiService.salesTargetRequest(authtoken, salesTargetRequestModel)

    fun getCurrentVisitListDetails(
        authtoken: String,
        currentvisitreq:CurrentVisitListRequest): Observable<CurrentVistListResponse?> ?=
        apiService.currentVisitRequest(authtoken, currentvisitreq)

    fun getVisitorCreationDetails(
        authtoken: String,
        visitorreq:VisitorCreationRequest): Observable<VisitorCreationResponse?> ?=
        apiService.visitCreationRequest(authtoken, visitorreq)

    fun getCustomerCheciInDetails(
        authtoken: String,
        postReq:CustomerCheckInRequest): Observable<CustomerCheckInResponse?> ?=
        apiService.customerCheckInRequest(authtoken, postReq)

    fun getInActiveDetails(
        authtoken: String,
        inactivereq:InActiveCustomerRequest): Observable<InActiveCustomerResponse?> ?=
        apiService.inActiveRequest(authtoken, inactivereq)

    fun getLandingDetails(authtoken: String): Observable<LandingMainResponseModel?> ?=
        apiService.landingProductsRequest(AppConstants.BaseURL + NetworkURI.productListCallOut,authtoken)

    fun getProductCategoryDetails(authtoken: String?,
                                   productCAtegoryRequestID: ProductCategoryDetailsRequestModel): Observable<ProductCategoryDetailsResponseModel?>? =
        apiService.productCategoryDetailsRequest(authtoken,productCAtegoryRequestID)

    fun getBrandCategoryDetails(authtoken: String?,
                                  brandCAtegoryRequestID: BrandProductsRequest): Observable<BrandProductsResponse?>? =
        apiService.brandCategoryDetailsRequest(authtoken,brandCAtegoryRequestID)

    fun getInstockProductDetails(authtoken: String?,
                                  instockReqBody: InstockRequestModel): Observable<InstockResponseModel?>? =
        apiService.inStockProductDetailsRequest(authtoken,instockReqBody)

    fun getFilterDetails(authtoken: String?,
                                 filterReqBody: FilterRequestModel): Observable<FilterDropDownResponseModel?>? =
        apiService.filterDetailsRequest(authtoken,filterReqBody)

    fun getFiltedProducts(authtoken: String?,
                         filteredprodReqBody: FilterProductListRequest): Observable<FilterProductListResponse?>? =
        apiService.filteredProductRequest(authtoken,filteredprodReqBody)


    fun getProductViewCallOut(authtoken: String?,
                                  productviewCalloutReq: ProductViewCallOutRequest): Observable<ProductViewCalloutResponse?>? =
        apiService.productViewCalloutRequest(authtoken,productviewCalloutReq)

    fun getProductColorDetails(authtoken: String?,
                                  productColorRequestID: ProductColorViewRequest): Observable<ProductColorViewDetailResponse?>? =
        apiService.productColorDetailsRequest(authtoken,productColorRequestID)

    fun getWishlistCreationDetails(authtoken: String?,
                               wishlistReq: WishlistCreationRequest): Observable<WishlistCreationResponse?>? =
        apiService.wishlistCreationDetailsRequest(authtoken,wishlistReq)

    fun getProductLocationDetails(authtoken: String?,
                               productColorRequestID: ProductColorViewRequest): Observable<ProductColorViewDetailResponse?>? =
        apiService.productLocationDetailsRequest(authtoken,productColorRequestID)

    fun getAddtoCartDetails(authtoken: String?,
                               addtocartReq: AddtoCartRequest): Observable<AddtoCartResponse>? =
        apiService.addToCartRequest(authtoken,"application/json",addtocartReq)

    fun getAddtoCompareDetails(authtoken: String?,
                            addtocompareReq: AddtoCompareRequest): Observable<AddtoCompareResponse>? =
        apiService.addToComapreRequest(authtoken,"application/json",addtocompareReq)

    fun getRemoveCompareDetails(authtoken: String?,
                               removeReq: RemoveComparisonRequest): Observable<RemoveComparisonResponse>? =
        apiService.removeComapreRequest(authtoken,"application/json",removeReq)

    fun getRemoveWishlistDetails(authtoken: String?,
                                removeReq: RemoveWishlistRequest): Observable<RemoveWishlistResponse>? =
        apiService.removeWishlistRequest(authtoken,"application/json",removeReq)

    fun getIncreaseQtyDetails(authtoken: String?,
                                 removeReq: IncreaseProductQtyRequest): Observable<IncreaseProductQtyResponse>? =
        apiService.increaseQtyRequest(authtoken,"application/json",removeReq)

    fun getInActiveCustomerDetails(authtoken: String?,
                                removeReq: InActiveCustomerRequest): Observable<InActiveCustomerResponse>? =
        apiService.InActiveCustomerRequest(authtoken,"application/json",removeReq)

    fun getCartProductDetails(authtoken: String?,
                                   cartProdReq: CartProductRequest): Observable<CartProductResponse?>? =
        apiService.cartProductDetailRequest(authtoken,cartProdReq)

    fun getRemoveCartProductDetails(authtoken: String?,
                              removecartProdReq: RemoveCartItemRequest): Observable<RemoveCartItemResponse?>? =
        apiService.removecartProductDetailRequest(authtoken,removecartProdReq)

    fun getRemoveSingleProductProductDetails(authtoken: String?,
                                    removecartProdReq: RemoveSingleComparisonRequest): Observable<RemoveSingleComparisonResponse?>? =
        apiService.removesingleProductComparisonDetailRequest(authtoken,removecartProdReq)

    fun getCompareDetails(authtoken: String?,
                              compareProdReq: ComparisonRequest): Observable<ComparisonResponse?>? =
        apiService.compareProductDetailRequest(authtoken,compareProdReq)

    fun getSimilarProductDetails(authtoken: String?,
                          similarProdReq: SimilarProductRequest): Observable<SimilarProductResponse?>? =
        apiService.similarProductDetailRequest(authtoken,similarProdReq)

    fun wishlistProductsRequest(authtoken: String): Observable<WishlistResponseModel?> ?=
        apiService.wishlistProductsRequest(AppConstants.BaseURL + NetworkURI.wishListCallOut,authtoken, "application/json" )

    fun getAccountsDetails(authtoken: String,pagenumer: String): Observable<AccountsResponseModel?> ?=
        apiService.accountsRequest(AppConstants.BaseURL + NetworkURI.accountsListCallOut+pagenumer,authtoken)

    fun getCustomerRelatedwishlist(authtoken: String?,
                              sendCustomerrelatedBody: CustomerRelatedRequestModel): Observable<CustomerRelatedWishlistResponse?>? =
        apiService.customerRelatedWishlistRequest(authtoken,sendCustomerrelatedBody)

    fun getVisitorLogDetails(authtoken: String?,
                                   PostReq: VisitLogsRequestModel): Observable<VisitLogsResponseModel?>? =
        apiService.visitorListCalloutRequest(authtoken,PostReq)

    fun getAccountsUpdate(authtoken: String?,
                                sendAccountsUpdateBody: AccountsUpdateRequestModel): Observable<AccountsUpdateResponseModel?>? =
        apiService.accountsUpdateRequest(
            authtoken,sendAccountsUpdateBody)

    /*fun validateOTP(request: OTPRequest): Observable<ValidateResponse> =
        apiService.validateOTP(request)

    fun validateUser(request: ValidateRequest): Observable<ValidateUserResponse> =
        apiService.validateUser(request)

    fun getHealthProfessional(
        token: String,
        queryMap: HashMap<String, String>
    ): Observable<List<VaccineItem>> = apiService.getHealthProfessional(token, queryMap)

    fun getVaccineCenters(queryMap: HashMap<String, String>): Observable<List<VaccineCenterModel>> =
        apiService.getVaccineCenters(queryMap)

    fun validateClient(request: ValidateClientRequest): Observable<ValidateClientResponse> =
        apiService.validateClient(request)

    fun vaccineCheckIn(
        token: String,
        request: VaccineCheckInRequest
    ): Observable<ValidateResponse> =
        apiService.vaccineCheckIn(token, request)

    fun searchUsersDetails(searchRequest: SearchRequest): Observable<List<SearchItem>?> =
        apiService.searchUsersDetails(searchRequest)

    fun generateQrCode(token: String = "", data: String? = ""): Observable<GenerateQrResponse> =
        apiService.generateQrCode(token, data)

     */

}
