package com.checkin.helpers

import android.Manifest
import androidx.multidex.BuildConfig
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

object AppConstants {

    var ACCESS_TOKEN = ""
    var REFRESH_TOKEN = ""
    var BaseURL = "https://absyz-ab-dev-ed.my.salesforce.com/services/apexrest/"
    var BaseImageURL = "https://absyz-ab-dev-ed.my.salesforce.com/services/data/v52.0/sobjects/ContentVersion/"
    var SalesPersonID = ""
    var cartItemCount : Int = 0
    var compareItemCount : Int = 0
    var CustomerID = ""
    var  Customer_Name = ""

    const val THEMECOLOR = "#274493"


    const val sizeKB: Long = 1024
    const val sizeMB = sizeKB * sizeKB
    const val sizeGB = sizeMB * sizeKB
    const val sizeTB = sizeGB * sizeKB
    const val size2GBInMB = sizeKB * 2

    const val ORDER_ASC = "asc"
    const val ORDER_DESC = "desc"
    const val ALL_STORES = true

    var APP_LANG = "en"
    var IS_APP_LANG_CHANGED = false

    const val EXTRA_OTP_TOKEN = "extra otp token"
    const val HEADER_KEY_AUTH = "x-auth-token"
    const val HEADER_FOR_YOU = "Token"
    const val HEADER_AUTHORIZATION = "Authorization"

    const val ARN_NOTIFICATION = "arn:aws:sns:us-east-1:238527534465:app/GCM/Painters"
    const val APP_LINK =
        "https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}"

    fun setLang(lang: String) {
        APP_LANG = lang
    }

    fun setAccessToken(token: String) {
        ACCESS_TOKEN = token
    }

    fun setIsLangChanged(isChaged: Boolean) {
        IS_APP_LANG_CHANGED = isChaged
    }

}
