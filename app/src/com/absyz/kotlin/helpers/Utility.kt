package com.checkin.helpers


import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.location.LocationManager
import android.media.MediaScannerConnection
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.telephony.TelephonyManager
import android.text.Html
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.ColorRes
import androidx.annotation.RawRes
import androidx.core.content.ContextCompat
import androidx.core.widget.CompoundButtonCompat
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.absyz.kotlin.R
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.helpers.RecyclerLinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.request.RequestOptions

import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import org.apache.commons.lang3.StringUtils
import java.io.*
import java.nio.charset.StandardCharsets
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class Utility(context: Context) {

    //private constructor to avoid client applications to use constructor

    @set:Inject
    var sessionStore: SessionStore


    val countryCode: String?
        get() {
            var mobile: String? = ""
            try {
                mobile = sessionStore.getString(SessionStore.COUNTRY_CODE, "")
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            return mobile
        }

    companion object {

        //private static final Utility instance = new Utility();
        private val TAG = "Utility"

        private val gson = Gson()

        fun getDesiredDateFormatFromUTC(
            existFormat: String,
            desiredFormat: String,
            timeString: String
        ): String {
            var desiredDate = ""
            try {

                val existSDFormat = SimpleDateFormat(existFormat, Locale.ENGLISH)//HH:mm:ss
                existSDFormat.timeZone = TimeZone.getTimeZone("UTC")
                //existSDFormat.setTimeZone(TimeZone.getDefault());
                val date = existSDFormat.parse(timeString)

                val desiredSDFormat = SimpleDateFormat(desiredFormat)
                //desiredSDFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                desiredSDFormat.timeZone = TimeZone.getDefault()

                desiredDate = desiredSDFormat.format(date)

            } catch (ex: Exception) {
                ex.printStackTrace()
                desiredDate = timeString
            }

            return desiredDate
        }

    }

    init {
        Log.e("Utility", " initialized-")
        sessionStore = (context as ApplicationController).applicationComponent!!.sessionStore()
    }

    /*public static Utility getInstance() {
        return instance;
    }*/

    /*Progress dialog*/
    /*fun getProgressDialogCircle(context: Context?): Dialog? {
        var dialog: Dialog? = null
        try {
            dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.dialog_loading_view)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCanceledOnTouchOutside(false)
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
        return dialog
    }*/

    fun getGson(): Gson {
        return gson
    }

    fun share(context: Context, title: String, extra: String) {
        try {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT, extra)
            intent.putExtra(Intent.EXTRA_SUBJECT, title)
            context.startActivity(Intent.createChooser(intent, "Share"))
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun MergeColors(backgroundColor: Int, foregroundColor: Int): Int {
        val ALPHA_CHANNEL: Int = 24
        val RED_CHANNEL: Int = 16
        val GREEN_CHANNEL: Int = 8
        val BLUE_CHANNEL: Int = 0
        var a = 0
        var r = 0
        var g = 0
        var b = 0
        try {
            val ap1 = (backgroundColor shr ALPHA_CHANNEL and 0xff).toDouble() / 255.0
            val ap2 = (foregroundColor shr ALPHA_CHANNEL and 0xff).toDouble() / 255.0
            val ap = ap2 + ap1 * (1 - ap2)

            val amount1 = ap1 * (1 - ap2) / ap
            val amount2 = amount1 / ap

            a = (ap * 255.0).toInt() and 0xff

            r =
                ((backgroundColor shr RED_CHANNEL and 0xff).toFloat() * amount1 + (foregroundColor shr RED_CHANNEL and 0xff).toFloat() * amount2).toInt() and 0xff
            g =
                ((backgroundColor shr GREEN_CHANNEL and 0xff).toFloat() * amount1 + (foregroundColor shr GREEN_CHANNEL and 0xff).toFloat() * amount2).toInt() and 0xff
            b =
                ((backgroundColor and 0xff).toFloat() * amount1 + (foregroundColor and 0xff).toFloat() * amount2).toInt() and 0xff

        } catch (ex: Exception) {
            ex.printStackTrace()
        }


        return a shl ALPHA_CHANNEL or (r shl RED_CHANNEL) or (g shl GREEN_CHANNEL) or (b shl BLUE_CHANNEL)
    }

    private fun IsInternetConnected(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    fun showToast(context: Context, message: String?) {
        try {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun isSimSupport(context: Context): Boolean {
        var isAvailable = false
        try {
            val telephonyManager =
                context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager  //gets the current TelephonyManager
            isAvailable = telephonyManager.simState == TelephonyManager.SIM_STATE_READY
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return isAvailable
    }

    private fun setTransparentToolbar(activity: Activity) {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(activity, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true)
        }
        if (Build.VERSION.SDK_INT >= 19) {
            activity.window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(activity, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
            activity.window.statusBarColor = Color.TRANSPARENT
        }
    }

    private fun setWindowFlag(activity: Activity, bits: Int, on: Boolean) {
        val win = activity.window
        val winParams = win.attributes
        if (on) {
            winParams.flags = winParams.flags or bits
        } else {
            winParams.flags = winParams.flags and bits.inv()
        }
        win.attributes = winParams
    }

    /*fun supportActionBar(
        activity: Activity,
        actionBar: ActionBar,
        color_primary: String,
        toolbarTitleTV: TextView,
        title: String,
        isHomeActivity: Boolean) {
        var color_dark: String
        try {
            color_dark = "COlor code"

            actionBar.title = ""

            if (isHomeActivity) {
                try {
                    val upArrow = activity.resources.getDrawable(R.drawable.hamburger)
                    upArrow.setColorFilter(
                        activity.resources.getColor(R.color.white),
                        PorterDuff.Mode.SRC_ATOP
                    )
                    actionBar.setHomeAsUpIndicator(upArrow)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            } else {
                //toolbarTitleTV.setTextSize(activity.getResources().getDimension(R.dimen.toolbar_title_10sp));
                try {
                    val upArrow = activity.resources.getDrawable(R.drawable.back_button)
                    upArrow.setColorFilter(
                        activity.resources.getColor(R.color.white),
                        PorterDuff.Mode.SRC_ATOP
                    )
                    actionBar.setHomeAsUpIndicator(upArrow)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeButtonEnabled(true)
            actionBar.elevation = 0f
            toolbarTitleTV.text = title

            try {
                actionBar.setBackgroundDrawable(ColorDrawable(Color.parseColor(color_primary)))
            } catch (e: Exception) {
                e.printStackTrace()
            }

            try {
                val color =
                    MergeColors(Color.parseColor(color_primary), Color.parseColor("#33000000"))
                color_dark = String.format("#%06X", 0xFFFFFF and color)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = activity.window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = Color.parseColor(color_dark)
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }*/

    fun setImageBackgroundTint(imageView: ImageView, colorCode: String) {
        try {
            if (Build.VERSION.SDK_INT <= 22) {
                imageView.background.setColorFilter(
                    Color.parseColor(colorCode),
                    PorterDuff.Mode.SRC_ATOP
                )
            } else {
                imageView.backgroundTintList = ColorStateList.valueOf(Color.parseColor(colorCode))
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun setImageSRCTint(context: Context, imageView: ImageView, @ColorRes id: Int) {
        try {
            imageView.setColorFilter(ContextCompat.getColor(context, id), PorterDuff.Mode.SRC_IN)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun GetMilliSecondsFromDate(date: String, SIMPLE_DATE_FORMAT: String): Long {
        var timeInMilliseconds: Long = 0
        val sdfDate: SimpleDateFormat
        try {

            sdfDate = SimpleDateFormat(SIMPLE_DATE_FORMAT, Locale.ENGLISH)

            try {
                val mDate = sdfDate.parse(date)
                timeInMilliseconds = mDate.time
                Log.i(TAG, " $timeInMilliseconds")
            } catch (e: ParseException) {
                e.printStackTrace()
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return timeInMilliseconds
    }

    fun GetCurrentDate(SIMPLE_DATE_FORMAT: String): String {
        var strDate = ""
        try {
            val sdfDate = SimpleDateFormat(SIMPLE_DATE_FORMAT, Locale.ENGLISH)//dd/MM/yyyy
            val now = Date()
            strDate = sdfDate.format(now)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return strDate
    }

    fun getNumberOfDaysFromUTC(
        existFormat: String,
        timeString: String
    ): Int {
        var desiredDate = 0
        try {

            val existSDFormat = SimpleDateFormat(existFormat, Locale.ENGLISH)//HH:mm:ss
            existSDFormat.timeZone = TimeZone.getTimeZone("UTC")
            //existSDFormat.setTimeZone(TimeZone.getDefault());
            val dateEnd = existSDFormat.parse(timeString)

            val currentDate = Date()

            if (dateEnd.after(currentDate)) {
                val diff: Long = dateEnd.getTime() - currentDate.getTime()
                val numberDays = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
                desiredDate = numberDays.toInt()
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return desiredDate
    }

    fun getUTCDateFormat(existFormat: String, desiredFormat: String, timeString: String): String {
        var desiredDate = ""
        var date: Date
        try {

            val existSDFormat = SimpleDateFormat(existFormat)//HH:mm:ss
            //existSDFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            existSDFormat.timeZone = TimeZone.getDefault()
            date = existSDFormat.parse(timeString)
            val calendar = Calendar.getInstance()
            calendar.time = date
            calendar.add(Calendar.SECOND, 59)
            date = calendar.time

            val desiredSDFormat = SimpleDateFormat(desiredFormat)
            desiredSDFormat.timeZone = TimeZone.getTimeZone("UTC")
            //desiredSDFormat.setTimeZone(TimeZone.getDefault());

            desiredDate = desiredSDFormat.format(date)

        } catch (ex: Exception) {
            ex.printStackTrace()
            desiredDate = timeString
        }

        return desiredDate
    }

    fun getTimeInDate(SIMPLE_DATE_FORMAT: String, timeString: String): Date? {
        var date: Date? = null
        try {
            val simpleDateFormat = SimpleDateFormat(SIMPLE_DATE_FORMAT, Locale.ENGLISH)//HH:mm:ss
            date = simpleDateFormat.parse(timeString)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return date
    }


    fun isPastDate(SIMPLE_DATE_FORMAT: String, timeString: String): Boolean {
        var date: Date? = null
        var isPast = false
        try {
            val simpleDateFormat = SimpleDateFormat(SIMPLE_DATE_FORMAT, Locale.ENGLISH)//HH:mm:ss
            simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
            date = simpleDateFormat.parse(timeString)
            val currentDate = Date()
            Log.e(TAG, "isPastDate - date - " + simpleDateFormat.format(date))
            Log.e(TAG, "isPastDate - date current - " + simpleDateFormat.format(currentDate))
            isPast =
                date!!.before(currentDate) || date.compareTo(currentDate) == 0//will return true if date is past or present
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return isPast
    }

    fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        try {
            if (height > reqHeight || width > reqWidth) {
                val heightRatio = Math.round(height.toFloat() / reqHeight.toFloat())
                val widthRatio = Math.round(width.toFloat() / reqWidth.toFloat())
                inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
            }
            val totalPixels = (width * height).toFloat()
            val totalReqPixelsCap = (reqWidth * reqHeight * 2).toFloat()

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return inSampleSize
    }

    /*fun setToolBar(
        activity: Activity,
        actionBar: ActionBar,
        toolbar: Toolbar,
        title: String,
        isCanelBtn: Boolean) {
        try {
            actionBar.setDisplayShowHomeEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowTitleEnabled(true)
            actionBar.title = title

            if (isCanelBtn) {
                try {
                    val upArrow = activity.resources.getDrawable(R.drawable.back_button)
                    upArrow.setColorFilter(
                        activity.resources.getColor(R.color.colorPrimary),
                        PorterDuff.Mode.SRC_ATOP
                    )
                    actionBar.setHomeAsUpIndicator(upArrow)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            } else {
                val upArrow = activity.resources.getDrawable(R.drawable.back_button)
                upArrow.setColorFilter(
                    activity.resources.getColor(R.color.colorPrimary),
                    PorterDuff.Mode.SRC_ATOP
                )
                actionBar.setHomeAsUpIndicator(upArrow)
            }
            applyFontForToolbarTitle(activity, toolbar)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }*/

    fun applyFontForToolbarTitle(context: Activity, toolbar: Toolbar) {
        try {
            for (i in 0 until toolbar.childCount) {
                val view = toolbar.getChildAt(i)
                if (view is TextView) {
                    val titleFont = RobotoTextView.getRoboto(context, 3)
                    if (view.text == toolbar.title) {
                        view.typeface = titleFont
                        break
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun tabsFontChange(context: Context, tabLayout: TabLayout) {
        try {
            val vg = tabLayout.getChildAt(0) as ViewGroup
            val tabsCount = vg.childCount
            val titleFont = RobotoTextView.getRoboto(context, RobotoTextView.Roboto.ROBOTO_BOLD)
            for (j in 0 until tabsCount) {
                val vgTab = vg.getChildAt(j) as ViewGroup
                val tabChildsCount = vgTab.childCount
                for (i in 0 until tabChildsCount) {
                    val tabViewChild = vgTab.getChildAt(i)
                    if (tabViewChild is TextView) {
                        tabViewChild.typeface = titleFont
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun buttonFontChange(context: Context, button: Button) {
        try {
            val titleFont = RobotoTextView.getRoboto(context, 2)
            button.typeface = titleFont
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun editTextFontChange(context: Context, view: EditText) {
        try {
            val titleFont = RobotoTextView.getRoboto(context, 2)
            view.typeface = titleFont
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun setRadioButtonColor(radioButton: RadioButton, checkedColor: Int, uncheckedColor: Int) {
        try {
            val states = arrayOf(intArrayOf(android.R.attr.state_checked), intArrayOf())
            val colors = intArrayOf(checkedColor, uncheckedColor)
            CompoundButtonCompat.setButtonTintList(radioButton, ColorStateList(states, colors))
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    /*fun showNetworkErrorMessage(context: Context, exception: String) {
        val messageBody: String
        try {
            if (exception.contains(AppConstants.SOCKET_TIMEOUT)) {
                messageBody = context.getString(R.string.internet_slow)
            } else if (exception.contains(AppConstants.INVALID_HOSTNAME) || exception.contains(
                    AppConstants.CONNECTION_GONE
                )
                || exception.contains(AppConstants.NO_CONNECTION)
            ) {
                messageBody = context.getString(R.string.error_no_internet_connection)
            } else {
                messageBody = exception
            }
            alertMessageDialog(context, messageBody)
            //Utility.showToast(context, messageBody);
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }*/

    fun saveImageGallery(
        context: Context,
        bitmap: Bitmap,
        filename: String?,
        imgActualPath: String?
    ): String {
        var file: File? = null
        val galleryDirectory: File
        val directory: File
        val root: String
        val byteArrayData: ByteArray
        val localBitMap: Bitmap
        val fos: FileOutputStream
        var isGallery = false
        try {
            root = Environment.getExternalStorageDirectory().toString()
            directory = File(root + "/" + context.getString(R.string.app_name))
            if (!directory.exists())
                directory.mkdirs()

            /* galleryDirectory = directory.getParentFile();
            galleryDirectory = new File(directory + "/" + context.getString(R.string.app_name) + " Images");
            if (!galleryDirectory.exists())
                galleryDirectory.mkdirs();*/

            if (filename != null) {
                if (filename.toString().endsWith(".jpg")
                    || filename.toString().endsWith(".png")
                    || filename.toString().endsWith(".jpeg")
                ) {

                    file = File(directory, filename)
                    isGallery = true
                }
            }
            if (file != null && file.exists())
                file.delete()
            try {
                if (file != null) {
                    fos = FileOutputStream(file, false)
                    if (isGallery) {
                        if (imgActualPath != null) {
                            localBitMap = BitmapFactory.decodeFile(imgActualPath)
                            val stream = ByteArrayOutputStream()
                            localBitMap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                            byteArrayData = stream.toByteArray()
                            fos.write(byteArrayData)
                        } else
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 95, fos)
                    }
                    fos.flush()
                    fos.close()

                    MediaScannerConnection
                        .scanFile(
                            context, arrayOf(file.absolutePath),
                            arrayOf("image/jpeg"), null
                        )
                }

            } catch (ex: Exception) {
                ex.printStackTrace()
            } catch (ex: OutOfMemoryError) {
                ex.printStackTrace()
            }

            refreshGallery(context, "$directory/$filename")
        } catch (e: Exception) {
            e.printStackTrace()
            // TODO: handle exception
        }

        return ""
    }

    fun refreshGallery(context: Context, url: String) {
        try {
            val file = File(url)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
                val f =
                    File("file://" + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES))
                val contentUri = Uri.fromFile(file)
                mediaScanIntent.data = contentUri
                context.sendBroadcast(mediaScanIntent)
            } else {
                context.sendBroadcast(Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse(url)))
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }


    //    public static Spanned getFormattedTitle(String title){
    //            String temp = title.substring(0, title.indexOf(" ")).trim();
    //            title = title.substring(temp.length(),title.length()).trim();
    //            try {
    //                return Html.fromHtml(temp + " " + "<b>" + title + "</b>", Html.FROM_HTML_MODE_LEGACY);
    //            }catch (Exception e) {
    //                return ;
    //            }
    //    }
    fun loadJSONFromAsset(fileName: String?, context: Context): String? {
        val json: String
        json = try {
            val `is` = context.assets.open(fileName!!)
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer, StandardCharsets.UTF_8)
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    fun isGPSEnable(context: Context): Boolean {
        var isGPSEnable = false
        try {
            val locationManager = context.getSystemService(LOCATION_SERVICE) as LocationManager
            val isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
            val isNetworkEnabled =
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

            if (!isGPSEnabled && !isNetworkEnabled) {
                isGPSEnable = false
            } else {
                isGPSEnable = true
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return isGPSEnable
    }

    /*fun alertMessageNoGPS(activity: Activity) {
        try {
            val builder = AlertDialog.Builder(activity)
            builder.setMessage(activity.getString(R.string.gpsDisableEnableText))
                .setCancelable(false)
                .setPositiveButton(
                    activity.getString(R.string.statusYesBtn)
                ) { dialog, id ->
                    activity.startActivity(
                        Intent(
                            Settings.ACTION_LOCATION_SOURCE_SETTINGS
                        )
                    )
                }
            /* .setNegativeButton(activity.getString(R.string.statusNoBtn), new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog,
                                            @SuppressWarnings("unused") final int id) {
                            dialog.cancel();
                        }
                    });*/
            val alert = builder.create()
            alert.show()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }*/

    fun setImageBackgroundTint(imageView: ImageView, color: Int) {
        try {
            if (Build.VERSION.SDK_INT <= 22) {
                imageView.background.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
            } else {
                imageView.backgroundTintList = ColorStateList.valueOf(color)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun updateResources(context: Context, language: String) {
        try {
            val locale = Locale(language)
            Locale.setDefault(locale)
            val res = context.resources
            val config = Configuration(res.configuration)
            config.setLocale(locale)
            res.updateConfiguration(config, res.displayMetrics)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun getCountryFlag(countryCode: String): String {
        var firstLetter = 0
        var secondLetter = 0
        try {
            // parameter - Locale locale
            //String countryCode = locale.getCountry();
            firstLetter = Character.codePointAt(countryCode, 0) - 0x41 + 0x1F1E6
            secondLetter = Character.codePointAt(countryCode, 1) - 0x41 + 0x1F1E6
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return String(Character.toChars(firstLetter)) + String(Character.toChars(secondLetter))
    }

    fun readFileFromRawDirectory(context: Context, @RawRes id: Int): String {
        val iStream: InputStream
        var byteStream: ByteArrayOutputStream? = null
        try {
            iStream = context.resources.openRawResource(id)
            val buffer = ByteArray(iStream.available())
            iStream.read(buffer)
            byteStream = ByteArrayOutputStream()
            byteStream.write(buffer)
            byteStream.close()
            iStream.close()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }

        return byteStream!!.toString()
    }

    /*fun searchSpinnerItems(
        searchableSpinner: SearchableSpinner,
        strText: String,
        spinnerList: ArrayList<SpinnerModel>) {
        val filteredList = ArrayList<SpinnerModel>()
        try {
            for (i in spinnerList.indices) {

                val text = spinnerList[i].title!!.toLowerCase()

                if (text.toLowerCase().startsWith(strText)) {
                    filteredList.add(spinnerList[i])
                }
            }
            searchableSpinner.updateList(filteredList)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }*/



    fun loadGlide(
        context: Context,
        profileIV: ImageView,
        profileImagePath: String,
        logo_border: Int
    ) {
        try {
            val circularProgressDrawable = CircularProgressDrawable(context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()


            val glideUrl = GlideUrl(
                profileImagePath,
                LazyHeaders.Builder()
                    .addHeader("Authorization", "Bearer "+ AppConstants.ACCESS_TOKEN)
                    .build()
            )

            Glide.with(context)
                .load(glideUrl)
                .placeholder(circularProgressDrawable)
                .apply(
                    RequestOptions.placeholderOf(logo_border)
                        .error(logo_border)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .fitCenter()
                        .skipMemoryCache(false)
                        .dontAnimate()
                )
                .into(profileIV)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun loadGlideSkipCache(
        context: Context,
        profileIV: ImageView,
        profileImagePath: String,
        logo_border: Int
    ) {
        try {

            Glide.with(context)
                .load(profileImagePath)
                .apply(
                    RequestOptions.placeholderOf(logo_border)
                        .error(logo_border)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter()
                        .skipMemoryCache(true)
                        .dontAnimate()
                )
                .into(profileIV)


        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun loadGlideWithCircleSkipCache(
        context: Context,
        profileIV: ImageView,
        profileImagePath: String,
        logo_border: Int
    ) {
        try {

            Glide.with(context)
                .load(profileImagePath)
                .apply(
                    RequestOptions.placeholderOf(logo_border)
                        .error(logo_border)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter()
                        .circleCrop()
                        .skipMemoryCache(true)
                        .dontAnimate()
                )
                .into(profileIV)


        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun getSharedPreference(context: Context): SharedPreferences? {
        var sharedPreferences: SharedPreferences? = null
        try {
            sharedPreferences =
                context.getSharedPreferences(SessionStore.sharePrefFileName, Context.MODE_PRIVATE)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return sharedPreferences
    }

    fun getCountryFromCode(code: String): String {
        return Locale("", code).displayCountry
    }

    fun customRegistrationMessageDialog(context: Context, screeName: String, userName: String) {
        try {
            //, final boolean isMoneySent, final boolean isAccountAdded
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            /* dialog.setContentView(R.layout.custom_message_dialog);

            TextView okTV = dialog.findViewById(R.id.okTV);

            TextView thankTV = dialog.findViewById(R.id.thankTV);
            TextView messageTV = dialog.findViewById(R.id.messageTV);
            TextView messageHintTV = dialog.findViewById(R.id.messageHintTV);

            if (StringUtils.equalsIgnoreCase(screeName, context.getString(R.string.sendMoney))) {
                okTV.setText(context.getString(R.string.done));
                thankTV.setText(context.getString(R.string.success));
                messageTV.setText(context.getString(R.string.moneySentToBankSuccess));
                messageHintTV.setVisibility(View.GONE);
            }
            okTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    if (StringUtils.equalsIgnoreCase(screeName, context.getString(R.string.ok))) {
                       // MainActivity.gotoLoginActivity(context);
                        ((Activity) context).finish();
                    }

                }
            });*/

            dialog.setOnKeyListener(object : DialogInterface.OnKeyListener {

                override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                    ///it will not kill dialog on backpress while app is in minimize mode
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        val startMain = Intent(Intent.ACTION_MAIN)
                        startMain.addCategory(Intent.CATEGORY_HOME)
                        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        context.startActivity(startMain)
                    }
                    return true
                }
            })

            dialog.show()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun textMandatory(textView: TextView, header: String) {
        val builder: SpannableStringBuilder
        try {

            val colored = "*"

            builder = SpannableStringBuilder(header + colored)
            builder.setSpan(
                ForegroundColorSpan(Color.BLACK), header.length, builder.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            textView.text = builder

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun hideKeyboard(activity: Activity) {
        try {
            val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            //Find the currently focused view, so we can grab the correct window token from it.
            var view = activity.currentFocus
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = View(activity)
            }
            //imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            imm.hideSoftInputFromWindow(
                activity.window.decorView.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
            //activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun openKeyboard(activity: Activity) {
        try {
            val inputMethodManager =
                activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager?.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun showKeyboardSearchSpinner(context: Context, ettext: SearchView) {
        ettext.requestFocus()
        ettext.postDelayed({
            val keyboard =
                context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            keyboard.showSoftInput(ettext, 0)
        }, 100)
    }

    fun getFileSizeInByte(filePath: String): Long {
        val finalSize: Long = 0
        var sizeInBytes: Long = 0
        try {
            val file = File(filePath)

            sizeInBytes = file.length()
            Log.e(TAG, "getFileSizeInByte() - $sizeInBytes Byte -FilePath :- $filePath")

            //  double sizeInKB = sizeInBytes / AppConstants.sizeKB;/// size in KB
            //  Log.i(TAG, "getFileSizeInByte() - " + sizeInKB + " KB -FilePath :- " + filePath);

            // double sizeInMB = sizeInBytes / AppConstants.sizeMB;/// size in MB
            // Log.i(TAG, "getFileSizeInByte() - " + sizeInMB + " MB -FilePath :- " + filePath);

            // long sizeInGB = sizeInBytes / AppConstants.sizeGB;/// size in GB
            // Log.i(TAG, "getFileSizeInByte() - " + sizeInGB + " GB -FilePath :- " + filePath);

            // long sizeInTB = sizeInBytes / AppConstants.sizeTB;/// size in TB
            // Log.i(TAG, "getFileSizeInByte() - " + sizeInTB + " TB -FilePath :- " + filePath);


        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return sizeInBytes
    }


    fun getRecyclerview(
        context: Context?,
        recyclerView: RecyclerView,
        isHorizontal: Boolean,
        isDivider: Boolean): RecyclerView? {
        try {
            if (isHorizontal) recyclerView.layoutManager =
                RecyclerLinearLayoutManager(
                    context,
                    LinearLayoutManager.HORIZONTAL,
                    false
                ) else recyclerView.layoutManager =
                RecyclerLinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            if (isDivider) {
                if (isHorizontal) recyclerView.addItemDecoration(
                    com.absyz.kotlin.helpers.DividerItemDecoration(
                        context!!,
                        com.absyz.kotlin.helpers.DividerItemDecoration.HORIZONTAL_LIST,
                        20,
                        2
                    )
                ) else recyclerView.addItemDecoration(
                    com.absyz.kotlin.helpers.DividerItemDecoration(
                        context!!, com.absyz.kotlin.helpers.DividerItemDecoration.VERTICAL_LIST, 20, 2
                    )
                )
            } /*else
                recyclerView.addItemDecoration(new DividerItemDecoration(context));*/
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
        return recyclerView
    }

    fun getFileNameWithoutExtension(filePath: String): String {
        var fileName = ""

        try {
            val file = File(filePath)
            if (file != null && file.exists()) {
                val name = file.name
                fileName = name.replaceFirst("[.][^.]+$".toRegex(), "")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            fileName = ""
        }

        return fileName

    }

    /*fun getCities(cityModels: List<CityModel>): ArrayList<SpinnerModel> {
        val cityList = ArrayList<SpinnerModel>()
        try {
            for (cityModel in cityModels) {
                try {
                    val spinnerModel = SpinnerModel()
                    if (!StringUtils.isBlank(cityModel.name)) {
                        spinnerModel.id = cityModel.id
                        spinnerModel.title = cityModel.name
                        cityList.add(spinnerModel)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

            }

            try {
                Collections.sort(cityList) { o1, o2 -> o1.title!!.compareTo(o2.title!!) }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return cityList
    }

    fun getCountries(countryModels: List<CountryModel>): ArrayList<SpinnerModel> {
        val modelArrayList = ArrayList<SpinnerModel>()
        try {
            for (countryModel in countryModels) {
                try {
                    val spinnerModel = SpinnerModel()
                    if (!StringUtils.isBlank(countryModel.name_en)) {
                        spinnerModel.id = countryModel.id
                        spinnerModel.title = countryModel.name_en
                        spinnerModel.description = countryModel.country_code
                        modelArrayList.add(spinnerModel)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

            }

            try {
                Collections.sort(modelArrayList) { o1, o2 -> o1.title!!.compareTo(o2.title!!) }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return modelArrayList
    }

    fun showTitleDialog(context: Context, eTTitle: TextView) {
        try {
            val items = arrayOf(
                context.getString(R.string.title_mr),
                context.getString(R.string.title_miss),
                context.getString(R.string.title_mrs)
            )
            val builder = AlertDialog.Builder(context)
            builder.setTitle(null)

            builder.setAdapter(
                ArrayAdapter(
                    context,
                    R.layout.spinner_item, R.id.itemTextV, items
                )
            ) { dialog, which -> eTTitle.text = items[which] }
            builder.show()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }*/

    fun focusOnView(scrollView: ScrollView, view: View) {
        try {
            Handler().post { scrollView.scrollTo(0, view.top) }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    /*fun findImageURL(files: ArrayList<ImageFileModel>, profileID: Int): String? {
        var url: String? = ""
        try {
            for (imageFileModel in files) {
                try {

                    if (profileID == imageFileModel.id) {
                        url = imageFileModel.download_url
                        break
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return url
    }*/

    fun getLoggedInMobileNumber(context: Context): String {
        var mobile = ""
        try {
            mobile =
                sessionStore.getString(SessionStore.COUNTRY_CODE, "")!! + sessionStore.getString(
                    SessionStore.MOBILE,
                    ""
                )!!
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return mobile
    }

    fun getUserID(context: Context): Int {
        return sessionStore.getString(SessionStore.USER_ID, "0")!!.toInt()
    }

    fun getLoggedInMobileNumberWithoutCode(context: Context): String? {
        var mobile: String? = ""
        try {
            mobile = sessionStore.getString(SessionStore.MOBILE, "")
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return mobile
    }

    fun showMapDirection(context: Context, customerLocation: Location?) {
        val directionsBuilder: Uri.Builder
        try {
            if (customerLocation != null) {
                directionsBuilder = Uri.Builder()
                    .scheme("https")
                    .authority("www.google.com")
                    .appendPath("maps")
                    .appendPath("dir")
                    .appendPath("")
                    .appendQueryParameter("api", "1")
                    // .appendQueryParameter("origin", 26.281143 + "," + 50.214734)
                    //.appendQueryParameter("destination", customerLocation.getLatitude() + "," + customerLocation.getLongitude());
                    //.appendQueryParameter("destination", 28.403194 + "," + 45.98228);
                    .appendQueryParameter(
                        "destination",
                        customerLocation.latitude.toString() + "," + customerLocation.longitude
                    )

                context.startActivity(Intent(Intent.ACTION_VIEW, directionsBuilder.build()))
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun showNoConnectionDialog(fragmentManager: FragmentManager, screenName: String) {
        try {
            /* DialogFragment bottomSheetDialogFragment = new NoConnectionFragment();
            bottomSheetDialogFragment.show(fragmentManager, bottomSheetDialogFragment.getTag());

            Bundle bundle = new Bundle(1);
            bundle.putString(AppConstants.TOOLBAR_TITLE, screenName);
            bottomSheetDialogFragment.setArguments(bundle);*/
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun toggleEnableDisable(textView: TextView, isEnable: Boolean) {
        try {
            if (isEnable) {
                textView.alpha = 1f
                textView.isEnabled = true
            } else {
                textView.alpha = 0.3f
                textView.isEnabled = false
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun toggleEnableDisable(textView: View, isEnable: Boolean) {
        try {
            if (isEnable) {
                textView.alpha = 1f
                textView.isEnabled = true
            } else {
                textView.alpha = 0.3f
                textView.isEnabled = false
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    /*fun onClickTwoButtonChangeSharpCorner(
        context: Context,
        clickView: TextView,
        unClickView: TextView) {
        try {
            clickView.setTextColor(context.resources.getColor(R.color.white))
            clickView.setBackgroundResource(R.drawable.rectangle_sharp_corner)

            unClickView.setTextColor(context.resources.getColor(R.color.black))
            unClickView.setBackgroundResource(R.drawable.rectangle_sharp_corner_line_border)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun onClickOneButtonChangeSharpCorner(context: Context, clickView: TextView) {
        try {
            clickView.setTextColor(context.resources.getColor(R.color.white))
            clickView.setBackgroundResource(R.drawable.rectangle_sharp_corner)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun onClickTwoButtonChangeRoundCorner(
        context: Context,
        clickView: TextView,
        unClickView: TextView) {
        try {
            clickView.setTextColor(context.resources.getColor(R.color.white))
            clickView.setBackgroundResource(R.drawable.rectangle_round_corner_theme)

            unClickView.setTextColor(context.resources.getColor(R.color.black))
            unClickView.setBackgroundResource(R.drawable.rectangle_corner_line_border)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun onClickOneButtonChangeRoundCorner(context: Context, clickView: TextView) {
        try {
            clickView.setTextColor(context.resources.getColor(R.color.white))
            clickView.setBackgroundResource(R.drawable.rectangle_round_corner_theme)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun alertMessageDialog(context: Context, message: String) {
        try {

            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.custom_dialog_alert)

            val okTV = dialog.findViewById<TextView>(R.id.okTV)

            val messageTV = dialog.findViewById<TextView>(R.id.messageTV)
            messageTV.text = message

            okTV.setOnClickListener { dialog.dismiss() }

            dialog.setOnKeyListener(object : DialogInterface.OnKeyListener {

                override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                    ///it will not kill dialog on backpress while app is in minimize mode
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        val startMain = Intent(Intent.ACTION_MAIN)
                        startMain.addCategory(Intent.CATEGORY_HOME)
                        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        context.startActivity(startMain)
                    }
                    return true
                }
            })

            dialog.show()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }*/

    fun updateBuildDialog(context: Context) {
        try {

            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            /*  dialog.setContentView(R.layout.custom_playstore_dialog);

            TextView massageTV = dialog.findViewById(R.id.massageTV);
            massageTV.setText(context.getString(R.string.updateCOnfirmation));
            TextView yesTV = dialog.findViewById(R.id.yesTV);
            TextView noTV = dialog.findViewById(R.id.noTV);

            yesTV.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.APP_LINK));
                    context.startActivity(intent);
                }

            });
            noTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });*/

            dialog.setOnKeyListener(object : DialogInterface.OnKeyListener {

                override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                    ///it will not kill dialog on backpress while app is in minimize mode
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        val startMain = Intent(Intent.ACTION_MAIN)
                        startMain.addCategory(Intent.CATEGORY_HOME)
                        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        context.startActivity(startMain)
                    }
                    return true
                }
            })

            dialog.show()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    /*fun setNotHaveAccountText(context: Context, textView: TextView?) {
        var text = ""
        try {
            text =
                "<b><font color=" + AppConstants.THEMECOLOR + ">" + context.getString(R.string.here) + "</font></b>"
            text = context.getString(R.string.alreadyRegister, "$text")
            setHtmlText(textView!!, text)
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }*/

    fun setTermsConditionText(context: Context, textView: TextView?) {
        var text = ""
        try {
            /*text =
                context.getString(R.string.herebyAgree) + " <font color=" + AppConstants.THEMECOLOR + ">" + context.getString(
                    R.string.termsCondition
                ) + "</font> " + context.getString(R.string.and) + " <font color=" + AppConstants.THEMECOLOR + ">" + context.getString(
                    R.string.privacyPolicy
                ) + "</font>"
            setHtmlText(textView!!, text)*/
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    fun setHtmlText(textView: TextView, text: String?) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                textView.text = Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
            } else {
                textView.text = Html.fromHtml(text)
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    /*fun selectViewRoundBackground(context: Context, selectedView: View, textv: TextView) {
        selectedView.setBackgroundResource(R.drawable.rectangle_corner_line_border_theme)
        textv.setTextColor(context.resources.getColor(R.color.colorPrimary))
    }

    fun unSelectViewRoundBackground(context: Context, button: View, textv: TextView) {
        button.setBackgroundResource(R.drawable.rectangle_corner_line_border_gray)
        textv.setTextColor(context.resources.getColor(R.color.text_color))
    }*/

    fun customPermissionDialog(context: Context, screeName: String, userName: String) {
        try {
            //, final boolean isMoneySent, final boolean isAccountAdded
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            /* dialog.setContentView(R.layout.custom_message_dialog);

            TextView okTV = dialog.findViewById(R.id.okTV);

            TextView thankTV = dialog.findViewById(R.id.thankTV);
            TextView messageTV = dialog.findViewById(R.id.messageTV);
            TextView messageHintTV = dialog.findViewById(R.id.messageHintTV);

            if (StringUtils.equalsIgnoreCase(screeName, context.getString(R.string.sendMoney))) {
                okTV.setText(context.getString(R.string.done));
                thankTV.setText(context.getString(R.string.success));
                messageTV.setText(context.getString(R.string.moneySentToBankSuccess));
                messageHintTV.setVisibility(View.GONE);
            }
            okTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    if (StringUtils.equalsIgnoreCase(screeName, context.getString(R.string.ok))) {
                       // MainActivity.gotoLoginActivity(context);
                        ((Activity) context).finish();
                    }

                }
            });*/

            dialog.setOnKeyListener(object : DialogInterface.OnKeyListener {

                override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                    ///it will not kill dialog on backpress while app is in minimize mode
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        val startMain = Intent(Intent.ACTION_MAIN)
                        startMain.addCategory(Intent.CATEGORY_HOME)
                        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        context.startActivity(startMain)
                    }
                    return true
                }
            })

            dialog.show()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    /*fun hasEmptyError(views: List<EditText>): Boolean {
        var isError = false
        try {
            val size = views.size
            for (i in 0 until size) {
                try {
                    if (StringUtils.isBlank(views[i].text.toString().trim())) {
                        views[i].setBackgroundResource(R.drawable.edittext_rectangle_error)
                        if (!isError)
                            isError = true
                    } else {
                        views[i].setBackgroundResource(R.drawable.edittext_rectangle)
                    }
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
        return isError
    }

    fun editTextBackgroundTextChange(view: EditText) {
        try {
            if (StringUtils.isBlank(view.text.toString().trim())) {
                view.setBackgroundResource(R.drawable.edittext_rectangle_error)
            } else {
                view.setBackgroundResource(R.drawable.edittext_rectangle)
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    fun editTextBackgroundError(view: View) {
        try {
            view.setBackgroundResource(R.drawable.edittext_rectangle_error)
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    fun viewBackgroundWhiteLine(view: View) {
        try {
            view.setBackgroundResource(R.drawable.edittext_rectangle_white_line)
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    fun spinnerBackgroundError(view: View) {
        try {
            view.setBackgroundResource(R.drawable.spinner_background_error)
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    fun spinnerBackground(view: View) {
        try {
            view.setBackgroundResource(R.drawable.spinner_background)
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    fun editTextBackground(view: View) {
        try {
            view.setBackgroundResource(R.drawable.edittext_rectangle)
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }


    fun getRecyclerview(
        context: Context?,
        recyclerView: RecyclerView,
        isHorizontal: Boolean,
        isDivider: Boolean): RecyclerView? {
        try {
            if (isHorizontal) recyclerView.layoutManager =
                RecyclerLinearLayoutManager(
                    context,
                    LinearLayoutManager.HORIZONTAL,
                    false
                ) else recyclerView.layoutManager =
                RecyclerLinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            if (isDivider) {
                if (isHorizontal) recyclerView.addItemDecoration(
                    DividerItemDecoration(
                        context!!,
                        DividerItemDecoration.HORIZONTAL_LIST,
                        20,
                        2
                    )
                ) else recyclerView.addItemDecoration(
                    DividerItemDecoration(
                        context!!, DividerItemDecoration.VERTICAL_LIST, 20, 2
                    )
                )
            } /*else
                recyclerView.addItemDecoration(new DividerItemDecoration(context));*/
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
        return recyclerView
    }

    fun getRecyclerviewGrid(
        context: Context?,
        recyclerView: RecyclerView,
        spanCount: Int,
        isHorizontal: Boolean,
        isDivider: Boolean): RecyclerView? {
        try {
            if (isHorizontal) recyclerView.layoutManager =
                GridLinearLayoutManager(
                    context,
                    spanCount,
                    LinearLayoutManager.HORIZONTAL,
                    false
                ) else recyclerView.layoutManager =
                GridLinearLayoutManager(context, spanCount, LinearLayoutManager.VERTICAL, false)
            if (isDivider) {
                if (isHorizontal) recyclerView.addItemDecoration(
                    DividerItemDecoration(
                        context!!,
                        DividerItemDecoration.HORIZONTAL_LIST,
                        20,
                        2
                    )
                ) else recyclerView.addItemDecoration(
                    DividerItemDecoration(
                        context!!, DividerItemDecoration.VERTICAL_LIST, 20, 2
                    )
                )
            } /*else
                recyclerView.addItemDecoration(new DividerItemDecoration(context));*/
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
        return recyclerView
    }

    fun applyDecimalPattern() {
        try {
            AppConstants.decimalFormatOnePlace.applyPattern("########0.#")
            AppConstants.decimalFormatTwoPlace.applyPattern("########0.##")
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    fun getPriceWithSAR(context: Context?, priceFloat: Float = 0f): String? {
        var price = ""
        try {
            price =
                getCurrency(context!!) + "" + AppConstants.decimalFormatTwoPlace.format(priceFloat)
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
        return price
    }

    fun getPriceWithSAR(context: Context?, priceFloat: Int = 0): String? {
        var price = ""
        try {
            price =
                getCurrency(context!!) + "" + AppConstants.decimalFormatTwoPlace.format(priceFloat)
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
        return price
    }

    fun getPriceWithSAR(context: Context?, priceFloat: Double = 0.0): String? {
        var price = ""
        try {
            price =
                getCurrency(context!!) + "" + AppConstants.decimalFormatTwoPlace.format(priceFloat)
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
        return price
    }

    private fun getCurrency(context: Context): String {
        var currency = "INR"
        try {
            currency = context.resources.getString(R.string.rupee)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return currency
    }*/

    fun setViewBackgroundTint(context: Context, view: View, id: Int) {
        try {
            view.background.setColorFilter(
                context.resources.getColor(id),
                PorterDuff.Mode.SRC_ATOP
            )
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    /*fun shareFriend(context: Context) {
        try {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name))
            var strShareMessage = "\n${
                context.getString(
                    R.string.shareMessage,
                    AppConstants.REFER_YOU_EARN.toString()
                )
            }\n"
            strShareMessage += AppConstants.APP_LINK
            /* val screenshotUri =
                 Uri.parse("android.resource://${BuildConfig.APPLICATION_ID}/" + R.drawable.app_logo)
             intent.type = "image/png"
             intent.putExtra(Intent.EXTRA_STREAM, screenshotUri)*/
            intent.putExtra(Intent.EXTRA_TEXT, strShareMessage)
            context.startActivity(
                Intent.createChooser(
                    intent,
                    context.getString(R.string.shareVia)
                )
            )
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }*/

    fun setError(tvError: TextView, error: String) {
        try {
            tvError.text = ""
            if (!StringUtils.isBlank(error)) {
                tvError.text = error
                tvError.visibility = View.VISIBLE
            } else {
                tvError.visibility = View.GONE
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    /*fun getProgressDialogCircle(activity: FragmentActivity?) {

    }*/

    fun getProgressDialogCircle(context: Context?): Dialog? {
        var dialog: Dialog? = null
        try {
            dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.dialog_loading_view)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCanceledOnTouchOutside(false)
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
        return dialog
    }

    fun loadGlideurl(context: Context, imageUrl: String) {
        try{
             GlideUrl(
                imageUrl,
                LazyHeaders.Builder()
                    .addHeader("Authorization", "Bearer "+ AppConstants.ACCESS_TOKEN)
                    .build()
            )
        }catch (ex: Exception){
            ex.printStackTrace()
        }

    }

}