package com.absyz.kotlin.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.absyz.kotlin.R
import com.absyz.kotlin.activities.ui.ProductDetailActivity
import com.absyz.kotlin.adapters.ProductCategoryDetailsAdapter
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.FragmentSalesTargetBinding
import com.absyz.kotlin.model.RequestModel.ProductCategoryDetailsRequestModel
import com.absyz.kotlin.model.RequestModel.SalesTargetRequestModel
import com.absyz.kotlin.model.ResponseModel.ProductCategoryDetails
import com.absyz.kotlin.model.ResponseModel.ProductCategoryDetailsResponseModel
import com.absyz.kotlin.model.ResponseModel.SalesTargetListDetails
import com.absyz.kotlin.model.ResponseModel.SalesTargetResponseModel
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel
import com.monsterbrain.recyclerviewtableview.TargetSalesAdapter

//class ProductCategoryDetailsFragment : BaseFragment() {
//
//    lateinit var binding: FragmentSalesTargetBinding
//    var targetlist: MutableList<SalesTargetListDetails> = ArrayList()
//
//    //private var tableRecycler : RecyclerView? = null
//    lateinit var productCategoryDetailsAdapter: ProductCategoryDetailsAdapter
//
//    lateinit var homeViewModel: HomeViewModel
//
//
//    @Override
//    override fun provideFragmentView(
//        inflater: LayoutInflater?, parent: ViewGroup?,
//        savedInstanceState: Bundle?): View? {
//        binding =
//            DataBindingUtil.inflate(inflater!!, R.layout.fragment_product_category, parent, false)
//
//        return binding.root
//    }
//
//    override fun onActivityCreated(savedInstanceState: Bundle?) {
//        super.onActivityCreated(savedInstanceState)
//
//        try{
//            (requireActivity() as AppCompatActivity).supportActionBar?.hide()
//
//            (activity?.application as ApplicationController).applicationComponent.inject(this)
//            setadapter()
//
//            //View model instance
//            homeViewModel =
//                ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)
//
//            //Observer
//            homeViewModel!!.getProductCategoryDetailsResponse()
//                .observe(viewLifecycleOwner, { this.consumeProductCategoryDetailsResponse(it as ApiResponse<*>) })
//
//            //Api Call
//            productcatDetailsPostString = ProductCategoryDetailsRequestModel(
//                productCategoryId =
//            )
//            sendAPi(productcatDetailsPostString!!,pagenumer)
//
//            binding.burgerMenu.setOnClickListener {
//                Toast.makeText(context,"Back icon pressed", Toast.LENGTH_SHORT).show()
//            }
//
//
//        }catch (ex: Exception){
//            ex.printStackTrace()
//        }
//    }
//
//    @SuppressLint("UseRequireInsteadOfGet")
//    private fun setadapter() {
//
//        try {
//            productCategoryDetailsAdapter = ProductCategoryDetailsAdapter(activity!!, utility!!)
//            val layoutManager = GridLayoutManager(context, 3)
//            binding.targetRV.setLayoutManager(layoutManager);
//            binding.targetRV.setHasFixedSize(true)
//            binding.targetRV.setAdapter(targetsalesAdapter);
//
//        } catch (ex: Exception) {
//            ex.printStackTrace()
//        }
//    }
//
//    private fun consumeProductCategoryDetailsResponse(response: ApiResponse<*>) {
//        Log.e(TAG, "consumeProductCategoryDetailsResponse()")
//        try {
//            when (response.apiStatus) {
//
//                ApiStatus.LOADING -> {
//                    showLoader()
//                }
//
//                ApiStatus.SUCCESS -> {
//                    dismissLoader()
//                    response?.let { apiResponse ->
//                        apiResponse.data?.let {
//                            if (it is ProductCategoryDetailsResponseModel) {
//                                productCategoryDetails.addAll(it.ProductDetails!!)
//                                current_pg = it.currentPage
//                                last_pg = it.lastPage
//                                updateProductCategoryDetailsList()
//                            }
//                        }
//                    }
//                }
//
//                ApiStatus.ERROR -> {
//                    dismissLoader()
//                    Toast.makeText(context,response.error!!.message,Toast.LENGTH_SHORT).show()
//                }
//            }
//        } catch (ex: Exception) {
//            ex.printStackTrace()
//            Log.e(TAG, "consumeResponse() Exception - ${ex.message}")
//        }
//
//    }
//
//    private fun updateProductCategoryDetailsList() {
//        try {
//            targetsalesAdapter.updateList(targetListDetails)
//            targetsalesAdapter.setOnItemCLickListerner(object :TargetSalesAdapter.onItemClickListener{
//                override fun onItemCLick(position: Int) {
//
//                    Toast.makeText(context, targetListDetails[position].Name, Toast.LENGTH_SHORT).show()
//
//                    val intent_detail = Intent(context, ProductDetailActivity::class.java)
//                    intent_detail.putExtra("name",targetListDetails[position].Name)
//                    intent_detail.putExtra("id",targetListDetails[position].Id)
//                    intent_detail.putExtra("producttype",targetListDetails[position].ProductType)
//                    intent_detail.putExtra("imageId",targetListDetails[position].ProductImage)
//                    intent_detail.putExtra("category",targetListDetails[position].Category)
//                    intent_detail.putExtra("dimentions",targetListDetails[position].Dimentions)
//                    intent_detail.putExtra("desc",targetListDetails[position].ProductDescription)
//                    intent_detail.putExtra("stock",targetListDetails[position].ProductStock)
//                    intent_detail.putExtra("price",targetListDetails[position].Price.toString())
//                    intent_detail.putExtra("qty",targetListDetails[position].Quantity.toString())
//
//                    startActivity(intent_detail)
//
//                }
//            } )
//        } catch (ex: Exception) {
//            ex.printStackTrace()
//        }
//
//    }
//
//
//    fun sendAPi(productcatDetailsPostString: ProductCategoryDetailsRequestModel,pagenumber: String) {
//        try {
//            homeViewModel.ProductCategoryDetailsData(
//                productcatDetailsPostString,
//                "Bearer " + AppConstants.ACCESS_TOKEN
//            )        } catch (ex: Exception) {
//            ex.printStackTrace()
//        }
//    }
//
//
//}