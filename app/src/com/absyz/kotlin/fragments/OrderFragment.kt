package com.absyz.kotlin.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.absyz.kotlin.R
import com.absyz.kotlin.activities.ui.CartActivity
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.FragmentOrderBinding
import com.absyz.kotlin.model.RequestModel.CartProductRequest
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.viewmodel.HomeViewModel


class OrderFragment : BaseFragment() {

    lateinit var binding: FragmentOrderBinding

    @Override
    override fun provideFragmentView(
        inflater: LayoutInflater?, parent: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding =
            DataBindingUtil.inflate(inflater!!, R.layout.fragment_order, parent, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        try{
            /*Main Content Area starts*/
            (requireActivity() as AppCompatActivity).supportActionBar?.hide()

            binding.burgerMenu.setOnClickListener {
                try {
                    if (activity?.supportFragmentManager?.backStackEntryCount!! > 1) {
                        activity?.supportFragmentManager?.popBackStack()
                    } else {
                        super.getActivity()?.onBackPressed()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        }catch (ex: Exception){
            ex.printStackTrace()
        }
    }

}