package com.absyz.kotlin.fragments

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.absyz.kotlin.R
import com.absyz.kotlin.databinding.FragmentAccountBinding
import com.absyz.kotlin.databinding.FragmentPieChartBinding
import com.absyz.kotlin.viewmodel.Score
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import kotlinx.android.synthetic.main.fragment_pie_chart.*

class PieChartFragment : BaseFragment() {

    lateinit var binding: FragmentPieChartBinding
    private var scoreList = ArrayList<Score>()

    @Override
    override fun provideFragmentView(
        inflater: LayoutInflater?, parent: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding =
            DataBindingUtil.inflate(inflater!!, R.layout.fragment_pie_chart, parent, false)
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        try{
            binding.checkbox.setOnClickListener {
                val sceneViewerIntent = Intent(Intent.ACTION_VIEW)
                sceneViewerIntent.data = Uri.parse("https://arvr.google.com/scene-viewer/1.0?file=https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/Avocado/glTF/Avocado.gltf")
                sceneViewerIntent.setPackage("com.google.android.googlequicksearchbox")
                startActivity(sceneViewerIntent)
            }

            initPieChart()
            setDataToPieChart()
            scoreList = getScoreList()

            initBarChart()


            //now draw bar chart with dynamic data
            val entries: ArrayList<BarEntry> = ArrayList()

            //you can replace this data object with  your custom object
            for (i in scoreList.indices) {
                val score = scoreList[i]
                entries.add(BarEntry(i.toFloat(), score.score.toFloat()))
            }


            val barDataSet = BarDataSet(entries, "")
            barDataSet.setColors(*ColorTemplate.COLORFUL_COLORS)

            val data = BarData(barDataSet)
            barChart.data = data

            barDataSet.valueTextSize = 20f
            barChart.invalidate()



        }catch (e: Exception){
            e.printStackTrace()
        }




        }

    // simulate api call
    // we are initialising it directly
    private fun getScoreList(): ArrayList<Score> {
        scoreList.add(Score("Ujjain", 56))
        scoreList.add(Score("Indore", 75))
        scoreList.add(Score("Dewas", 85))
        scoreList.add(Score("Hyd", 45))
        scoreList.add(Score("Delhi", 63))

        return scoreList
    }
    private fun initBarChart() {

//        hide grid lines
        barChart.axisLeft.setDrawGridLines(false)
        val xAxis: XAxis = barChart.xAxis
        xAxis.setDrawGridLines(false)
        xAxis.setDrawAxisLine(false)

        //remove right y-axis
        barChart.axisRight.isEnabled = false

        //remove legend
        barChart.legend.isEnabled = false


        //remove description label
        barChart.description.isEnabled = false


        //add animation
        barChart.animateY(3000)

        // to draw label on xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM_INSIDE
        xAxis.valueFormatter = MyAxisFormatter()
        xAxis.setDrawLabels(true)
        xAxis.granularity = 1f
        xAxis.labelRotationAngle = +90f    }

    private fun setDataToPieChart() {
        pieChart.setUsePercentValues(true)
        val dataEntries = ArrayList<PieEntry>()
        dataEntries.add(PieEntry(70f, "Product Sales"))
        dataEntries.add(PieEntry(20f, "Pending"))
        dataEntries.add(PieEntry(10f, "Target"))


        val colors: ArrayList<Int> = ArrayList()
        colors.add(Color.parseColor("#4DD0E1"))
        colors.add(Color.parseColor("#FFF176"))
        colors.add(Color.parseColor("#FF8A65"))



        val dataSet = PieDataSet(dataEntries, "")
        val data = PieData(dataSet)

        // In Percentage
        data.setValueFormatter(PercentFormatter())

        dataSet.sliceSpace = 3f
        dataSet.colors = colors
        dataSet.valueTextSize = 25.1F
        pieChart.data = data
        data.setValueTextSize(15f)
        pieChart.setExtraOffsets(5f, 10f, 5f, 5f)
        pieChart.animateY(1400, Easing.EaseInOutQuad)

        //create hole in center
        pieChart.holeRadius = 58f
        pieChart.transparentCircleRadius = 61f
        pieChart.isDrawHoleEnabled = true
        pieChart.setHoleColor(Color.WHITE)


        //add text in center
        pieChart.setDrawCenterText(true);
        pieChart.centerText = "Sales Targets"



        pieChart.invalidate()    }

    private fun initPieChart() {
        pieChart.setUsePercentValues(true)
        pieChart.description.text = ""
        //hollow pie chart
        pieChart.isDrawHoleEnabled = false
        pieChart.setTouchEnabled(false)
        pieChart.setDrawEntryLabels(false)
        //adding padding
        pieChart.setExtraOffsets(20f, 0f, 20f, 20f)
        pieChart.setUsePercentValues(true)
        pieChart.isRotationEnabled = false
        pieChart.setDrawEntryLabels(false)
        pieChart.legend.orientation = Legend.LegendOrientation.VERTICAL
        pieChart.legend.isWordWrapEnabled = true    }


    inner class MyAxisFormatter : IndexAxisValueFormatter() {

        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            val index = value.toInt()
            Log.d(TAG, "getAxisLabel: index $index")
            return if (index < scoreList.size) {
                scoreList[index].name
            } else {
                ""
            }
        }
    }

}
