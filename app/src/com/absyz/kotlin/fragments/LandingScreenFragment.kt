package com.absyz.kotlin.fragments

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isGone
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.absyz.kotlin.R
import com.absyz.kotlin.activities.ui.BrandCategoryDetailActivity
import com.absyz.kotlin.activities.ui.ProductCategoryDetailActivity
import com.absyz.kotlin.activities.ui.ProductDetailActivity
import com.absyz.kotlin.adapters.*
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.dagger.factory.ViewModelFactory
import com.absyz.kotlin.databinding.FragmentLandingScreenBinding
import com.absyz.kotlin.model.ResponseModel.*
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel
import com.monsterbrain.recyclerviewtableview.TargetSalesAdapter
import kotlinx.android.synthetic.main.fragment_landing_screen.*


class LandingScreenFragment : BaseFragment() {

    lateinit var binding: FragmentLandingScreenBinding
    var list: MutableList<ProductCategory> = ArrayList()

    //Product
//    private var productCatRecycler: RecyclerView? = null
    lateinit var productCatAdapter: ProductCategoriesAdapter

    //Trending products
//    private var trendingRecycler: RecyclerView? = null
    lateinit var trendingAdapter: TrendingProductsAdapter

    //Newly added Products
//    private var newlyRecycler: RecyclerView? = null
    lateinit var newlyAdapter: NewlyAddedProductsAdapter

    //Brand
//    private var brandRecycler: RecyclerView? = null
    lateinit var brandAdapter: BrandAdapter

    //Wishlist
//    private var wishlistRecycler: RecyclerView? = null
    lateinit var wishlistAdapter: WishlistAdapter

    lateinit var homeViewModel: HomeViewModel

//    private val progressDialog: ProgressDialog? = null

    private var productcategoryDetails: ArrayList<ProductCategory> = ArrayList()
    private var trendingproductsDetails: ArrayList<TrendingProductsForTheStore> = ArrayList()
    private var newlyaddedDetails: ArrayList<NewlyAddedProducts> = ArrayList()
    private var brandDetails: ArrayList<Brands> = ArrayList()

    private var wishListDetails: ArrayList<WishListDetails> = ArrayList()

    @Override
    override fun provideFragmentView(
        inflater: LayoutInflater?, parent: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater!!, R.layout.fragment_landing_screen, parent, false)
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        try {
            (activity?.application as ApplicationController).applicationComponent.inject(this)
            setadapter()

            /*Main Content Area starts*/
            //View model instance
            homeViewModel =
                ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

            //Observer
            homeViewModel!!.getLandingResponse()
                .observe(viewLifecycleOwner, { this.consumeLandingResponse(it as ApiResponse<*>) })
            homeViewModel!!.getWishlistResponse()
                .observe(viewLifecycleOwner, { this.consumeWishliatResponse(it as ApiResponse<*>) })

            landingapiCall()
            wishlistapiCall()


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    @SuppressLint("UseRequireInsteadOfGet")
    private fun setadapter() {

        try {
            showLoader()

            productCatAdapter = ProductCategoriesAdapter(activity!!, utility!!)
            utility!!.getRecyclerview(context, binding.productCatRv, true, false)!!.adapter =
                productCatAdapter


            trendingAdapter = TrendingProductsAdapter(activity!!, utility!!)
            utility!!.getRecyclerview(context, binding.trendingRv, true, false)!!.adapter =
                trendingAdapter


            brandAdapter = BrandAdapter(activity!!, utility!!)
            utility!!.getRecyclerview(context, binding.brandRv, true, false)!!.adapter =
                brandAdapter


            newlyAdapter = NewlyAddedProductsAdapter(activity!!, utility!!)
            utility!!.getRecyclerview(context, binding.newlyaddedRv, true, false)!!.adapter =
                newlyAdapter


            wishlistAdapter = WishlistAdapter(activity!!, utility!!)
            utility!!.getRecyclerview(context, binding.wishlistRv, true, false)!!.adapter =
                wishlistAdapter

            dismissLoader()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }


//
//        val prodlayoutManager: RecyclerView.LayoutManager =
//            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
//        val trendlayoutManager: RecyclerView.LayoutManager =
//            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
//        val newlylayoutManager: RecyclerView.LayoutManager =
//            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
//        val brandslayoutManager: RecyclerView.LayoutManager =
//            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
//        val wishlistlayoutManager: RecyclerView.LayoutManager =
//            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

//        product_cat_rv!!.layoutManager = prodlayoutManager

//        productCatAdapter = view?.let { ProductCategoriesAdapter(it.context) }

//        trending_rv!!.layoutManager = trendlayoutManager
//        trendingAdapter = view?.let { TrendingProductsAdapter(it.context) }
//
//        newlyadded_rv!!.layoutManager = newlylayoutManager
//        newlyAdapter = view?.let { NewlyAddedProductsAdapter(it.context) }
//
//        brand_rv!!.layoutManager = brandslayoutManager
//        brandAdapter = view?.let { BrandAdapter(it.context) }
//
//        wishlist_rv!!.layoutManager = wishlistlayoutManager
//        wishlistAdapter = view?.let { WishlistAdapter(it.context) }
    }


    private fun consumeLandingResponse(response: ApiResponse<*>) {
        Log.e(HomeViewModel.TAG, "consumeLandingResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING ->
                    showLoader()
//                    progressDialog!!.setMessage("Please Wait..")
//                    progressDialog.setCanceledOnTouchOutside(false)
//                    progressDialog.setCancelable(false)
//                    progressDialog.show()


                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is LandingMainResponseModel) {

                                productcategoryDetails.addAll(it.ProductCategory)
                                trendingproductsDetails.addAll(it.TrendingProductsForTheStore)
                                newlyaddedDetails.addAll(it.NewlyAddedProducts)
                                brandDetails.addAll(it.Brands)

                                updatelandingList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //1 Toast.makeText(context, response.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeViewModel.TAG, "consumeResponse() Exception - ${ex.message}")
        }
    }



    private fun updatelandingList() {
        try {
            if(productcategoryDetails.size.equals(0)){
                binding.prodLayout.visibility = View.GONE
            } else {
                productCatAdapter?.updateList(productcategoryDetails)
                productCatAdapter.setOnItemCLickListerner(object :ProductCategoriesAdapter.onItemClickListener{
                    override fun onItemCLick(position: Int) {

                        //Toast.makeText(context, productcategoryDetails[position].ProductCategory, Toast.LENGTH_SHORT).show()

                          val intent_detail = Intent(context, ProductCategoryDetailActivity::class.java)
                          intent_detail.putExtra("id",productcategoryDetails[position].Id)
                          intent_detail.putExtra("catname",productcategoryDetails[position].ProductCategory)
                        startActivity(intent_detail)

                    }
                } )
            }
            if(trendingproductsDetails.size.equals(0)){
                binding.trendingLayout.visibility = View.GONE
            } else {
                trendingAdapter?.updateList(trendingproductsDetails)
                trendingAdapter.setOnItemCLickListerner(object :TrendingProductsAdapter.onItemClickListener{
                    override fun onItemCLick(position: Int) {

                        //Toast.makeText(context, trendingproductsDetails[position].Name, Toast.LENGTH_SHORT).show()

                        val intent_detail = Intent(context, ProductDetailActivity::class.java)
                        intent_detail.putExtra("id",trendingproductsDetails[position].Id)
                        intent_detail.putExtra("cat_id",trendingproductsDetails[position].ProductCategoryId)
                        intent_detail.putExtra("from_activity_name","trending_products")

//                        intent_detail.putExtra("name",trendingproductsDetails[position].Name)

//                        intent_detail.putExtra("producttype",trendingproductsDetails[position].ProductType)
//                        intent_detail.putExtra("imageId",trendingproductsDetails[position].ProductImageId)
//                        intent_detail.putExtra("category",trendingproductsDetails[position].ProductSubCategory)
//                        intent_detail.putExtra("dimentions",trendingproductsDetails[position].Dimentions)
//                        intent_detail.putExtra("desc",trendingproductsDetails[position].ProductDescription)
//                        intent_detail.putExtra("price",trendingproductsDetails[position].Price.toString())
//                        intent_detail.putExtra("qty",trendingproductsDetails[position].Quantity.toString())

                        startActivity(intent_detail)

                    }
                } )

            }
            if(newlyaddedDetails.size.equals(0)){
                binding.newlyAddedLayout.visibility = View.GONE
            } else {
                newlyAdapter?.updateList(newlyaddedDetails)
                newlyAdapter.setOnItemCLickListerner(object :NewlyAddedProductsAdapter.onItemClickListener{
                    override fun onItemCLick(position: Int) {

                        //Toast.makeText(context, newlyaddedDetails[position].Name, Toast.LENGTH_SHORT).show()

                        val intent_detail = Intent(context, ProductDetailActivity::class.java)
                        intent_detail.putExtra("id",newlyaddedDetails[position].Id)
                        intent_detail.putExtra("cat_id",newlyaddedDetails[position].ProductCategoryId)
                        intent_detail.putExtra("from_activity_name","newly_added_products")

//                        intent_detail.putExtra("name",newlyaddedDetails[position].Name)
//                        intent_detail.putExtra("producttype",newlyaddedDetails[position].ProductType)
//                        intent_detail.putExtra("imageId",newlyaddedDetails[position].ProductImage)
//                        intent_detail.putExtra("category",newlyaddedDetails[position].Category)
//                        intent_detail.putExtra("dimentions",newlyaddedDetails[position].Dimentions)
//                        intent_detail.putExtra("desc",newlyaddedDetails[position].ProductDescription)
//                        intent_detail.putExtra("price",newlyaddedDetails[position].Price.toString())
//                        intent_detail.putExtra("qty",newlyaddedDetails[position].Quantity.toString())

                        startActivity(intent_detail)

                    }
                } )

            }
            if(brandDetails.size.equals(0)){
                binding.brandLayout.visibility = View.GONE
            } else {
                brandAdapter?.updateList(brandDetails)
                brandAdapter.setOnItemCLickListerner(object :BrandAdapter.onItemClickListener{
                    override fun onItemCLick(position: Int) {

                        val intent_detail = Intent(context, BrandCategoryDetailActivity::class.java)
                        intent_detail.putExtra("id",brandDetails[position].Id)
                        intent_detail.putExtra("name",brandDetails[position].Brand)
                        startActivity(intent_detail)

                        //Toast.makeText(context, brandDetails[position].Brand, Toast.LENGTH_SHORT).show()

//                        val intent_detail = Intent(context, ProductDetailActivity::class.java)
//                        intent_detail.putExtra("name",targetListDetails[position].Name)
//                        intent_detail.putExtra("id",targetListDetails[position].Id)
//                        intent_detail.putExtra("producttype",targetListDetails[position].ProductType)
//                        intent_detail.putExtra("imageId",targetListDetails[position].ProductImage)
//                        intent_detail.putExtra("category",targetListDetails[position].Category)
//                        intent_detail.putExtra("dimentions",targetListDetails[position].Dimentions)
//                        intent_detail.putExtra("desc",targetListDetails[position].ProductDescription)
//                        intent_detail.putExtra("stock",targetListDetails[position].ProductStock)
//                        intent_detail.putExtra("price",targetListDetails[position].Price)
//                        intent_detail.putExtra("qty",targetListDetails[position].Quantity)
//
//                        startActivity(intent_detail)

                    }
                } )

            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun consumeWishliatResponse(response: ApiResponse<*>) {
        Log.e(HomeViewModel.TAG, "consumeLandingResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is WishlistResponseModel) {
                                wishListDetails.clear()
                                wishListDetails.addAll(it.WishListDetails)

                                updatewishList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //2 Toast.makeText(context, response.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeViewModel.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun updatewishList() {
        try {
            if(wishListDetails.size.equals(0)){
                binding.wishLayout.visibility = View.GONE
            } else {
                wishlistAdapter?.updateList(wishListDetails)
                wishlistAdapter.setOnItemCLickListerner(object :WishlistAdapter.onItemClickListener{
                    override fun onItemCLick(position: Int) {

                        //Toast.makeText(context, wishListDetails[position].Name, Toast.LENGTH_SHORT).show()

                        val intent_detail = Intent(context, ProductDetailActivity::class.java)
                        intent_detail.putExtra("id",wishListDetails[position].Id)
                        intent_detail.putExtra("cat_id",wishListDetails[position].ProductCategoryId)
                        intent_detail.putExtra("from_activity_name","wishlist_details")

                        startActivity(intent_detail)

                    }
                } )

            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun landingapiCall() {
        try {
            homeViewModel.LandingData(
                this,
                "Bearer " + AppConstants.ACCESS_TOKEN
            )
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun wishlistapiCall() {
        try {
            homeViewModel.WishlistData(
                this,
                "Bearer " + AppConstants.ACCESS_TOKEN
            )
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }


}