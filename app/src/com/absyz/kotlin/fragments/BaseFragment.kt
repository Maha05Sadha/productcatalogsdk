package com.absyz.kotlin.fragments

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.absyz.kotlin.activities.ui.BaseActivity
import com.absyz.kotlin.activities.ui.CreateVisit
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.dagger.factory.ViewModelFactory
import com.checkin.helpers.SessionStore
import com.checkin.helpers.Utility
import com.checkin.network.ErrorUtils
import javax.inject.Inject


abstract class BaseFragment : Fragment() {

    var dialog: Dialog? = null

//    @set:Inject
//    var pickMediaActivity: PickMediaActivity? = null

    @set:Inject
    var utility: Utility? = null

    @set:Inject
    var sessionStore: SessionStore? = null

//    @set:Inject
//    var inputValidator: InputValidator? = null

    @set:Inject
    var viewModelFactory: ViewModelFactory? = null

    @set:Inject
    var errorUtils: ErrorUtils? = null

    companion object {
        const val TAG = "BaseFragment"
    }

    abstract fun provideFragmentView(
        inflater: LayoutInflater?,
        parent: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?

    override fun onCreateView(
        inflater: LayoutInflater,
        parent: ViewGroup?,
        savedInstanseState: Bundle?): View? {
        Log.e(TAG,"onCreateView()")
        val rootView = provideFragmentView(inflater,parent,savedInstanseState)
        (requireActivity().application as ApplicationController).applicationComponent.inject(this)
        dialog = utility!!.getProgressDialogCircle(activity)
        return rootView
    }

    fun onBackPressed() {
        try {
            if (activity?.supportFragmentManager?.backStackEntryCount!! > 1) {
                activity?.supportFragmentManager?.popBackStack()
            } else {
                super.getActivity()?.onBackPressed()
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }



    internal fun firstTimeCreated(savedInstanceState: Bundle?) = savedInstanceState == null

    open fun showLoader() {
        try {
            Log.e(TAG, "showLoader()")
            requireActivity().runOnUiThread {
                try {
                    if (dialog != null) {
                        dialog!!.show()
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
            Log.e(BaseActivity.TAG, "showLoader() Exception - " + ex.message)
        }
    }

    open fun dismissLoader() {
        try {
            Log.e(TAG, "dismissLoader()")
            requireActivity().runOnUiThread {
                try {
                    if (dialog != null) {
                        dialog!!.dismiss()
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
            Log.e(BaseActivity.TAG, "showLoader() Exception - " + ex.message)
        }
    }


}