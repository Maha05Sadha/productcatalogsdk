package com.absyz.kotlin.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.absyz.kotlin.R
import com.absyz.kotlin.activities.ui.ProductDetailActivity
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.FragmentSalesTargetBinding
import com.absyz.kotlin.model.RequestModel.SalesTargetRequestModel
import com.absyz.kotlin.model.ResponseModel.SalesTargetListDetails
import com.absyz.kotlin.model.ResponseModel.SalesTargetResponseModel
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel
import com.monsterbrain.recyclerviewtableview.TargetSalesAdapter

class SalesTargetFragment : BaseFragment() {

    lateinit var binding: FragmentSalesTargetBinding
    var targetlist: MutableList<SalesTargetListDetails> = ArrayList()

    //private var tableRecycler : RecyclerView? = null
    lateinit var targetsalesAdapter: TargetSalesAdapter

    lateinit var homeViewModel: HomeViewModel

    private var targetListDetails: ArrayList<SalesTargetListDetails> = ArrayList()

    var targetPostString: SalesTargetRequestModel ?= null

    var pagenumer : String = "1"
    var current_pg : String = ""
    var last_pg : String = ""
    var target_count:Int = 0

    @Override
    override fun provideFragmentView(
        inflater: LayoutInflater?, parent: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding =
            DataBindingUtil.inflate(inflater!!, R.layout.fragment_sales_target, parent, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        try{
            (requireActivity() as AppCompatActivity).supportActionBar?.hide()

            (activity?.application as ApplicationController).applicationComponent.inject(this)
            setadapter()

            //View model instance
            homeViewModel =
                ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

            //Observer
            homeViewModel!!.getSalesTargetResponse()
                .observe(viewLifecycleOwner, { this.consumeSalesTargetResponse(it as ApiResponse<*>) })

            //Api Call
            targetPostString = SalesTargetRequestModel(
                salespersonId = AppConstants.SalesPersonID)
            sendAPi(targetPostString!!,pagenumer)

            binding.burgerMenu.setOnClickListener {
                try {
                    if (activity?.supportFragmentManager?.backStackEntryCount!! > 1) {
                        activity?.supportFragmentManager?.popBackStack()
                    } else {
                        super.getActivity()?.onBackPressed()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
                //Toast.makeText(context,"Back icon pressed", Toast.LENGTH_SHORT).show()
            }


        }catch (ex: Exception){
            ex.printStackTrace()
        }
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun setadapter() {

        try {
            targetsalesAdapter = TargetSalesAdapter(activity!!, utility!!)
//            utility!!.getRecyclerviewforgrid(context, binding.targetRV,false,false)!!.adapter = targetsalesAdapter
            val layoutManager = GridLayoutManager(context, 3)
            binding.targetRV.setLayoutManager(layoutManager);
            binding.targetRV.setHasFixedSize(true)
            binding.targetRV.setAdapter(targetsalesAdapter);




//            targetsalesAdapter = TargetSalesAdapter(activity!!, utility!!)
//            utility!!.getRecyclerview(context, binding.targetRV,false,false)!!.adapter =
//                targetsalesAdapter
//            gridLayoutManager = GridLayoutManager(context,4, LinearLayoutManager.VERTICAL,false)
//            binding.targetRV!!.layoutManager = gridLayoutManager
//            binding.targetRV.setHasFixedSize(true)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun consumeSalesTargetResponse(response: ApiResponse<*>) {
        Log.e(TAG, "consumeSalesTargetResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                   showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is SalesTargetResponseModel) {
                                targetListDetails.addAll(it.TargetListDetails!!)
                                current_pg = it.currentPage.toString()
                                last_pg = it.lastPage.toString()
                                updateSaelsTargetList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(context,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun updateSaelsTargetList() {
        try {
            target_count = targetListDetails.size
            binding.targetCountVal.text = target_count.toString()
            targetsalesAdapter.updateList(targetListDetails)
            targetsalesAdapter.setOnItemCLickListerner(object :TargetSalesAdapter.onItemClickListener{
                override fun onItemCLick(position: Int) {

                    //Toast.makeText(context, targetListDetails[position].Name, Toast.LENGTH_SHORT).show()

                    val intent_detail = Intent(context, ProductDetailActivity::class.java)
                    intent_detail.putExtra("id",targetListDetails[position].Id)
                    intent_detail.putExtra("from_activity_name","salestarget_details")

                    startActivity(intent_detail)

                }
            } )
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }


    fun sendAPi(targetPostString: SalesTargetRequestModel,pagenumber: String) {
        try {
            homeViewModel?.SendSalesTargetData(this,targetPostString,"Bearer " + AppConstants.ACCESS_TOKEN,pagenumber)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


}