package com.absyz.kotlin.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.absyz.kotlin.R
import com.absyz.kotlin.Retrofit.IOnBackPressed
import com.absyz.kotlin.activities.ui.AccountUpdateActivity
import com.absyz.kotlin.activities.ui.BaseActivity
import com.absyz.kotlin.activities.ui.CartActivity
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.FragmentAccountBinding
import com.absyz.kotlin.databinding.FragmentViewLogsBinding
import com.absyz.kotlin.model.RequestModel.*
import com.absyz.kotlin.model.ResponseModel.*
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel
import com.monsterbrain.recyclerviewtableview.VisitLogsAdapter

class ViewLogsFragment : BaseFragment() {

    lateinit var binding: FragmentViewLogsBinding

    lateinit var visitLogsAdapter: VisitLogsAdapter

    lateinit var homeViewModel: HomeViewModel

    private var cartProductdetails: ArrayList<ProductCartDetails> = ArrayList()
    var cartprodReq: CartProductRequest?= null

    private var visitorDetails: ArrayList<VisitorDetails> = ArrayList()
    var visitReq: VisitLogsRequestModel?= null
    var removeReq : InActiveCustomerRequest?= null
    var visit_count : Int = 0
    var customer_id : String = ""
    var cust_message : String = ""

    @Override
    override fun provideFragmentView(
        inflater: LayoutInflater?, parent: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding =
            DataBindingUtil.inflate(inflater!!, R.layout.fragment_view_logs, parent, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        try{
            /*Main Content Area starts*/
            (requireActivity() as AppCompatActivity).supportActionBar?.hide()

            (activity?.application as ApplicationController).applicationComponent.inject(this)
            setadapter()

            //View model instance
            homeViewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

            //Observer
            homeViewModel!!.getVisitLogsResponse()
                .observe(viewLifecycleOwner, { this.consumeVisitLogsResponse(it as ApiResponse<*>) })

            visitReq = VisitLogsRequestModel(
                salespersonId = AppConstants.SalesPersonID
            )
            sendvisitorAPi(visitReq!!)

            //InActive Customer Observer
            homeViewModel!!.getInActiveCustomerResponse()?.observe(viewLifecycleOwner, { this.consumeInActiveCustomerResponse(it as ApiResponse<*>) })

            binding.burgerMenu.setOnClickListener {
                try {
                    if (activity?.supportFragmentManager?.backStackEntryCount!! > 1) {
                        activity?.supportFragmentManager?.popBackStack()
                    } else {
                        super.getActivity()?.onBackPressed()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }                //Toast.makeText(context,"Back icon pressed", Toast.LENGTH_SHORT).show()
            }

            //Observer
//            homeViewModel!!.getCartProductDetailsResponse()?.observe(viewLifecycleOwner, { this.ConsumeCartProductData(it as ApiResponse<*>) })
//
//            cartprodReq = CartProductRequest(
//                customerId = AppConstants.CustomerID
//            )
//            apiCall(cartprodReq!!)

//            binding.cartIcon.setOnClickListener{
//                val intent_detail = Intent(context, CartActivity::class.java)
//                startActivity(intent_detail)
//            }


        }catch (ex: Exception){
            ex.printStackTrace()
        }
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun setadapter() {

        try {

            visitLogsAdapter = VisitLogsAdapter(activity!!, utility!!){index -> deleteItem(index)}
            utility!!.getRecyclerview(context, binding.visitRv,false,false)!!.adapter =
                visitLogsAdapter

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun consumeVisitLogsResponse(response: ApiResponse<*>) {
        Log.e(HomeViewModel.TAG, "consumeVisitLogsResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()

                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    //progressDialog?.dismiss()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is VisitLogsResponseModel) {
                                visitorDetails.clear()
                                visitorDetails.addAll(it.VisitorDetails!!)
                                updateVisitLogsList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(context,response.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeViewModel.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun updateVisitLogsList() {
        try {
            visit_count = visitorDetails.size
            binding.accCountVal.text = visit_count.toString()
            visitLogsAdapter?.updateList(visitorDetails)
            visitLogsAdapter?.setOnItemCLickListerner(object : VisitLogsAdapter.onItemClickListener{
                override fun onItemCLick(position: Int) {
                    Toast.makeText(context, visitorDetails[position].Name, Toast.LENGTH_SHORT).show()
                }
            } )


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun sendvisitorAPi(visitlogsReq: VisitLogsRequestModel) {
        try {
            homeViewModel?.VisitLogsData("Bearer " + AppConstants.ACCESS_TOKEN,visitlogsReq)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

//    private fun apiCall(cartprodReq: CartProductRequest) {
//
//        try {
//            homeViewModel?.CartProductDetails(
//                cartprodReq,
//                "Bearer " + AppConstants.ACCESS_TOKEN)
//        } catch (ex: Exception) {
//            ex.printStackTrace();
//        }
//
//    }
//
//    fun ConsumeCartProductData(apiResponse: ApiResponse<*>) {
//        Log.e(BaseActivity.TAG, "ConsumeCartProductData()")
//        try{
//            when (apiResponse.apiStatus) {
//                ApiStatus.LOADING -> showLoader()
//
//                ApiStatus.SUCCESS ->{
//                    dismissLoader()
//                    apiResponse?.let { apiResponse ->
//                        apiResponse.data?.let {
//                            if (it is CartProductResponse) {
//                                cartProductdetails.addAll(it.ProductCartDetails!!)
//                                AppConstants.cartItemCount = cartProductdetails.size
//                                binding.cartCount.text = AppConstants.cartItemCount.toString()
//
//                            }
//                        }
//                    }
//                }
//
//                ApiStatus.ERROR -> {
//                    dismissLoader()
//                    Toast.makeText(context,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
//                }
//            }
//        }catch (ex:Exception) {
//            ex.printStackTrace();
//        }
//    }

    class MyFragment : Fragment(), IOnBackPressed {
        @SuppressLint("UseRequireInsteadOfGet")
        override fun onBackPressed(): Boolean {
            return if (fragmentManager!!.backStackEntryCount > 1) {
                //action not popBackStack
                fragmentManager!!.popBackStack()
                true
            } else {
                false
            }
        }
    }

    fun deleteItem(index: Int){
        //customer_id = visitorDetails[index].Id

        removeReq = InActiveCustomerRequest(
            customerId = visitorDetails[index].Id
        )
        inactiveapiCall(removeReq!!)
        //visitorDetails.removeAt(index)
        visitLogsAdapter.setItems(visitorDetails)



    }

    private fun inactiveapiCall(removeReq: InActiveCustomerRequest) {

        try {
            homeViewModel.InactiveCustomerData(
                removeReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeInActiveCustomerResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeInActiveCustomerResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is InActiveCustomerResponse) {
//                                messageList.add(it.message)
                                cust_message = it.Message
                                Toast.makeText(context,it.Message,Toast.LENGTH_SHORT).show()
                                visitReq = VisitLogsRequestModel(
                                    salespersonId = AppConstants.SalesPersonID
                                )
                                sendvisitorAPi(visitReq!!)
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    visitReq = VisitLogsRequestModel(
                        salespersonId = AppConstants.SalesPersonID
                    )
                    sendvisitorAPi(visitReq!!)
                    Toast.makeText(context,"Visit De-activated",Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

}
