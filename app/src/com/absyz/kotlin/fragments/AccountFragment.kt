package com.absyz.kotlin.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.absyz.kotlin.R
import com.absyz.kotlin.activities.ui.*
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.FragmentAccountBinding
import com.absyz.kotlin.model.RequestModel.CartProductRequest
import com.absyz.kotlin.model.ResponseModel.*
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel
import com.monsterbrain.recyclerviewtableview.AccountsAdapter

class AccountFragment : BaseFragment() {

    lateinit var binding: FragmentAccountBinding
    var accountsList: MutableList<AccountsUserDetails> = ArrayList()

    lateinit var accountAdapter: AccountsAdapter

    lateinit var homeViewModel: HomeViewModel

    private var accountsDetails: ArrayList<AccountsUserDetails> = ArrayList()

    var pagenumer : String = "1"
    var current_pg : String = ""
    var next_pg: Int = 0
    var last_pg : Int = 0
    var account_count: Int = 0

    private var cartProductdetails: ArrayList<ProductCartDetails> = ArrayList()
    var cartprodReq: CartProductRequest?= null

    private var customerDetails: ArrayList<RecentCustomerDetails> = ArrayList()


    @Override
    override fun provideFragmentView(
        inflater: LayoutInflater?, parent: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding =
            DataBindingUtil.inflate(inflater!!, R.layout.fragment_account, parent, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        try{
            /*Main Content Area starts*/
            (requireActivity() as AppCompatActivity).supportActionBar?.hide()

            (activity?.application as ApplicationController).applicationComponent.inject(this)
            setadapter()

            //View model instance
            homeViewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

            //Recent Customer Observer
            homeViewModel.getRecentCustomerResponse()?.observe(viewLifecycleOwner, { this.ConsumeRecentCustomerData(it as ApiResponse<*>) })
            //Call Recent Customer APi
            recentCustomerapiCall()

            //Observer
            homeViewModel!!.getAccountsResponse()
                .observe(viewLifecycleOwner, { this.consumeAccountsResponse(it as ApiResponse<*>) })

            sendAPi(pagenumer)

            binding.burgerMenu.setOnClickListener {
                try {
                    if (activity?.supportFragmentManager?.backStackEntryCount!! > 1) {
                        activity?.supportFragmentManager?.popBackStack()
                    } else {
                        super.getActivity()?.onBackPressed()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
                //Toast.makeText(context,"Back icon pressed", Toast.LENGTH_SHORT).show()
            }

            //Observer
            homeViewModel!!.getCartProductDetailsResponse()?.observe(viewLifecycleOwner, { this.ConsumeCartProductData(it as ApiResponse<*>) })

            cartprodReq = CartProductRequest(
                customerId = AppConstants.CustomerID
            )
            apiCall(cartprodReq!!)

            binding.cartIcon.setOnClickListener{
                val intent_detail = Intent(context, CartActivity::class.java)
                startActivity(intent_detail)
            }

            binding.accsearchText.setOnClickListener(){
                //Toast.makeText(this,"Currently not Available!",Toast.LENGTH_SHORT).show()

                val intent = Intent(context, SearchProductActivity::class.java)
                intent.putExtra("fromactivity","accounts")
                startActivity(intent)
            }

            binding.searchView.setOnClickListener(){
                //Toast.makeText(this,"Currently not Available!",Toast.LENGTH_SHORT).show()

                val intent = Intent(context, SearchProductActivity::class.java)
                intent.putExtra("fromactivity","accounts")
                startActivity(intent)
            }

            binding.searchIcon.setOnClickListener(){
                //Toast.makeText(this,"Currently not Available!",Toast.LENGTH_SHORT).show()

                val intent = Intent(context, SearchProductActivity::class.java)
                intent.putExtra("fromactivity","accounts")
                startActivity(intent)
            }

        }catch (ex: Exception){
            ex.printStackTrace()
        }
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun setadapter() {

        try {

            accountAdapter = AccountsAdapter(activity!!, utility!!)
            utility!!.getRecyclerview(context, binding.accountRv,false,false)!!.adapter =
                accountAdapter

        } catch (ex: Exception) {
            ex.printStackTrace()
        }


//
//        val prodlayoutManager: RecyclerView.LayoutManager =
//            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
//        val trendlayoutManager: RecyclerView.LayoutManager =
//            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
//        val newlylayoutManager: RecyclerView.LayoutManager =
//            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
//        val brandslayoutManager: RecyclerView.LayoutManager =
//            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
//        val wishlistlayoutManager: RecyclerView.LayoutManager =
//            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

//        product_cat_rv!!.layoutManager = prodlayoutManager

//        productCatAdapter = view?.let { ProductCategoriesAdapter(it.context) }

//        trending_rv!!.layoutManager = trendlayoutManager
//        trendingAdapter = view?.let { TrendingProductsAdapter(it.context) }
//
//        newlyadded_rv!!.layoutManager = newlylayoutManager
//        newlyAdapter = view?.let { NewlyAddedProductsAdapter(it.context) }
//
//        brand_rv!!.layoutManager = brandslayoutManager
//        brandAdapter = view?.let { BrandAdapter(it.context) }
//
//        wishlist_rv!!.layoutManager = wishlistlayoutManager
//        wishlistAdapter = view?.let { WishlistAdapter(it.context) }
    }


    private fun consumeAccountsResponse(response: ApiResponse<*>) {
        Log.e(HomeViewModel.TAG, "consumeAccountsResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
//                    progressDialog!!.setMessage("Please Wait..")
//                    progressDialog.setCanceledOnTouchOutside(false)
//                    progressDialog.setCancelable(false)
//                    progressDialog.show()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    //progressDialog?.dismiss()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is AccountsResponseModel) {
                                accountsDetails.clear()
                                accountsDetails.addAll(it.UserDetails!!)
                                current_pg = it.currentPage.toString()
                                last_pg = it.lastPage!!
                                updateAccountsList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(context,response.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(HomeViewModel.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun updateAccountsList() {
        try {
            account_count = accountsDetails.size
            binding.accCountVal.text = account_count.toString()
            accountAdapter?.updateList(accountsDetails)
            accountAdapter?.setOnItemCLickListerner(object :AccountsAdapter.onItemClickListener{
                override fun onItemCLick(position: Int) {

                    //Toast.makeText(context, accountsDetails[position].Name, Toast.LENGTH_SHORT).show()

                    val intent_detail = Intent(context, AccountUpdateActivity::class.java)
                    intent_detail.putExtra("name",accountsDetails[position].Name)
                    intent_detail.putExtra("id",accountsDetails[position].Id)
                    intent_detail.putExtra("phone",accountsDetails[position].Phone)
                    intent_detail.putExtra("email",accountsDetails[position].Email)
                    intent_detail.putExtra("address",accountsDetails[position].Address)

//                    intent_detail.putExtra("state",accountsDetails[position].BillingState)
//                    intent_detail.putExtra("city",accountsDetails[position].BillingCity)

                    startActivity(intent_detail)

                }
            } )


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun sendAPi(pagenumber: String) {
        current_pg = pagenumber
        try {
            for(i in pagenumber?.toInt()..last_pg){
                current_pg = i.toString()
            }
            homeViewModel?.AccountsData(this,"Bearer " + AppConstants.ACCESS_TOKEN,current_pg)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun apiCall(cartprodReq: CartProductRequest) {

        try {
            homeViewModel?.CartProductDetails(
                cartprodReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeCartProductData(apiResponse: ApiResponse<*>) {
        Log.e(BaseActivity.TAG, "ConsumeCartProductData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is CartProductResponse) {
                                cartProductdetails.addAll(it.ProductCartDetails!!)
                                AppConstants.cartItemCount = cartProductdetails.size
                                binding.cartCount.text = AppConstants.cartItemCount.toString()

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(context,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun recentCustomerapiCall() {

        try {
            homeViewModel.recentCustomerDetails(
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeRecentCustomerData(apiResponse: ApiResponse<*>) {
        Log.e(BaseActivity.TAG, "ConsumeRecentCustomerData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is RecentLoginCustomerResponse) {
                                customerDetails.addAll(it.CustomerDetails!!)
                                updateRecentCustomerDetails()
                                //Toast.makeText(this, recentCustomerdetails?.get(0)?.Name.toString(),Toast.LENGTH_SHORT).show()

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(context,apiResponse.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateRecentCustomerDetails() {
        try {
            val recentCustomerModel = customerDetails[0]
            //recent_customer_name = recentCustomerdetails?.get(0)?.Name.toString()
            AppConstants.Customer_Name = recentCustomerModel!!.Name
            AppConstants.CustomerID = recentCustomerModel!!.Id
            AppConstants.cartItemCount = recentCustomerModel!!.CartListCount
            //Toast.makeText(this,AppConstants.Customer_Name,Toast.LENGTH_SHORT).show()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * tied to [Activity.onResume] of the containing
     * Activity's lifecycle.
     */
    override fun onResume() {
        super.onResume()
        sendAPi("1")
    }


    //    class MyFragment : Fragment(), IOnBackPressed {
//        @SuppressLint("UseRequireInsteadOfGet")
//        override fun onBackPressed(): Boolean {
//            return if (fragmentManager!!.backStackEntryCount > 1) {
//                //action not popBackStack
//                fragmentManager!!.popBackStack()
//                true
//            } else {
//                false
//            }
//        }
//    }

    }
