package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class FilterColors (
    @SerializedName("Colour") var Colour : String
)