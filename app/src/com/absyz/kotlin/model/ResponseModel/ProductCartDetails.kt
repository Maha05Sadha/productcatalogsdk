package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class ProductCartDetails (
    @SerializedName("Id") var Id : String,
    @SerializedName("Name") var Name : String,
    @SerializedName("ProductCategoryId") var ProductCategoryId : String,
    @SerializedName("CartProductAvailability") var CartProductAvailability : Boolean,
    @SerializedName("ProductColourCode") var ProductColourCode : String,
    @SerializedName("ProductColourName") var ProductColourName : String,
    @SerializedName("ProductImage") var ProductImage : String,
    @SerializedName("ProductLocation") var ProductLocation : String,
    @SerializedName("SalesPersonId") var SalesPersonId : String,
    @SerializedName("IsProductAvailable?") var IsProductAvailable : Boolean,
    @SerializedName("Quantity") var Quantity : Int,
    @SerializedName("GrossTotal") var GrossTotal : Int,
    @SerializedName("ProductId") var ProductId : String,
    @SerializedName("ProductName") var ProductName : String,
    @SerializedName("Brand") var Brand : String,
    @SerializedName("ProductType") var ProductType : String,
    @SerializedName("ProductDescription") var ProductDescription : String,
    @SerializedName("Material") var Material : String,
    @SerializedName("Warranty") var Warranty : String,
    @SerializedName("ProductSKU") var ProductSKU : String,
    @SerializedName("TotalProductQuantity") var TotalProductQuantity : Int,
    @SerializedName("Assembly") var Assembly : String,
    @SerializedName("Dimentions") var Dimentions : String,
    @SerializedName("Weight") var Weight : String,
    @SerializedName("SeatingHight") var SeatingHight : Double,
    @SerializedName("Price") var Price : Int,
    @SerializedName("ProductSubCategory") var ProductSubCategory : String,
    @SerializedName("CustomerName") var CustomerName : String,
    @SerializedName("CustomerMobileNumber") var CustomerMobileNumber : String,
    @SerializedName("CustomerAddress") var CustomerAddress : String,
    @SerializedName("CustomerEmail") var CustomerEmail : String
        )
