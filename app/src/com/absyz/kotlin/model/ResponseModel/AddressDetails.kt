package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class AddressDetails (
    @SerializedName("Address") var Address : String
        )