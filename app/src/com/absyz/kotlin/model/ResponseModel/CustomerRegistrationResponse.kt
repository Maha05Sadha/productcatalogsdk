package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class CustomerRegistrationResponse (
    @SerializedName("attributes") var attributes : CustomerRegistrationAttribute,
    @SerializedName("Phone") var Phone : String,
    @SerializedName("Name") var Name : String,
    @SerializedName("Email__c") var Email_c : String,
    @SerializedName("Address__c") var Address_c : String,
    @SerializedName("OwnerId") var OwnerId : String,
    @SerializedName("Active__c") var Active_c : String,
    @SerializedName("Login_Time__c") var LoginTime_c : String,
    @SerializedName("Id") var Id : String
)