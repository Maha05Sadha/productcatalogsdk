package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class ProductStorageList (
    @SerializedName("StockValue") var StockValue : Int,
    @SerializedName("Address") var Address : String


)