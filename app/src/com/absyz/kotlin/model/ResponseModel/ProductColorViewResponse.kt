package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class ProductColorViewResponse (
    @SerializedName("ProductStorageList") var ProductStorageList : List<ProductColorViewDetails>

)