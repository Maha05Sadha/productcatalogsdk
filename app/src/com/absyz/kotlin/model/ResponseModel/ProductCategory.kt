package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class ProductCategory
    (
    @SerializedName("Id") var Id : String,
    @SerializedName("ProductCategory") var ProductCategory : String,
    @SerializedName("productDetails") var productDetails : String
    )