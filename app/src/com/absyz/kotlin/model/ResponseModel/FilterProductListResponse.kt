package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class FilterProductListResponse (
    @SerializedName("ProductDetails") var ProductDetails : List<FilterProductDetails>
)