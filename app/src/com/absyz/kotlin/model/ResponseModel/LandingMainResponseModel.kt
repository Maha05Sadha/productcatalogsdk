package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class LandingMainResponseModel (

    var contentTitle: String,
    @SerializedName("ProductCategory") var ProductCategory : List<ProductCategory>,
    @SerializedName("TrendingProductsForTheStore") var TrendingProductsForTheStore : List<TrendingProductsForTheStore>,
    @SerializedName("NewlyAddedProducts") var NewlyAddedProducts : List<NewlyAddedProducts>,
    @SerializedName("Brands") var Brands : List<Brands>

)