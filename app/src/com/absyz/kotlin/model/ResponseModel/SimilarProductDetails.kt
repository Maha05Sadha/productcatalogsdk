package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class SimilarProductDetails (
    @SerializedName("ProductId") var ProductId : String,
    @SerializedName("Name") var Name : String,
    @SerializedName("IsProductAvailable?") var IsProductAvailable : Boolean,
    @SerializedName("ProductCategoryId") var ProductCategoryId : String,
    @SerializedName("ProductLocation") var ProductLocation : String,
    @SerializedName("ProductName") var ProductName : String,
    @SerializedName("Brand") var Brand : String,
    @SerializedName("ProductType") var ProductType : String,
    @SerializedName("ProductDescription") var ProductDescription : String,
    @SerializedName("Material") var Material : String,
    @SerializedName("Warranty") var Warranty : String,
    @SerializedName("ProductImageId") var ProductImageId : String,
    @SerializedName("ProductSKU") var ProductSKU : String,
    @SerializedName("Quantity") var Quantity : Int,
    @SerializedName("Assembly") var Assembly : String,
    @SerializedName("Dimentions") var Dimentions : String,
    @SerializedName("Weight") var Weight : String,
    @SerializedName("SeatingHight") var SeatingHight : Double,
    @SerializedName("Colour") var Colour : String,
    @SerializedName("ColourName") var ColourName : String,
    @SerializedName("Price") var Price : Int,
    @SerializedName("ProductSubCategory") var ProductSubCategory : String
        )