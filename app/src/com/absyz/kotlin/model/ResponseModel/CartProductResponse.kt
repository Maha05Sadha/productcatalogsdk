package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class CartProductResponse (
    @SerializedName("ProductCartDetails") var ProductCartDetails : List<ProductCartDetails>,
    @SerializedName("CartListCount") var CartListCount : Int,
    @SerializedName("TotalPrice") var TotalPrice : Double
)