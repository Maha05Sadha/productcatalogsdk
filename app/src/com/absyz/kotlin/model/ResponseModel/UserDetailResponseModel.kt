package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class UserDetailResponseModel (
    @SerializedName("UserDetails") var UserDetails : List<UserDetails>
)