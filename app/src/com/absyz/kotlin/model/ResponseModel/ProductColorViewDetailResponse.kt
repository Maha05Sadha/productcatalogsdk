package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class ProductColorViewDetailResponse (
    @SerializedName("Addresses") var Addresses : List<AddressDetails>,
    @SerializedName("StockValues") var StockValues : List<StockValueDetails>,
    @SerializedName("ColourCodes") var ColourCodes : List<ColorCodeDetails>,
    @SerializedName("ColourNames") var ColourNames : List<ColorNameDetails>,
    @SerializedName("ProductImages") var ProductImages : List<ColorPorductImageDetails>
)