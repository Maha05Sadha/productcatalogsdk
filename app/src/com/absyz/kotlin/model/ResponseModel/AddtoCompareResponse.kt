package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

data class AddtoCompareResponse(

    @SerializedName("Message") var Message : String,
    @SerializedName("ProductAvailable") var ProductAvailable : Boolean


)