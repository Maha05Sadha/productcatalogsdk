package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class VisitorCreationAttribute (
    @SerializedName("type") var type : String,
    @SerializedName("url") var url : String
        )