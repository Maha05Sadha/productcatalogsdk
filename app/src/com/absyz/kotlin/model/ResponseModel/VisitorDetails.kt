package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class VisitorDetails (
    @SerializedName("Id") var Id : String,
    @SerializedName("Name") var Name : String,
    @SerializedName("SalesPerson") var SalesPerson : String,
    @SerializedName("Status") var Status : String,
    @SerializedName("CustomerName") var CustomerName : String,
    @SerializedName("CustomerAddress") var CustomerAddress : String,
    @SerializedName("CustomerPhone") var CustomerPhone : String,
    @SerializedName("CustomerEmail") var CustomerEmail : String
        )