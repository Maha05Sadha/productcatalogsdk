package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class IncreaseProductQtyResponse (
    @SerializedName("Message") var Message : String
        )