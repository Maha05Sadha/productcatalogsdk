package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class WishlistResponseModel (
    @SerializedName("WishListDetails") var WishListDetails : List<WishListDetails>,
    @SerializedName("WishlistCount") var WishlistCount : Int
)