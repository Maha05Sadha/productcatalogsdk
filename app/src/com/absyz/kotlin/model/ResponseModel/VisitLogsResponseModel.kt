package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class VisitLogsResponseModel (
    @SerializedName("VisitorDetails") var VisitorDetails : List<VisitorDetails>
        )