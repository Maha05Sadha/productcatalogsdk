package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class CurrentVistListResponse (
    @SerializedName("CustomerList") var CustomerList : List<CustomerListDetails>,
    @SerializedName("VisitorList") var VisitorList : List<VisiterListDetails>
        )