package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class SimilarProductResponse (
    @SerializedName("ProductCartDetails") var ProductCartDetails : List<SimilarProductDetails>
        )