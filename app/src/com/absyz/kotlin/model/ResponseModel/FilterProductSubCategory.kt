package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class FilterProductSubCategory (
    @SerializedName("Type") var Type : String

)