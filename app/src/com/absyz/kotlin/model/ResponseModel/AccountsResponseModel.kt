package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class AccountsResponseModel (
    @SerializedName("UserDetails") var UserDetails : List<AccountsUserDetails>,
    @SerializedName("current_page") var currentPage : String,
    @SerializedName("last_page") var lastPage : Int,
    @SerializedName("Per_page") var PerPage : Int,
    @SerializedName("Total") var Total : Int
        )