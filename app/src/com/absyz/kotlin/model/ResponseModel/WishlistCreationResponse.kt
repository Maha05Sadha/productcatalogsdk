package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class WishlistCreationResponse (
    @SerializedName("attributes") var attributes : WishlistCreationAttribute,
    @SerializedName("Customer__c") var Customer_c : String,
    @SerializedName("Product__c") var Product_c : String,
    @SerializedName("Id") var Id : String
        )