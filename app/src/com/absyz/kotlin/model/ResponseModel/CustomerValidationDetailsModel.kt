package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

data class CustomerValidationDetailsModel
    (
    @SerializedName("Id") var Id : String,
    @SerializedName("Name") var Name : String,
    @SerializedName("Phone") var Phone : String,
    @SerializedName("Email__c") var Email_c : String,
    @SerializedName("Address") var Address : String,
    @SerializedName("Error") var Error : String
)
