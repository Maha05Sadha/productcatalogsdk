package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class CustomerCheckInResponse (
    @SerializedName("attributes") var attributes : CustomerCheckInAttribute,
    @SerializedName("Id") var Id : String,
    @SerializedName("Name") var Name : String,
    @SerializedName("Login_Time__c") var LoginTime_c : String,
    @SerializedName("Active__c") var Active_c : String
        )