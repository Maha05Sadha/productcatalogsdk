package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class VisitorCreationResponse (
    @SerializedName("attributes") var attributes : VisitorCreationAttribute,
    @SerializedName("OwnerId") var OwnerId : String,
    @SerializedName("Visit_Status__c") var VisitStatus_c : String,
    @SerializedName("Visitor_Login_Time__c") var VisitorLoginTime_c : String,
    @SerializedName("Id") var Id : String
)