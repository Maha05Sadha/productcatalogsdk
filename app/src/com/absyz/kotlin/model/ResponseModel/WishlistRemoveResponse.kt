package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class WishlistRemoveResponse (
    @SerializedName("Message") var Message : String

        )