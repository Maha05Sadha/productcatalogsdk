package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class ProductColorViewDetails (
    @SerializedName("Id") var Id : String,
    @SerializedName("StockValue") var StockValue : Int,
    @SerializedName("Address") var Address : String,
    @SerializedName("ColourCode") var ColourCode : String,
    @SerializedName("ColourName") var ColourName : String,
    @SerializedName("ProductImage") var ProductImage : String
)