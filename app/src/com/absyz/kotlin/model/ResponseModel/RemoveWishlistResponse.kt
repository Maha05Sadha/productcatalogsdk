package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class RemoveWishlistResponse (
    @SerializedName("Message") var Message : String

)