package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class ProductCategoryDetailsResponseModel (
    @SerializedName("ProductDetails") var ProductDetails : List<ProductCategoryDetails>,
    @SerializedName("current_page") var currentPage : String,
    @SerializedName("last_page") var lastPage : Int,
    @SerializedName("Per_page") var PerPage : Int,
    @SerializedName("Total") var Total : Int
        )