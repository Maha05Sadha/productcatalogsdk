package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class InActiveCustomerResponse (
    @SerializedName("Message") var Message : String
        )