package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class ProductColorCodes (
    @SerializedName("ProductColourCode") var ProductColourCode : String,
    @SerializedName("ProductColourName") var ProductColourName : String

)