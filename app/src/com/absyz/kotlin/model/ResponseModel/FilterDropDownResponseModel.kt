package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class FilterDropDownResponseModel (
    @SerializedName("ProductSubCategories") var ProductSubCategories : List<FilterProductSubCategory>,
    @SerializedName("Materials") var Materials : List<FilterMaterial>,
    @SerializedName("Colours") var Colours : List<FilterColors>
        )