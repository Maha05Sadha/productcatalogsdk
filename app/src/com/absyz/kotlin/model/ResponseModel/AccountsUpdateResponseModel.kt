package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class AccountsUpdateResponseModel (
    @SerializedName("attributes") var attributes : AccountsAttributes,
    @SerializedName("Id") var Id : String,
    @SerializedName("Name") var Name : String,
    @SerializedName("Phone") var Phone : String,
    @SerializedName("Email__c") var Email_c : String,
    @SerializedName("Address__c") var Address_c : String
    )