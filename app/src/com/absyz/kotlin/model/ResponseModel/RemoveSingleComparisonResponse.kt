package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class RemoveSingleComparisonResponse (
    @SerializedName("attributes") var attributes : RemoveSinglecomparisonatrribute,
    @SerializedName("Id") var Id : String,
    @SerializedName("Name") var Name : String,
    @SerializedName("Product__c") var Product_c : String
        )