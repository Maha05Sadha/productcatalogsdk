package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class CurrentVisitResponseModel (
    @SerializedName("CustomerList") var CustomerList : List<CustomerList>,
    @SerializedName("VisitorList") var VisitorList : List<VisitorList>
        )