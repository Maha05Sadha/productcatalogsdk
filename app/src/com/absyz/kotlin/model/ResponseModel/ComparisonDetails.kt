package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class ComparisonDetails (
    @SerializedName("Id") var Id : String,
    @SerializedName("Name") var Name : String,
    @SerializedName("SalesPersonId") var SalesPersonId : String,
    @SerializedName("ColourName") var ColourName : String,
    @SerializedName("IsProductAvailable?") var IsProductAvailable : Boolean,
    @SerializedName("ProductName") var ProductName : String,
    @SerializedName("ProductLocation") var ProductLocation : String,
    @SerializedName("ProductId") var ProductId : String,
    @SerializedName("Brand") var Brand : String,
    @SerializedName("ProductCategoryId") var ProductCategoryId : String,
    @SerializedName("ProductCategoryName") var ProductCategoryName : String,
    @SerializedName("ProductType") var ProductType : String,
    @SerializedName("ProductDescription") var ProductDescription : String,
    @SerializedName("Material") var Material : String,
    @SerializedName("Warranty") var Warranty : String,
    @SerializedName("ProductImageId") var ProductImageId : String,
    @SerializedName("ProductSKU") var ProductSKU : String,
    @SerializedName("Quantity") var Quantity : Int,
    @SerializedName("Assembly") var Assembly : String,
    @SerializedName("Dimentions") var Dimentions : String,
    @SerializedName("Weight") var Weight : String,
    @SerializedName("SeatingHight") var SeatingHight : String,
    @SerializedName("Colour") var Colour : String,
    @SerializedName("Price") var Price : Int,
    @SerializedName("ProductSubCategory") var ProductSubCategory : String
        )