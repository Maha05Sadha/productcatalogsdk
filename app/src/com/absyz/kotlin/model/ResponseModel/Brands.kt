package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class Brands (
    @SerializedName("Id") var Id : String,
    @SerializedName("Brand") var Brand : String,
    @SerializedName("BrandImage") var BrandImage : String
        )