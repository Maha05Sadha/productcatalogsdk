package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class RemoveCartItemResponse (
    @SerializedName("attributes") var attributes : RemoveCartAttributes,
    @SerializedName("Id") var Id : String,
    @SerializedName("Name") var Name : String,
    @SerializedName("Customer__c") var Customer_c : String,
    @SerializedName("Product__c") var Product_c : String
        )