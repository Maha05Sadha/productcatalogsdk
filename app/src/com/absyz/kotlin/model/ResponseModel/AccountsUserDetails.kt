package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class AccountsUserDetails (
    @SerializedName("Id") var Id : String,
    @SerializedName("CreatedDate") var CreatedDate : String,
    @SerializedName("Name") var Name : String,
    @SerializedName("Phone") var Phone : String,
    @SerializedName("Email") var Email : String,
    @SerializedName("BillingCity") var BillingCity : String,
    @SerializedName("Address") var Address : String,
    @SerializedName("BillingState") var BillingState : String,
    @SerializedName("Active") var Active : String,
    @SerializedName("WishlistCount") var WishlistCount : Int
        )