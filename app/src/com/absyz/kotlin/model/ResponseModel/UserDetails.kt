package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class UserDetails (
    @SerializedName("Id") var Id : String,
    @SerializedName("Name") var Name : String,
    @SerializedName("Email") var Email : String,
    @SerializedName("Phone") var Phone : String,
    @SerializedName("ProfileName") var ProfileName : String,
    @SerializedName("UserRoleName") var UserRoleName : String,
    @SerializedName("CompanyName") var CompanyName : String,
    @SerializedName("TimeZoneSidKey") var TimeZoneSidKey : String,
    @SerializedName("City") var City : String,
    @SerializedName("State") var State : String,
    @SerializedName("CreatedDate") var CreatedDate : String,
    @SerializedName("ManagerName") var ManagerName : String,
    @SerializedName("MobilePhone") var MobilePhone : String
        )