package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class BrandProductsResponse (
    @SerializedName("ProductDetails") var ProductDetails : List<ProductBrandDetails>,
    @SerializedName("current_page") var currentPage : String,
    @SerializedName("last_page") var lastPage : Int,
    @SerializedName("Per_page") var PerPage : Int,
    @SerializedName("Total") var Total : Int
        )