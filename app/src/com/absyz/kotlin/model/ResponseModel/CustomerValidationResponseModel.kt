package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

data class CustomerValidationResponseModel(
    @SerializedName("CustomerDetails") var CustomerDetails: List<CustomerValidationDetailsModel>
)
