package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class SalesTargetListDetails (
    @SerializedName("Id") var Id : String,
    @SerializedName("Name") var Name : String,
    @SerializedName("IsProductAvailable?") var IsProductAvailable : Boolean,
    @SerializedName("Brand") var Brand : String,
    @SerializedName("ProductType") var ProductType : String,
    @SerializedName("ProductDescription") var ProductDescription : String,
    @SerializedName("Category") var Category : String,
    @SerializedName("Material") var Material : String,
    @SerializedName("Warranty") var Warranty : String,
    @SerializedName("ProductImage") var ProductImage : String,
    @SerializedName("ProductSKU") var ProductSKU : String,
    @SerializedName("Quantity") var Quantity : Int,
    @SerializedName("Assembly") var Assembly : String,
    @SerializedName("Dimentions") var Dimentions : String,
    @SerializedName("Weight") var Weight : String,
    @SerializedName("SeatingHight") var SeatingHight : Double,
    @SerializedName("Colour") var Colour : String,
    @SerializedName("Price") var Price : Int,
    @SerializedName("Total") var Total : Int,
    @SerializedName("Remaining") var Remaining : Int,
    @SerializedName("ProductSubCategory") var ProductSubCategory : String,
    @SerializedName("ProductStock") var ProductStock : String

)