package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class ProductViewDetails (
    @SerializedName("Id") var Id : String,
    @SerializedName("Name") var Name : String,
    @SerializedName("IsProductAvailable?") var IsProductAvailable : Boolean,
    @SerializedName("Brand") var Brand : String,
    @SerializedName("ProductType") var ProductType : String,
    @SerializedName("ProductDescription") var ProductDescription : String,
    @SerializedName("ProductCategory") var ProductCategory : String,
    @SerializedName("ProductCategoryId") var ProductCategoryId : String,
    @SerializedName("Material") var Material : String,
    @SerializedName("Warranty") var Warranty : String,
    @SerializedName("ProductImage") var ProductImage : String,
    @SerializedName("ProductImage1") var ProductImage1 : String,
    @SerializedName("ProductImage2") var ProductImage2 : String,
    @SerializedName("ProductImage3") var ProductImage3 : String,
    @SerializedName("ProductImage4") var ProductImage4 : String,
    @SerializedName("ProductSKU") var ProductSKU : String,
    @SerializedName("Assembly") var Assembly : String,
    @SerializedName("Dimentions") var Dimentions : String,
    @SerializedName("Weight") var Weight : String,
    @SerializedName("SeatingHight") var SeatingHight : Double,
    @SerializedName("Price") var Price : Int,
    @SerializedName("Quantity") var Quantity : Int,
    @SerializedName("ProductSubCategory") var ProductSubCategory : String,
    @SerializedName("ProductVideo") var ProductVideo : String,
    @SerializedName("ColourCode") var ColourCode : String,
    @SerializedName("ColourName") var ColourName : String
        )