package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class StockValueDetails (
    @SerializedName("Quantity") var Quantity : Int

)