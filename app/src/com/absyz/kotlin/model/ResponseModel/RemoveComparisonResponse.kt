package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class RemoveComparisonResponse (
        @SerializedName("Message") var Message : String

        )