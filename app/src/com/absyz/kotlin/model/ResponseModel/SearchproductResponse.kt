package com.absyz.kotlin.model.ResponseModel

import com.absyz.kotlin.model.ResponseModel.SearchProductDetails
import com.google.gson.annotations.SerializedName

data class SearchproductResponse(
    @SerializedName("ProductDetails") var ProductDetails : List<SearchProductDetails>,
    @SerializedName("Total") var Total : Int
)