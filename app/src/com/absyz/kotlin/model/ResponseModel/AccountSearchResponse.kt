package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class AccountSearchResponse (
    @SerializedName("UserDetails") var UserDetails : List<AccountSearchUserDetails>,
    @SerializedName("Total") var Total : Int
        )