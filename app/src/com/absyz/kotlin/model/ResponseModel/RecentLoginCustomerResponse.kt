package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class RecentLoginCustomerResponse (
    @SerializedName("CustomerDetails") var CustomerDetails : List<RecentCustomerDetails>
        )