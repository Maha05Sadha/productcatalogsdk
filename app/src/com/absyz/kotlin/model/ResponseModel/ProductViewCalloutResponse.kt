package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class ProductViewCalloutResponse (
    @SerializedName("ProductDetails") var ProductDetails : List<ProductViewDetails>,
    @SerializedName("ProductColourCodes") var ProductColourCodes : List<ProductColorCodes>,
    @SerializedName("ProductStorageList") var ProductStorageList : List<ProductStorageList>
        )