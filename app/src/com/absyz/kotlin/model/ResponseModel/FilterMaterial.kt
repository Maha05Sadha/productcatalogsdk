package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class FilterMaterial (
    @SerializedName("Material") var Material : String
        )