package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class RecentCustomerDetails(
    @SerializedName("Id") var Id : String,
    @SerializedName("Name") var Name : String,
    @SerializedName("Address") var Address : String,
    @SerializedName("Email") var Email : String,
    @SerializedName("Phone") var Phone : String,
    @SerializedName("CartListCount") var CartListCount : Int,
    @SerializedName("CustomerIdentity") var CustomerIdentity : String
)