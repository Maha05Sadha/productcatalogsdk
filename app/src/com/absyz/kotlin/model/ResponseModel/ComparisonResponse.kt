package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class ComparisonResponse (
    @SerializedName("ProductComparisonDetails") var ProductComparisonDetails : List<ComparisonDetails>
        )