package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class CustomerRelatedWishlistResponse (
    @SerializedName("WishListDetails") var WishListDetails : List<RelatedWishlistDetails>,
    @SerializedName("WishlistCount") var WishlistCount : Int
)