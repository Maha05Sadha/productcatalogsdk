package com.absyz.kotlin.model.ResponseModel

import com.google.gson.annotations.SerializedName

class WishlistCreationAttribute (
    @SerializedName("type") var type : String,
    @SerializedName("url") var url : String
        )