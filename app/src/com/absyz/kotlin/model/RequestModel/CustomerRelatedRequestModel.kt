package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class CustomerRelatedRequestModel (
    @SerializedName("customerID") var customerID : String
)