package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class RemoveCartItemRequest (
    @SerializedName("cartId") var cartId : String

)