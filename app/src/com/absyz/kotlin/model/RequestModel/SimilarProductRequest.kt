package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class SimilarProductRequest (
    @SerializedName("productCategory") var productCategory : String
        )