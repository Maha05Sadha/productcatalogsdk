package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class IncreaseProductQtyRequest (
    @SerializedName("cartId") var cartId : String,
    @SerializedName("quantity") var quantity : String
        )