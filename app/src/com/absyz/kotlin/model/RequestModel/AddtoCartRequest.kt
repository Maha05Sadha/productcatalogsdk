package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class AddtoCartRequest (
    @SerializedName("customerId") var customerId : String,
    @SerializedName("productId") var productId : String,
    @SerializedName("colourCode") var colourCode : String,
    @SerializedName("colourName") var colourName : String,
    @SerializedName("location") var location : String,
    @SerializedName("productImage") var productImage : String,
    @SerializedName("prodQuantity") var prodQuantity : String,
    @SerializedName("productTotalQuantity") var productTotalQuantity : String
        )