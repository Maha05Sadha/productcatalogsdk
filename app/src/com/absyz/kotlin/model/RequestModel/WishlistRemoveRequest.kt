package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class WishlistRemoveRequest (
    @SerializedName("wishlistId") var wishlistId : String

        )