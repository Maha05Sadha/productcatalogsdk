package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class BrandProductsRequest (
    @SerializedName("productBrandId") var productBrandId : String

)