package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class CartProductRequest (
    @SerializedName("customerId") var customerId : String

)