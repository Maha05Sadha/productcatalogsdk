package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class VisitLogsRequestModel (
    @SerializedName("salespersonId") var salespersonId : String

)