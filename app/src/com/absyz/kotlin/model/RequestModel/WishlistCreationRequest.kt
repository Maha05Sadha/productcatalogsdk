package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class WishlistCreationRequest (
    @SerializedName("customerId") var customerId : String,
    @SerializedName("productId") var productId : String
        )