package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class AccountsUpdateRequestModel (
    @SerializedName("customerId") var customerId : String,
    @SerializedName("customerName") var customerName : String,
    @SerializedName("mobileNumber") var mobileNumber : String,
    @SerializedName("customerEmail") var customerEmail : String,
    @SerializedName("customerAddress") var customerAddress : String

)