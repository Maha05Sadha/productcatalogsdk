package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class FilterProductListRequest (
    @SerializedName("productCategoryId") var productCategoryId : String,
    @SerializedName("productSubCategory") var productSubCategory : String,
    @SerializedName("material") var material : String,
    @SerializedName("colourCode") var colourCode : String,
    @SerializedName("instock") var instock : String,
    @SerializedName("minPrice") var minPrice : String,
    @SerializedName("maxPrice") var maxPrice : String,
    @SerializedName("minHight") var minHight : String,
    @SerializedName("maxHight") var maxHight : String,
    @SerializedName("minWidth") var minWidth : String,
    @SerializedName("maxWidth") var maxWidth : String,
    @SerializedName("minDepth") var minDepth : String,
    @SerializedName("maxDepth") var maxDepth : String
        )