package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class CustomerCheckInRequest (
    @SerializedName("mobileNumber") var mobileNumber : String

        )