package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class RemoveWishlistRequest (
    @SerializedName("wishlistId") var wishlistId : String
)