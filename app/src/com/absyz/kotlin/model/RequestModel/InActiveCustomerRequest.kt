package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class InActiveCustomerRequest (
    @SerializedName("customerId") var customerId : String

)