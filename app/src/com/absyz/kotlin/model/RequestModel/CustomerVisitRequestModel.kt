package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class CustomerVisitRequestModel (
    @SerializedName("salespersonId") var salespersonId : String

)