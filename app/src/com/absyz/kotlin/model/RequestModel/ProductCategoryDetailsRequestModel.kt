package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class ProductCategoryDetailsRequestModel (
    @SerializedName("productCategoryId") var productCategoryId : String

)