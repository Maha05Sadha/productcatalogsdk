package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class ProductColorViewRequest (
    @SerializedName("productId") var productId : String,
    @SerializedName("colourCode") var colourCode : String,
    @SerializedName("location") var location : String
)