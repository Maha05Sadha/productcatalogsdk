package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class CurrentVisitListRequest (
    @SerializedName("salespersonId") var salespersonId : String
        )