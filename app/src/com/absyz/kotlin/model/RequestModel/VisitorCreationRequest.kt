package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class VisitorCreationRequest (
    @SerializedName("salespersonId") var salespersonId : String
)