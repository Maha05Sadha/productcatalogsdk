package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class CustomerRegistrationRequest (
    @SerializedName("mobileNumber") var mobileNumber : String,
    @SerializedName("name") var name : String,
    @SerializedName("customerEmail") var customerEmail : String,
    @SerializedName("customerAddress") var customerAddress : String,
    @SerializedName("salespersonId") var salespersonId : String
)