package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class SalesTargetRequestModel(
    @SerializedName("salespersonId") var salespersonId : String
)