package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class AddtoCompareRequest (
    @SerializedName("customerId") var customerId : String,
    @SerializedName("productId") var productId : String,
    @SerializedName("productCategoryId") var productCategoryId : String,
    @SerializedName("colourCode") var colourCode : String,
    @SerializedName("colourName") var colourName : String,
    @SerializedName("location") var location : String,
    @SerializedName("productImage") var productImage : String
        )