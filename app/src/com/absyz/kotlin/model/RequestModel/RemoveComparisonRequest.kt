package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class RemoveComparisonRequest (
    @SerializedName("customerId") var customerId : String

)