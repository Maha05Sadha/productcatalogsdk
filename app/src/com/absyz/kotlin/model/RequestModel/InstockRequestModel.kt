package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class InstockRequestModel (
    @SerializedName("inStockValue") var inStockValue : String,
    @SerializedName("productCategoryId") var productCategoryId : String
        )