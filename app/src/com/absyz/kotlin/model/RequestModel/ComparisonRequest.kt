package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class ComparisonRequest (
    @SerializedName("customerId") var customerId : String
        )