package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

data class CustomerValidationRequestModel (
    @SerializedName("mobileNumber") var mobileNumber : String
)