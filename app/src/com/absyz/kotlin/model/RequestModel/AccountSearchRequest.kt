package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

class AccountSearchRequest (
    @SerializedName("customerSearch") var customerSearch : String
)