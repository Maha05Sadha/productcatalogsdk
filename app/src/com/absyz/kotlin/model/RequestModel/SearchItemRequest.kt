package com.absyz.kotlin.model.RequestModel

import com.google.gson.annotations.SerializedName

data class SearchItemRequest(

    @SerializedName("productSearch") var productSearch : String

)


