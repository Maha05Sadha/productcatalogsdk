package com.absyz.kotlin.activities.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.absyz.kotlin.R
import com.absyz.kotlin.adapters.AccountsSearchAdapter
import com.absyz.kotlin.adapters.ComparisonAdapter
import com.absyz.kotlin.adapters.SimilarProductAdapter
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.ComparisonActivityBinding
import com.absyz.kotlin.fragments.BaseFragment
import com.absyz.kotlin.model.RequestModel.*
import com.absyz.kotlin.model.ResponseModel.*
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel

class ComparisonActivity: BaseActivity(), SimilarProductAdapter.ItemClickListener,ComparisonAdapter.ItemClickListener   {

    lateinit var binding: ComparisonActivityBinding

    lateinit var homeViewModel: HomeViewModel

    private var compareProductdetails: ArrayList<ComparisonDetails> = ArrayList()
    var compareprodReq: ComparisonRequest?= null

    private var similarProductdetails: ArrayList<SimilarProductDetails> = ArrayList()
    var similarProdReq: SimilarProductRequest?= null

    var productcategory_id : String = ""
    var cart_count : Int = 0
    var comp_Message : String = ""

    var addtocartRequest: AddtoCartRequest?= null
    var addtocompareRequest: AddtoCompareRequest?= null
    var removesingleReq : RemoveSingleComparisonRequest?= null

    private lateinit var comparisonAdapter: ComparisonAdapter

    private var customerDetails: ArrayList<RecentCustomerDetails> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.comparison_activity)

        (application as ApplicationController).applicationComponent.inject(this)
        //View model instance
        homeViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        productcategory_id = intent.getStringExtra("proc_cat_id").toString()

        val mLayoutManager = LinearLayoutManager(this)
        mLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        binding.comparisonRv.layoutManager = mLayoutManager

        val sLayoutManager = LinearLayoutManager(this)
        sLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        binding.similarRv.layoutManager = sLayoutManager

        //Recent Customer Observer
        homeViewModel.getRecentCustomerResponse()?.observe(this, { this.ConsumeRecentCustomerData(it as ApiResponse<*>) })
        //Call Recent Customer APi
        recentCustomerapiCall()

        //Observer
        homeViewModel!!.getCompareDetailsResponse()?.observe(this, { this.ConsumeComparisonProductData(it as ApiResponse<*>) })

        //Observer
        homeViewModel!!.getSimilarProductDetailsResponse()?.observe(this, { this.ConsumeSimilarProductData(it as ApiResponse<*>) })

        /*Add to cart api*/
        homeViewModel!!.getAddtoCartResponse()?.observe(this, { this.consumeAddtoCartResponse(it as ApiResponse<*>) })

        //Remove Comparison Observer
        homeViewModel!!.getRemoveSingleComparisonProductDetailsResponse()?.observe(this, { this.ConsumeRemoveSingleComparisonProductData(it as ApiResponse<*>) })


        compareprodReq = ComparisonRequest(
            customerId = AppConstants.CustomerID
        )
        apiCall(compareprodReq!!)

        /*Add to compare api*/
        homeViewModel!!.getAddtoCompareResponse()?.observe(this, { this.consumeAddtoCompareResponse(it as ApiResponse<*>) })

        similarProdReq = SimilarProductRequest(
            productCategory = productcategory_id
        )
        apiCallSimilar(similarProdReq!!)

        binding.cartIcon.setOnClickListener{
            val intent_detail = Intent(this@ComparisonActivity, CartActivity::class.java)
            startActivity(intent_detail)
            finish()
        }

        binding.burgerMenu.setOnClickListener() {
            onBackPressed()
            finish()
        }
    }

    private fun apiCall(compareprodReq: ComparisonRequest) {

        try {
            homeViewModel.CompareProductDetails(
                compareprodReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeComparisonProductData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeCartProductData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is ComparisonResponse) {
                                compareProductdetails.clear()
                                compareProductdetails.addAll(it.ProductComparisonDetails)
                                AppConstants.compareItemCount = compareProductdetails.size
                                binding.compareCount.text = AppConstants.compareItemCount.toString()
                                updateCompareProductDetails()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateCompareProductDetails() {
        try {
            comparisonAdapter = ComparisonAdapter(this,utility!!,this){index -> deleteItem(index)}
            binding.comparisonRv.adapter = comparisonAdapter
            comparisonAdapter?.updateList(compareProductdetails)
            comparisonAdapter.setOnItemCLickListerner(object : ComparisonAdapter.onItemClickListener{
                override fun onItemCLick(position: Int) {
                    //Toast.makeText(this@ComparisonActivity, compareProductdetails[position].ProductName, Toast.LENGTH_SHORT).show()
                }
            } )


        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

//    fun addToCart(index: Int){
//        //cart_id = cartProductdetails[index].Id
//
//        addtocartRequest = AddtoCartRequest(
//            customerId = AppConstants.CustomerID,
//            productId = compareProductdetails[index].ProductId,
//            colourCode = compareProductdetails[index].Colour,
//            colourName = compareProductdetails[index].ColourName,
//            location = compareProductdetails[index].ProductLocation,
//            productImage = compareProductdetails[index].ProductImageId,
//            prodQuantity = "1",
//            productTotalQuantity = compareProductdetails[index].Quantity.toString()
//        )
//        callAddtoCartapi(addtocartRequest!!)
//    }

    private fun callAddtoCartapi(addtocartReq: AddtoCartRequest) {

        try {
            homeViewModel.AddtoCartData(
                addtocartReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeAddtoCartResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeAddtoCartResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is AddtoCartResponse) {
                                //addtocartAttribute.addAll(listOf(it.attributes))
                                //cartproductId = it.Id

                                comp_Message = it.Message
                                if(comp_Message.equals("The product is out of stock.")){
                                    comparisonAdapter.addtocart("no")
                                    Toast.makeText(this,"This product is out of stock!",Toast.LENGTH_SHORT).show()
                                } else {
                                    //cartProductIDList.add(cartproductId)
                                    comparisonAdapter.addtocart("yes")
                                    var cartval : Int = 0
                                    cartval = AppConstants.cartItemCount + 1
                                    //Toast.makeText(this,"The product is now added to the cart.",Toast.LENGTH_SHORT).show()
                                    binding.cartCount.text = cartval.toString()
                                    //binding.addtocart.visibility = View.GONE
                                    //binding.addedcartlayout.visibility = View.VISIBLE
                                    Toast.makeText(this,"Product successfully added to cart!",Toast.LENGTH_SHORT).show()

                                }
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun apiCallSimilar(similarprodReq: SimilarProductRequest) {

        try {
            homeViewModel.SimilarProductDetails(
                similarprodReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeSimilarProductData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeSimilarProductData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is SimilarProductResponse) {
                                similarProductdetails.clear()
                                similarProductdetails.addAll(it.ProductCartDetails!!)
                                updateSimilarProductDetails()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateSimilarProductDetails() {
        try {
            val similarProductAdapter = SimilarProductAdapter(this,utility!!,this)
            binding.similarRv.adapter = similarProductAdapter
            similarProductAdapter?.updateList(similarProductdetails)
            similarProductAdapter.setOnItemCLickListerner(object : SimilarProductAdapter.onItemClickListener{
                override fun onItemCLick(position: Int) {
                    //Toast.makeText(this@ComparisonActivity, similarProductdetails[position].ProductName, Toast.LENGTH_SHORT).show()
                    val intent_detail = Intent(this@ComparisonActivity, ProductDetailActivity::class.java)
                    intent_detail.putExtra("id",similarProductdetails[position].ProductId)
                    intent_detail.putExtra("cat_id",similarProductdetails[position].ProductCategoryId)
                    intent_detail.putExtra("from_activity_name","similar_products")

                    startActivity(intent_detail)
                }
            } )


        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

//    fun addToCompare(index: Int){
//        //cart_id = cartProductdetails[index].Id
//
//        addtocompareRequest = AddtoCompareRequest(
//            customerId = AppConstants.CustomerID,
//            productId = similarProductdetails[index].ProductId,
//            productCategoryId = productcategory_id,
//            colourCode = similarProductdetails[index].Colour,
//            colourName = similarProductdetails[index].ColourName,
//            location = similarProductdetails[index].ProductLocation,
//            productImage = similarProductdetails[index].ProductImageId
//
//        )
//        callAddtoCompareapi(addtocompareRequest!!)
//    }

    private fun callAddtoCompareapi(addtocompareReq: AddtoCompareRequest) {

        try {
            homeViewModel.AddtoComapreData(
                addtocompareReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeAddtoCompareResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeAddtoCompareResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is AddtoCompareResponse) {
//                                messageList.add(it.message)
                                comp_Message = it.Message
                                Toast.makeText(this,comp_Message,Toast.LENGTH_SHORT).show()
                                apiCall(compareprodReq!!)
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun recentCustomerapiCall() {

        try {
            homeViewModel.recentCustomerDetails(
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeRecentCustomerData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeRecentCustomerData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is RecentLoginCustomerResponse) {
                                customerDetails.clear()
                                customerDetails.addAll(it.CustomerDetails!!)
                                updateRecentCustomerDetails()
                                //Toast.makeText(this, recentCustomerdetails?.get(0)?.Name.toString(),Toast.LENGTH_SHORT).show()

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,apiResponse.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateRecentCustomerDetails() {
        try {
            val recentCustomerModel = customerDetails[0]
            //recent_customer_name = recentCustomerdetails?.get(0)?.Name.toString()
            AppConstants.Customer_Name = recentCustomerModel!!.Name
            AppConstants.CustomerID = recentCustomerModel!!.Id
            AppConstants.cartItemCount = recentCustomerModel!!.CartListCount
            //Toast.makeText(this,AppConstants.Customer_Name,Toast.LENGTH_SHORT).show()
            cart_count = recentCustomerModel!!.CartListCount
            binding.cartCount.text = cart_count.toString()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun removeapiCall(removesingleprodReq: RemoveSingleComparisonRequest) {

        try {
            homeViewModel.RemoveSingleComparisonProductDetails(
                removesingleprodReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeRemoveSingleComparisonProductData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeRemoveSingleComparisonProductData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is RemoveSingleComparisonResponse) {
                                compareprodReq = ComparisonRequest(
                                    customerId = AppConstants.CustomerID
                                )
                                apiCall(compareprodReq!!)
                                //Toast.makeText(this,"Successfully Removed from Cart!", Toast.LENGTH_SHORT).show()

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    fun deleteItem(index: Int){
        var comparison_id = compareProductdetails[index].Id

        removesingleReq = RemoveSingleComparisonRequest(
            comparisonId = comparison_id
        )
        removeapiCall(removesingleReq!!)
        compareProductdetails.removeAt(index)
        comparisonAdapter.setItems(compareProductdetails)
        AppConstants.compareItemCount = compareProductdetails.size
        binding.compareCount.text = AppConstants.compareItemCount.toString()

    }

    override fun onItemClick(item: SimilarProductDetails?) {
        addtocompareRequest = AddtoCompareRequest(
            customerId = AppConstants.CustomerID,
            productId = item!!.ProductId,
            productCategoryId = item!!.ProductCategoryId,
            colourCode = item!!.Colour,
            colourName = item!!.ColourName,
            location = item!!.ProductLocation,
            productImage = item!!.ProductImageId

        )
        callAddtoCompareapi(addtocompareRequest!!)
    }

    override fun onItemClick(item: ComparisonDetails) {
        addtocartRequest = AddtoCartRequest(
            customerId = AppConstants.CustomerID,
            productId = item.ProductId,
            colourCode = item.Colour,
            colourName = item.ColourName,
            location = item.ProductLocation,
            productImage = item.ProductImageId,
            prodQuantity = "1",
            productTotalQuantity = item.Quantity.toString()
        )
        callAddtoCartapi(addtocartRequest!!)
    }

}