package com.absyz.kotlin.activities.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.absyz.kotlin.R
import com.absyz.kotlin.Retrofit.IOnBackPressed
import com.absyz.kotlin.adapters.CurrentVisitAdapter
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.LandingpageActivityBinding
import com.absyz.kotlin.fragments.*
import com.absyz.kotlin.model.RequestModel.*
import com.absyz.kotlin.model.ResponseModel.*
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel
import com.google.android.material.navigation.NavigationView
import com.monsterbrain.recyclerviewtableview.CartAdapter
import kotlinx.android.synthetic.main.navi_drawer_header.view.*


class LandingpageActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var binding: LandingpageActivityBinding

    lateinit var homeViewModel: HomeViewModel

    private var userDetails: ArrayList<UserDetails> = ArrayList()

    private var cartProductdetails: ArrayList<ProductCartDetails> = ArrayList()
    var cartprodReq: CartProductRequest?= null

    private var customerListdetails: ArrayList<CustomerListDetails> = ArrayList()
    private var visitorListdetails : ArrayList<VisiterListDetails> = ArrayList()
    lateinit var CustomerVisitorList : HashMap<String,String>


    var currentdetailsReq: CurrentVisitListRequest?= null
    var removeReq : InActiveCustomerRequest?= null

    private lateinit var currentVisitAdapter: CurrentVisitAdapter

    var message : String = ""
    var customer_count : Int = 0
    var cart_count : Int = 0

    private var customerDetails: ArrayList<RecentCustomerDetails> = ArrayList()
    private var customerCheckinReq : CustomerCheckInRequest ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.landingpage_activity)

        //Navigation Drawer items
        val toolbar : androidx.appcompat.widget.Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        (application as ApplicationController).applicationComponent.inject(this)

        binding.currentvisitRv.layoutManager = LinearLayoutManager(this)
        //binding.visitRv.layoutManager = LinearLayoutManager(this)

        val toggle = ActionBarDrawerToggle(this,binding.drawerlayout,toolbar,R.string.open,R.string.close)
        toggle.isDrawerIndicatorEnabled = true
        binding.drawerlayout.addDrawerListener(toggle)
        toggle.syncState()
        binding.naviDrawerItems.setNavigationItemSelectedListener ( this )

        //Logout
        val logout : View = findViewById(R.id.logoutview)
        logout.setOnClickListener{
            OnClicklogout()
        }

        //View model instance
        homeViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        //Recent Customer Observer
        homeViewModel.getRecentCustomerResponse()?.observe(this, { this.ConsumeRecentCustomerData(it as ApiResponse<*>) })
        //Call Recent Customer APi
        recentCustomerapiCall()

        //Observer
        homeViewModel.getuserdetailsResponse()
            ?.observe(this, { this.ConsumeUserDetailsData(it as ApiResponse<*>) })

        //Observer
        homeViewModel.getCurrentVisitResponse()
            ?.observe(this, { this.ConsumeCurrentVisitListData(it as ApiResponse<*>) })

        //Api CAll
        homeViewModel.UserDetails(this,
            "Bearer  " + AppConstants.ACCESS_TOKEN)

        //InActive Observer
        homeViewModel!!.getInActiveCustomerResponse()?.observe(this, { this.consumeInActiveCustomerResponse(it as ApiResponse<*>) })

        //Customer CheckIn
        homeViewModel.getcustomerCheckInResponse()
            ?.observe(this, { this.ConsumeCustomerCheckInData(it as ApiResponse<*>) })


        /*Toolbar Contents start*/
        /*Current Visits*/
        binding.currentCardview.visibility = View.GONE
        binding.createvisitView.setOnClickListener {
//            val intent = Intent(this, CreateVisit::class.java)
//            startActivity(intent)
            binding.currentCardview.visibility = View.VISIBLE

            binding.cancelBtn.setOnClickListener {
                binding.currentCardview.visibility = View.GONE
            }

            binding.newvisitBtn.setOnClickListener(){
                val intent = Intent(this, CreateVisit::class.java)
                startActivity(intent)
                binding.currentCardview.visibility = View.GONE
                finish()
            }

        }

        //Default Fragment
        ChangeFragment(LandingScreenFragment())

        //Observer
        homeViewModel!!.getCartProductDetailsResponse()?.observe(this, { this.ConsumeCartProductData(it as ApiResponse<*>) })

        cartprodReq = CartProductRequest(
            customerId = AppConstants.CustomerID
        )
        apiCall(cartprodReq!!)

        binding.cartIcon.setOnClickListener{
            val intent_detail = Intent(this@LandingpageActivity, CartActivity::class.java)
            startActivity(intent_detail)
            finish()
        }

        binding.searchText.setOnClickListener(){
            //Toast.makeText(this,"Currently not Available!",Toast.LENGTH_SHORT).show()

            val intent = Intent(this@LandingpageActivity, SearchProductActivity::class.java)
            intent.putExtra("fromactivity","products")
            startActivity(intent)
            finish()
        }

        binding.searchView.setOnClickListener(){
            //Toast.makeText(this,"Currently not Available!",Toast.LENGTH_SHORT).show()

            val intent = Intent(this@LandingpageActivity, SearchProductActivity::class.java)
            intent.putExtra("fromactivity","products")
            startActivity(intent)
            finish()

        }

        binding.searchIcon.setOnClickListener(){
            //Toast.makeText(this,"Currently not Available!",Toast.LENGTH_SHORT).show()

            val intent = Intent(this@LandingpageActivity, SearchProductActivity::class.java)
            intent.putExtra("fromactivity","products")
            startActivity(intent)
            finish()

        }
    }

    fun ConsumeUserDetailsData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeUserDetailsData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is UserDetailResponseModel) {
                                userDetails.clear()
                                userDetails.addAll(it.UserDetails!!)
                                updateUserDetails()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //1Toast.makeText(this,apiResponse.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateUserDetails() {
        try {
            val validation_model = userDetails[0]
            Log.d("userDetails->",validation_model.toString())
            AppConstants.SalesPersonID = validation_model.Id
            Log.d("SalesPersonID->",AppConstants.SalesPersonID)
            currentdetailsReq = CurrentVisitListRequest(
                salespersonId = validation_model.Id
            )
            currentvisitapiCall(currentdetailsReq!!)


        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.orders -> {
                setToolbarTitle("Orders")
                ChangeFragment(OrderFragment())
                binding.drawerlayout.closeDrawers()
            }
            R.id.accounts -> {
                setToolbarTitle("Accounts")
                ChangeFragment(AccountFragment())
                binding.drawerlayout.closeDrawers()
            }
            R.id.sales_target -> {
                setToolbarTitle("My Sales Target")
                ChangeFragment(SalesTargetFragment())
                binding.drawerlayout.closeDrawers()

            }
            R.id.visit_logs -> {
                setToolbarTitle("Visit Logs")
                ChangeFragment(ViewLogsFragment())
                binding.drawerlayout.closeDrawers()

            }
            R.id.settings -> {
                setToolbarTitle("Settings")
                ChangeFragment(SettingsFragment())
                binding.drawerlayout.closeDrawers()
            }

            R.id.states -> {
                setToolbarTitle("States")
                ChangeFragment(PieChartFragment())
                binding.drawerlayout.closeDrawers()
            }
        }
        return true
    }

    fun setToolbarTitle(title: String){
        supportActionBar?.title = title
    }

    fun ChangeFragment(frag: Fragment){
        val fragment = supportFragmentManager.beginTransaction()
        fragment.replace(R.id.fragment_container,frag).commit()
    }

    fun OnClicklogout(){
        //Logout from this application
        Toast.makeText(applicationContext,"Clicked on Logout",Toast.LENGTH_SHORT).show()
    }

    private fun apiCall(cartprodReq: CartProductRequest) {

        try {
            homeViewModel.CartProductDetails(
                cartprodReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeCartProductData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeCartProductData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is CartProductResponse) {
                                cartProductdetails.clear()
                                cartProductdetails.addAll(it.ProductCartDetails!!)
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //2Toast.makeText(this,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun currentvisitapiCall(currentdetailsReq: CurrentVisitListRequest) {

        try {
            homeViewModel.SendCurrentVisitListData(
                currentdetailsReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeCurrentVisitListData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeCurrentVisitListData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is CurrentVistListResponse) {
                                customerListdetails.clear()
                                customerListdetails.addAll(it.CustomerList!!)
                                visitorListdetails.addAll(it.VisitorList)
                                customer_count = customerListdetails.size
                                //customer_count = customerListdetails.size + visitorListdetails.size
                                binding.visitCount.text = customer_count.toString()
                                updateCurrentListDetails()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //3Toast.makeText(this,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateCurrentListDetails() {
        try {
            for(i in customerListdetails.indices) {
                //CustomerVisitorList.put(customerListdetails[i].Id,customerListdetails[i].Name)
            }
            for(j in visitorListdetails.indices) {
                //CustomerVisitorList.put(visitorListdetails[j].Id,visitorListdetails[j].Name)
            }
            //CurrentCustomer
            currentVisitAdapter = CurrentVisitAdapter(this,utility!!){index -> inActivateItems(index)}
            binding.currentvisitRv.adapter = currentVisitAdapter

            //currentVisitAdapter?.updateList(CustomerVisitorList)
            currentVisitAdapter?.updateList(customerListdetails)

            currentVisitAdapter.setOnItemCLickListerner(object : CurrentVisitAdapter.onItemClickListener{
                override fun onItemCLick(position: Int) {
                    //Toast.makeText(this@LandingpageActivity, customerListdetails[position].Name, Toast.LENGTH_SHORT).show()
                    customerCheckinReq = CustomerCheckInRequest(
                        mobileNumber = customerListdetails[position].Phone
                    )
                    checkinapiCall(customerCheckinReq!!)
                }
            } )
            //CurrentVisitors
//            val currentVisitoersadapter = CurrentVisitersAdapter(this,utility!!)
//            binding.visitRv.adapter = currentVisitoersadapter
//            //currentVisitAdapter?.updateList(CustomerVisitorList)
//            currentVisitoersadapter?.updateList(visitorListdetails)
//            currentVisitoersadapter.setOnItemCLickListerner(object : CurrentVisitersAdapter.onItemClickListener{
//                override fun onItemCLick(position: Int) {
//                    Toast.makeText(this@LandingpageActivity, visitorListdetails[position].Name, Toast.LENGTH_SHORT).show()
//                }
//            } )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun inActivateItems(index: Int){
        //customer_id = visitorDetails[index].Id

        removeReq = InActiveCustomerRequest(
            customerId = customerListdetails[index].Id
        )
        inactiveapiCall(removeReq!!)
        //visitorDetails.removeAt(index)
        currentVisitAdapter.setItems(customerListdetails)

    }

    private fun inactiveapiCall(removeReq: InActiveCustomerRequest) {

        try {
            homeViewModel.InactiveCustomerData(
                removeReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }


    private fun consumeInActiveCustomerResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeInActiveCustomerResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is InActiveCustomerResponse) {
                                message = it.Message
                                Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
                                currentdetailsReq = CurrentVisitListRequest(
                                    salespersonId = AppConstants.SalesPersonID
                                )
                                currentvisitapiCall(currentdetailsReq!!)
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,"Customer De-activated",Toast.LENGTH_SHORT).show()
                    currentdetailsReq = CurrentVisitListRequest(
                        salespersonId = AppConstants.SalesPersonID
                    )
                    currentvisitapiCall(currentdetailsReq!!)
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun recentCustomerapiCall() {

        try {
            homeViewModel.recentCustomerDetails(
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeRecentCustomerData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeRecentCustomerData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is RecentLoginCustomerResponse) {
                                customerDetails.clear()
                                customerDetails.addAll(it.CustomerDetails!!)
                                updateRecentCustomerDetails()
                                //Toast.makeText(this, recentCustomerdetails?.get(0)?.Name.toString(),Toast.LENGTH_SHORT).show()

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //4Toast.makeText(this,apiResponse.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateRecentCustomerDetails() {
        try {
            val recentCustomerModel = customerDetails[0]
            //recent_customer_name = recentCustomerdetails?.get(0)?.Name.toString()
            AppConstants.Customer_Name = recentCustomerModel!!.Name
            AppConstants.CustomerID = recentCustomerModel!!.Id
            AppConstants.cartItemCount = recentCustomerModel!!.CartListCount
            //Toast.makeText(this,AppConstants.Customer_Name,Toast.LENGTH_SHORT).show()

            cart_count = recentCustomerModel!!.CartListCount
            binding.cartCount.text = cart_count.toString()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun checkinapiCall(customercheckInReq: CustomerCheckInRequest) {

        try {
            homeViewModel.SendcustomerCheckInData(
                customercheckInReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeCustomerCheckInData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeCustomerCheckInData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is CustomerCheckInResponse) {
                                val customer_name = it.Name
                                currentdetailsReq = CurrentVisitListRequest(
                                    salespersonId = AppConstants.SalesPersonID
                                )
                                currentvisitapiCall(currentdetailsReq!!)
                                //Toast.makeText(this,"Checked In Successfully!", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //5Toast.makeText(this,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    override fun onBackPressed() {
        try {
            if (supportFragmentManager.backStackEntryCount > 1) {
                supportFragmentManager.popBackStack()
            } else {
                super.onBackPressed()
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    override fun onResume() {
        super.onResume()
        recentCustomerapiCall()
    }

}