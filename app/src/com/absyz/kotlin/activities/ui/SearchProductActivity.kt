package com.absyz.kotlin.activities.ui

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.absyz.kotlin.R
import com.absyz.kotlin.adapters.AccountsSearchAdapter
import com.absyz.kotlin.adapters.SearchAdapter
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.SearchproductActivityBinding
import com.absyz.kotlin.model.RequestModel.AccountSearchRequest
import com.absyz.kotlin.model.RequestModel.SearchItemRequest
import com.absyz.kotlin.model.ResponseModel.*
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel
import org.apache.commons.lang3.StringUtils

class SearchProductActivity : BaseActivity(), SearchAdapter.ItemClickListener,AccountsSearchAdapter.ItemClickListener, TextWatcher {

    lateinit var binding: SearchproductActivityBinding
     var searchItemRequest: SearchItemRequest?= null
    var accountItemreq : AccountSearchRequest?= null

    lateinit var homeViewModel: HomeViewModel
    lateinit var searchAdapter: SearchAdapter
    lateinit var accountsearchAdapter : AccountsSearchAdapter

    var list: MutableList<SearchProductDetails> = ArrayList()
    var accountlist: MutableList<AccountSearchUserDetails> = ArrayList()

    var fromactivity : String = ""


    companion object {

        const val TAG = "SearchActivity"

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            binding = DataBindingUtil.setContentView(this, R.layout.searchproduct_activity)
            (application as ApplicationController).applicationComponent.inject(this)
            //View model instance
            homeViewModel =
                ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

            fromactivity = intent.getStringExtra("fromactivity").toString()

            if(fromactivity.equals("accounts")){
                binding.searchLayout.searchText.visibility = View.GONE
                binding.searchLayout.searchAccText.visibility = View.VISIBLE
            } else if(fromactivity.equals("products")){
                binding.searchLayout.searchText.visibility = View.VISIBLE
                binding.searchLayout.searchAccText.visibility = View.GONE
            }

            binding.searchLayout.searchText.addTextChangedListener(this)
            binding.searchLayout.searchAccText.addTextChangedListener(this)

            setAdapter()
            if (binding.searchLayout.searchText.requestFocus()) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            if (binding.searchLayout.searchAccText.requestFocus()) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }

            homeViewModel!!.searchproductresponse()
                ?.observe(this, { this.consumeResponse(it as ApiResponse<*>) })

            homeViewModel!!.searchAccountresponse()
                ?.observe(this, { this.consumeAccountSearchResponse(it as ApiResponse<*>) })

            Log.e(TAG, "onCreate()")

            binding.searchLayout.burgerMenu.setOnClickListener() {
                onBackPressed()
                finish()
            }

//
//            binding.tvNoRecords.visibility = View.VISIBLE
//            binding.tvNoRecords.text = getString(R.string.searchWith)
//            binding.searchLayout.search.hint = getString(R.string.typeNameDob)

//            binding.searchLayout.cancelImageV.setOnClickListener {
//                binding.searchLayout.search.setText("")
//                //onClearSearch()
//            }


//            binding.searchLayout.search.isFocusableInTouchMode = true;
//            binding.searchLayout.search.requestFocus();



        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun searchapiCall(searchItemRequest: SearchItemRequest) {
        try {
            homeViewModel.SearchproductAPI(
                this,
                searchItemRequest,
                "Bearer " + AppConstants.ACCESS_TOKEN
            )
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun searchaccountapiCall(searchAccountRequest: AccountSearchRequest) {
        try {
            homeViewModel.SearchAccountAPI(
                this,
                searchAccountRequest,
                "Bearer " + AppConstants.ACCESS_TOKEN
            )
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun setAdapter() {
        try {
            searchAdapter = SearchAdapter(this, this, utility!!)
//            val layoutManager = GridLayoutManager(this, 3)
//            binding.recyclerView.setLayoutManager(layoutManager);
//            binding.recyclerView.setHasFixedSize(true)
//            binding.recyclerView.setAdapter(searchAdapter);
            utility!!.getRecyclerview(
                this,
                binding.recyclerView,
                false,
                isDivider = false
            )!!.adapter = searchAdapter
            searchAdapter.updateList(list)

            //Accounts Adapter
            accountsearchAdapter = AccountsSearchAdapter(this, this, utility!!)
            utility!!.getRecyclerview(
                this,
                binding.accountsrecyclerView,
                false,
                isDivider = false
            )!!.adapter = accountsearchAdapter
            accountsearchAdapter.updateList(accountlist)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }


    private fun consumeResponse(response: ApiResponse<*>) {
//        Log.e(SearchProductActivity.TAG, "consumeResponse()")
//        Log.e(s.TAG, "consumeResponse()")
        try {

            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    binding.tvNoRecords.text = getString(R.string.nodata)
//                    list.clear()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is SearchproductResponse) {
                                list.clear()
                                list.addAll(it.ProductDetails!!)
                                updateFirstList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        if (StringUtils.contains(
                                response.error!!.message,
                                "kotlin.jvm.internal.Intrinsics.checkNotNullParameter"
                            )
                        ) {
                            //binding.tvNoRecords.visibility = View.VISIBLE
                            binding.tvNoRecords.text = getString(R.string.nodata)
                            list.clear()
                            updateFirstList()
                        } else {
//                            binding.tvNoRecords.visibility = View.VISIBLE
//                            binding.recyclerView.visibility = View.GONE

//                            val apiError =
//                                errorUtils!!.parseError((response.error as HttpException).response())
                            utility!!.showToast(this, "error_description")
                        }
                        // val code = (response.error as HttpException).code()
                        // Log.e(TAG, "consumeResponse() Exception code - $code")

                    } catch (ex: Exception) {
                        ex.printStackTrace()
//                        Log.e(Sea.TAG, "consumeResponse() Exception0 - ${ex.message}")
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
//            Log.e(s.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun updateFirstList() {
        try {
            if(list.isEmpty()){
                binding.tvNoRecords.visibility = View.VISIBLE
                binding.recyclerView.visibility = View.GONE
                binding.accountsrecyclerView.visibility = View.GONE

            } else if(list.isNotEmpty()){
                binding.tvNoRecords.visibility = View.GONE
                binding.recyclerView.visibility = View.VISIBLE
                binding.accountsrecyclerView.visibility = View.GONE

                searchAdapter.updateList(list)
            }

//            if(list.size.equals(0)){
//                binding.tvNoRecords.visibility = View.VISIBLE
//                binding.recyclerView.visibility = View.GONE
//            } else{
//                searchAdapter.updateList(list)
//            }
//            setListVisibility(list.isNotEmpty())
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    override fun onItemClick(item: SearchProductDetails?) {

        val intent_detail = Intent(this@SearchProductActivity, ProductDetailActivity::class.java)
        //                   intent_detail.putExtra("name",productCategoryDetails[position].Name)
        intent_detail.putExtra("id", item!!.Id)
        intent_detail.putExtra("cat_id",item!!.ProductCategory)
        intent_detail.putExtra("from_activity_name","search_productView")

        startActivity(intent_detail)

    }

    private fun consumeAccountSearchResponse(response: ApiResponse<*>) {
//        Log.e(s.TAG, "consumeAccountSearchResponse()")
        try {

            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    binding.tvNoRecords.text = getString(R.string.nodata)
//                    list.clear()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is AccountSearchResponse) {
                                accountlist.clear()
                                accountlist.addAll(it.UserDetails!!)
                                updateAccountList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    try {
                        if (StringUtils.contains(
                                response.error!!.message,
                                "kotlin.jvm.internal.Intrinsics.checkNotNullParameter"
                            )
                        ) {
                            //binding.tvNoRecords.visibility = View.VISIBLE
                            binding.tvNoRecords.text = getString(R.string.nodata)
                            accountlist.clear()
                            updateAccountList()
                        } else {
//                            binding.tvNoRecords.visibility = View.VISIBLE
//                            binding.recyclerView.visibility = View.GONE

//                            val apiError =
//                                errorUtils!!.parseError((response.error as HttpException).response())
                            utility!!.showToast(this, "error_description")
                        }
                        // val code = (response.error as HttpException).code()
                        // Log.e(TAG, "consumeResponse() Exception code - $code")

                    } catch (ex: Exception) {
                        ex.printStackTrace()
//                        Log.e(Sea.TAG, "consumeResponse() Exception0 - ${ex.message}")
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
//            Log.e(s.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun updateAccountList() {
        try {
            if(accountlist.isEmpty()){
                binding.tvNoRecords.visibility = View.VISIBLE
                binding.recyclerView.visibility = View.GONE
                binding.accountsrecyclerView.visibility = View.GONE

            } else if(accountlist.isNotEmpty()){
                binding.tvNoRecords.visibility = View.GONE
                binding.recyclerView.visibility = View.GONE
                binding.accountsrecyclerView.visibility = View.VISIBLE

                accountsearchAdapter.updateList(accountlist)
            }

//            if(list.size.equals(0)){
//                binding.tvNoRecords.visibility = View.VISIBLE
//                binding.recyclerView.visibility = View.GONE
//            } else{
//                searchAdapter.updateList(list)
//            }
//            setListVisibility(list.isNotEmpty())
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    override fun onItemClick(item: AccountSearchUserDetails) {
        val intent_detail = Intent(this@SearchProductActivity, AccountUpdateActivity::class.java)
        intent_detail.putExtra("name",item!!.Name)
        intent_detail.putExtra("id",item!!.Id)
        intent_detail.putExtra("phone",item!!.Phone)
        intent_detail.putExtra("email",item!!.Email)
        intent_detail.putExtra("address",item!!.Address)

        startActivity(intent_detail)
        finish()
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        try {
            when {
                binding.searchLayout.searchText.text!!.hashCode() == s.hashCode() -> {

                    val conte= s?.length
                    if (!StringUtils.isBlank((s.toString())) && (s.toString().length > 2)) {
//                        binding.searchLayout.cancelImageV.visibility = View.VISIBLE
//                        editTypeStop(s.toString(), 400)
                        searchItemRequest = SearchItemRequest(
                            productSearch = s.toString(),
                        )

                        searchapiCall(searchItemRequest!!)
                    } else {
//                        binding.searchLayout.cancelImageV.visibility = View.GONE
                        onClearSearch()
                    }
                }
            }

            when {
                binding.searchLayout.searchAccText.text!!.hashCode() == s.hashCode() -> {

                    val conte= s?.length
                    if (!StringUtils.isBlank((s.toString())) && (s.toString().length > 3)) {
//                        binding.searchLayout.cancelImageV.visibility = View.VISIBLE
//                        editTypeStop(s.toString(), 400)
                        accountItemreq = AccountSearchRequest(
                            customerSearch = s.toString(),
                        )

                        searchaccountapiCall(accountItemreq!!)
                    } else {
//                        binding.searchLayout.cancelImageV.visibility = View.GONE
                        onClearSearch()
                    }
                }
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }


    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun onResume() {
        super.onResume()
        try {
            Log.e(SearchAdapter.TAG, "onResume()")
//            if (!StringUtils.isBlank(binding.searchLayout.searchText.text.toString().trim())) {
////                editTypeStop(binding.searchLayout.searchText.text.toString().trim(), 0)
//            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun onClearSearch() {
        try {
            list.clear()
            accountlist.clear()
//            binding.tvNoRecords.text = getString(R.string.searchWith)
            updateFirstList()
            updateAccountList()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }



}