package com.absyz.kotlin.activities.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.absyz.kotlin.R
import com.absyz.kotlin.adapters.AccountsSearchAdapter
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.CartActivityBinding
import com.absyz.kotlin.fragments.BaseFragment
import com.absyz.kotlin.model.RequestModel.*
import com.absyz.kotlin.model.ResponseModel.*
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel
import com.monsterbrain.recyclerviewtableview.CartAdapter
import com.monsterbrain.recyclerviewtableview.QuantityAdapter
import kotlinx.android.synthetic.main.create_visit.*

class CartActivity: BaseActivity(), CartAdapter.ItemClickListener, CartAdapter.QtyItemClickListener   {

    lateinit var binding: CartActivityBinding

    lateinit var homeViewModel: HomeViewModel

    private var cartProductdetails: ArrayList<ProductCartDetails> = ArrayList()
    var cartprodReq: CartProductRequest?= null

    var removeReq : RemoveCartItemRequest?= null

    var increaseQtyRequest : IncreaseProductQtyRequest ?= null

    private var customerCheckinReq : CustomerCheckInRequest ?= null

    private lateinit var cartAdapter: CartAdapter

    var cart_id : String = ""
    var total_price : String = ""
    var qty : String = ""
    var contact_num : String = ""
    var customer_identity : String = ""
    var cust_name : String = ""
    var error_msg : String = ""
    var phone_num : String = ""
    var cart_count : String = ""
    var cust_email : String = ""
    val full_qtylist = ArrayList<Int>()
    var current_qty : String = ""
    var cust_id : String = ""

    var selected_cartId : String = ""

    private var customerDetails: ArrayList<RecentCustomerDetails> = ArrayList()

    lateinit var customerValidationRequestModel : CustomerValidationRequestModel
    private var customerValidationValidationDetails: ArrayList<CustomerValidationDetailsModel> = ArrayList()
    lateinit var customerRegistrationRequest: CustomerRegistrationRequest

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.cart_activity)

        (application as ApplicationController).applicationComponent.inject(this)
        //View model instance
        homeViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)
        binding.targetRV.layoutManager = LinearLayoutManager(this)

        qty = intent.getStringExtra("qty").toString()

        //Recent Customer Observer
        homeViewModel.getRecentCustomerResponse()?.observe(this, { this.ConsumeRecentCustomerData(it as ApiResponse<*>) })
        //Call Recent Customer APi
        recentCustomerapiCall()

        //Observer
        homeViewModel!!.getCartProductDetailsResponse()?.observe(this, { this.ConsumeCartProductData(it as ApiResponse<*>) })

        cartprodReq = CartProductRequest(
            customerId = AppConstants.CustomerID
        )
        apiCall(cartprodReq!!)

        //Remove Cart Observer
        homeViewModel!!.getRemoveCartProductDetailsResponse()?.observe(this, { this.ConsumeRemoveCartProductData(it as ApiResponse<*>) })

        //Observer
        homeViewModel!!.getCustomerValidationResponse()
            ?.observe(this, { this.ConsumeCustomerValidationData(it as ApiResponse<*>) })

        //Observer
        homeViewModel!!.getCustomerRegistrationResponse()
            ?.observe(this, { this.ConsumeCustomerRegistartionData(it as ApiResponse<*>) })

        /*Increase Qty api*/
        homeViewModel.getIncreaseQtyResponse().observe(this, { this.consumeIncreaseQtyResponse(it as ApiResponse<*>) })

        //Create visit Observer
        homeViewModel.getcustomerCheckInResponse()
            ?.observe(this, { this.ConsumeCustomerCheckInData(it as ApiResponse<*>) })

        binding.burgerMenu.setOnClickListener() {
            onBackPressed()
            finish()
        }
    }

    private fun recentCustomerapiCall() {

        try {
            homeViewModel.recentCustomerDetails(
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeRecentCustomerData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeRecentCustomerData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is RecentLoginCustomerResponse) {
                                customerDetails.clear()
                                customerDetails.addAll(it.CustomerDetails!!)
                                updateRecentCustomerDetails()
                                //Toast.makeText(this, recentCustomerdetails?.get(0)?.Name.toString(),Toast.LENGTH_SHORT).show()

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //1Toast.makeText(this,apiResponse.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateRecentCustomerDetails() {
        try {
            val recentCustomerModel = customerDetails[0]
            //recent_customer_name = recentCustomerdetails?.get(0)?.Name.toString()
            AppConstants.Customer_Name = recentCustomerModel!!.Name
            AppConstants.CustomerID = recentCustomerModel!!.Id
            AppConstants.cartItemCount = recentCustomerModel!!.CartListCount
            cust_id = recentCustomerModel!!.Id

            customer_identity = recentCustomerModel!!.CustomerIdentity
            contact_num = recentCustomerModel!!.Phone
            cust_name = recentCustomerModel!!.Name
            cust_email = recentCustomerModel!!.Email
            if(customer_identity.equals("Customer")){
                binding.customerCard.visibility = View.VISIBLE
                binding.customerTitle.visibility = View.VISIBLE
                binding.customerNameTit.visibility = View.VISIBLE
                binding.customerName.visibility = View.VISIBLE
                binding.customerNumberTit.visibility = View.VISIBLE
                binding.customerNumber.visibility = View.VISIBLE
                binding.checkinBtn.visibility = View.VISIBLE
                binding.customerEmail.visibility = View.VISIBLE
                binding.customerEmailTit.visibility = View.VISIBLE
                binding.sep1.visibility = View.VISIBLE
                binding.sep2.visibility = View.VISIBLE
                binding.sep3.visibility = View.VISIBLE

                binding.customerName.text = cust_name
                binding.customerNumber.text = contact_num
                binding.customerEmail.text = cust_email
                binding.checkinBtn.setOnClickListener {
                    Toast.makeText(this,"Already Checked In!",Toast.LENGTH_SHORT).show()
                }
            } else {
                binding.visitCard.visibility = View.VISIBLE
                binding.custTitle.visibility = View.VISIBLE
                binding.searchView.visibility = View.VISIBLE
                binding.searchIcon.visibility = View.VISIBLE
                binding.errorMsg.visibility = View.VISIBLE
                binding.createNewBtn.visibility = View.VISIBLE

                binding.searchIcon.setOnClickListener {
                    isValidPhoneNumber()
                }
            }
            //Toast.makeText(this,AppConstants.Customer_Name,Toast.LENGTH_SHORT).show()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun isValidPhoneNumber() {
        phone_num = binding.custsearchText.text.toString()
        if(phone_num.equals("")){
            binding.custsearchText.setError("Enter Phonenumber!")
        } else if(phone_num.length<10 || phone_num.length>10){
            binding.custsearchText.setError("Enter a Valid Phonenumber!")
        }else{
            customerValidationRequestModel = CustomerValidationRequestModel(
                mobileNumber = binding.custsearchText.text.toString()
            )
            customerValidationapiCall(customerValidationRequestModel)
        }
    }

    private fun customerValidationapiCall(customerValidationRequestModel: CustomerValidationRequestModel) {

        try {
            homeViewModel.customerValidation(this,
                customerValidationRequestModel,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeCustomerValidationData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeCustomerValidationData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is CustomerValidationResponseModel) {
                                customerValidationValidationDetails.clear()
                                customerValidationValidationDetails.addAll(it.CustomerDetails!!)
                                updateCustomerValidation()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //2Toast.makeText(this,apiResponse.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateCustomerValidation() {
        try {
            val validation_model = customerValidationValidationDetails
            val d = Log.d("Customerval_res->", validation_model.toString())

            if(validation_model[0].Error.isNullOrEmpty()){

                //Enable Customer card
                binding.visitCard.visibility = View.GONE
                binding.custTitle.visibility = View.GONE
                binding.searchView.visibility = View.GONE
                binding.searchIcon.visibility = View.GONE
                binding.errorMsg.visibility = View.GONE
                binding.createNewBtn.visibility = View.GONE

                binding.customerCard.visibility = View.VISIBLE
                binding.customerTitle.visibility = View.VISIBLE
                binding.customerNameTit.visibility = View.VISIBLE
                binding.customerName.visibility = View.VISIBLE
                binding.customerNumberTit.visibility = View.VISIBLE
                binding.customerNumber.visibility = View.VISIBLE
                binding.checkinBtn.visibility = View.VISIBLE
                binding.customerEmail.visibility = View.VISIBLE
                binding.customerEmailTit.visibility = View.VISIBLE
                binding.sep1.visibility = View.VISIBLE
                binding.sep2.visibility = View.VISIBLE
                binding.sep3.visibility = View.VISIBLE

                binding.customerName.text = validation_model[0].Name
                binding.customerNumber.text = validation_model[0].Phone
                binding.customerEmail.text = validation_model[0].Email_c

                binding.checkinBtn.setOnClickListener {
                    //Toast.makeText(this,"Products added to Wishlist!",Toast.LENGTH_SHORT).show()
                    customerCheckinReq = CustomerCheckInRequest(
                        mobileNumber = validation_model[0].Phone
                    )
                    customercheckinapiCall(customerCheckinReq!!)
                }
                //var intent_cre = Intent(this, VisitExists::class.java)
//                intent_cre.putExtra("name",validation_model[0].Name)
//                intent_cre.putExtra("id",validation_model[0].Id)
//                intent_cre.putExtra("phone",validation_model[0].Phone)
//                intent_cre.putExtra("address",validation_model[0].Address)
//                intent_cre.putExtra("email",validation_model[0].Email_c)
                //startActivity(intent_cre)

            } else{
                //error_msg = validation_model[0].Error
                error_msg = "This number does not exist, Please create new customer."
                binding.errorMsg.text = error_msg

                binding.createNewBtn.setOnClickListener {

                    binding.visitCard.visibility = View.GONE
                    binding.custTitle.visibility = View.GONE
                    binding.searchView.visibility = View.GONE
                    binding.searchIcon.visibility = View.GONE
                    binding.errorMsg.visibility = View.GONE
                    binding.createNewBtn.visibility = View.GONE

                    //Enable register customer card
                    binding.createCustomer.visibility = View.VISIBLE
                    binding.createVisitTit2.visibility = View.VISIBLE
                    binding.enternumber2.visibility = View.VISIBLE
                    binding.entername.visibility = View.VISIBLE
                    binding.enteremail.visibility = View.VISIBLE
                    binding.enteraddress.visibility = View.VISIBLE
                    binding.createcheckinBtn.visibility = View.VISIBLE
                    binding.numSep.visibility = View.VISIBLE

                    binding.enternumber2.text = phone_num

                    binding.createcheckinBtn.setOnClickListener {
                        //Call validation creation api

                        customerRegistrationRequest = CustomerRegistrationRequest(
                            mobileNumber = phone_num,
                            name = binding.entername.text.toString(),
                            customerEmail = binding.enteremail.text.toString(),
                            customerAddress = binding.enteraddress.text.toString(),
                            salespersonId = AppConstants.SalesPersonID
                        )
                        customerregistrationapiCall(customerRegistrationRequest)
                    }

                }
                //var intent = Intent(this, VisitCreation::class.java)
                //intent.putExtra("error_msg",error_msg)
                //intent.putExtra("phone",binding.enternumber.text.toString())
                //startActivity(intent)
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun customerregistrationapiCall(customerRegistrationRequest: CustomerRegistrationRequest) {
        try {
            homeViewModel.customerRegistration(this,
                customerRegistrationRequest,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }
    }

    fun ConsumeCustomerRegistartionData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeCustomerRegistartionData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is CustomerRegistrationResponse ) {
                                Toast.makeText(this,"Successfully Registered!",Toast.LENGTH_SHORT).show()
                                //Enable Existing customer card
                                binding.createCustomer.visibility = View.GONE
                                binding.createVisitTit2.visibility = View.GONE
                                binding.enternumber2.visibility = View.GONE
                                binding.entername.visibility = View.GONE
                                binding.enteremail.visibility = View.GONE
                                binding.enteraddress.visibility = View.GONE
                                binding.createcheckinBtn.visibility = View.GONE
                                binding.numSep.visibility = View.GONE

                                customerValidationRequestModel = CustomerValidationRequestModel(
                                    mobileNumber = phone_num
                                )
                                customerValidationapiCall(customerValidationRequestModel)

                                binding.customerCard.visibility = View.VISIBLE
                                binding.customerTitle.visibility = View.VISIBLE
                                binding.customerNameTit.visibility = View.VISIBLE
                                binding.customerName.visibility = View.VISIBLE
                                binding.customerNumberTit.visibility = View.VISIBLE
                                binding.customerNumber.visibility = View.VISIBLE
                                binding.checkinBtn.visibility = View.VISIBLE
                                binding.customerEmail.visibility = View.VISIBLE
                                binding.customerEmailTit.visibility = View.VISIBLE
                                binding.sep1.visibility = View.VISIBLE
                                binding.sep2.visibility = View.VISIBLE
                                binding.sep3.visibility = View.VISIBLE

                                binding.checkinBtn.setOnClickListener {
                                    Toast.makeText(this,"Checked In Successfully!",Toast.LENGTH_SHORT).show()
                                }

//                                val validation_model = customerValidationValidationDetails
//                                binding.customerName.text = validation_model[0].Name
//                                binding.customerNumber.text = validation_model[0].Phone

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //2Toast.makeText(this,apiResponse.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun apiCall(cartprodReq: CartProductRequest) {

        try {
            homeViewModel.CartProductDetails(
                cartprodReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeCartProductData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeCartProductData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is CartProductResponse) {
                                cartProductdetails.clear()
                                cartProductdetails.addAll(it.ProductCartDetails!!)
                                //AppConstants.cartItemCount = it.CartListCount
                                AppConstants.cartItemCount = cartProductdetails.size
                                total_price = it.TotalPrice.toString()
                                binding.priceAmt.text = total_price
                                binding.cartCount.text = AppConstants.cartItemCount.toString()
                                binding.prodCountVal.text = AppConstants.cartItemCount.toString()
                                updateCartProductDetails()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //3Toast.makeText(this,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateCartProductDetails() {
        try {
            cartAdapter = CartAdapter(this,utility!!,this,this){index -> deleteItem(index)}
            binding.targetRV.adapter = cartAdapter
            cartAdapter.updateList(cartProductdetails)
            cartAdapter.setOnItemCLickListerner(object : CartAdapter.onItemClickListener{
                override fun onItemCLick(position: Int) {
                    //Toast.makeText(this@CartActivity, cartProductdetails[position].Name, Toast.LENGTH_SHORT).show()
                    val intent_detail = Intent(this@CartActivity, ProductDetailActivity::class.java)
                    intent_detail.putExtra("id",cartProductdetails[position].Id)
                    startActivity(intent_detail)
                }
            } )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun removeapiCall(removecartprodReq: RemoveCartItemRequest) {

        try {
            homeViewModel.RemoveCartProductDetails(
                removecartprodReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeRemoveCartProductData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeRemoveCartProductData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is RemoveCartItemResponse) {
                                updateRemovedCartDetails()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                   //3 Toast.makeText(this,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateRemovedCartDetails() {
        try {
            cartprodReq = CartProductRequest(
                customerId = cust_id
            )
            apiCall(cartprodReq!!)
            Toast.makeText(this,"Successfully Removed from Cart!", Toast.LENGTH_SHORT).show()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun deleteItem(index: Int){
        cart_id = cartProductdetails[index].Id

        removeReq = RemoveCartItemRequest(
            cartId = cart_id
        )
        removeapiCall(removeReq!!)
        cartProductdetails.removeAt(index)
        cartAdapter.setItems(cartProductdetails)
        AppConstants.cartItemCount = cartProductdetails.size
        binding.cartCount.text = AppConstants.cartItemCount.toString()
        binding.prodCountVal.text = AppConstants.cartItemCount.toString()
        //apiCall(cartprodReq!!)
    }

    override fun onItemClick(item: ProductCartDetails?) {
        //Toast.makeText(this@CartActivity, item!!.Quantity, Toast.LENGTH_SHORT).show()
        selected_cartId = item!!.Id
        var stock_val = item!!.TotalProductQuantity
        full_qtylist.clear()
            for(i in 1..stock_val) {
                full_qtylist.add(i)
            }

        cartAdapter.qtyItems(full_qtylist)
    }

    private fun increaseQtyapiCall(increaseQtyRequest: IncreaseProductQtyRequest) {

        try {
            homeViewModel.IncreaseQtyData(
                increaseQtyRequest,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeIncreaseQtyResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeIncreaseQtyResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is IncreaseProductQtyResponse) {
                                var comp_Message = it.Message
                                Toast.makeText(this,it.Message,Toast.LENGTH_SHORT).show()
                                cartprodReq = CartProductRequest(
                                    customerId = AppConstants.CustomerID
                                )
                                apiCall(cartprodReq!!)
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    override fun onQtyItemClick(qty_item: Int) {
        current_qty = qty_item.toString()
        increaseQtyRequest = IncreaseProductQtyRequest(
            cartId = selected_cartId,
            quantity = current_qty
        )
        increaseQtyapiCall(increaseQtyRequest!!)
    }

    private fun customercheckinapiCall(customercheckInReq: CustomerCheckInRequest) {

        try {
            homeViewModel.SendcustomerCheckInData(
                customercheckInReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeCustomerCheckInData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeCustomerCheckInData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is CustomerCheckInResponse) {
                                val customer_name = it.Name
                                Toast.makeText(this,"Checked In Successfully!", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //Toast.makeText(this,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }


}