package com.absyz.kotlin.activities.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.absyz.kotlin.R
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.CreateVisitBinding
import com.absyz.kotlin.model.RequestModel.CustomerValidationRequestModel
import com.absyz.kotlin.model.RequestModel.VisitorCreationRequest
import com.absyz.kotlin.model.ResponseModel.*
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.create_visit.*

class CreateVisit : BaseActivity() {

    lateinit var binding: CreateVisitBinding

    lateinit var homeViewModel: HomeViewModel
    lateinit var customerValidationRequestModel : CustomerValidationRequestModel
    lateinit var visitExists: VisitExists

    private var customerValidationValidationDetails: ArrayList<CustomerValidationDetailsModel> = ArrayList()
    private var visitcreationReq : VisitorCreationRequest ?= null

    var error_msg: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.create_visit)

        (application as ApplicationController).applicationComponent.inject(this)
        //View model instance
        homeViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        //Observer
        homeViewModel!!.getCustomerValidationResponse()
            ?.observe(this, { this.ConsumeCustomerValidationData(it as ApiResponse<*>) })

        //Visitor Creation Observer
        homeViewModel.getVisitorCreationResponse()
            ?.observe(this, { this.ConsumeVisitCreationData(it as ApiResponse<*>) })

        //onclick for skip
        binding.skip.setOnClickListener() {
            //Call visitor  creation api
            visitcreationReq = VisitorCreationRequest(
                salespersonId = AppConstants.SalesPersonID
            )
            visitcreationapiCall(visitcreationReq!!)

            val intent = Intent(this, LandingpageActivity::class.java)
            startActivity(intent)
            finish()
        }

        //onclick for continue
        binding.cont.setOnClickListener() {
            isValidPhoneNumber()
        }

    }

    // phonenumber validation
    private fun isValidPhoneNumber() {
        if(binding.enternumber.equals("")){
            binding.enternumber.setError("Enter Phonenumber!")
        } else if(binding.enternumber.length()<10 || enternumber.length()>10){
            binding.enternumber.setError("Enter a Valid Phonenumber!")
        } else{
            customerValidationRequestModel = CustomerValidationRequestModel(
                mobileNumber = binding.enternumber.text.toString()
            )
            apiCall(customerValidationRequestModel)
        }

//        if(enternumber.equals(1234567890)){
//            numberExist = true
//            val intent = Intent(this, VisitExists::class.java)
//            startActivity(intent)
//        }else if(enternumber.length()==10){
//            numberExist = false
//            val intent = Intent(this, VisitCreation::class.java)
//            startActivity(intent)
//        }
    }

    private fun apiCall(customerValidationRequestModel: CustomerValidationRequestModel) {

        try {
            homeViewModel.customerValidation(this,
                customerValidationRequestModel,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

     fun ConsumeCustomerValidationData(apiResponse: ApiResponse<*>) {
         Log.e(TAG, "ConsumeCustomerValidationData()")
         try{
             when (apiResponse.apiStatus) {
                 ApiStatus.LOADING -> showLoader()

                 ApiStatus.SUCCESS ->{
                     dismissLoader()
                     apiResponse?.let { apiResponse ->
                         apiResponse.data?.let {
                             if (it is CustomerValidationResponseModel) {
                                 customerValidationValidationDetails.clear()
                                 customerValidationValidationDetails.addAll(it.CustomerDetails!!)
                                 updateCustomerValidation()
                             }
                         }
                     }
                     }

                 ApiStatus.ERROR -> {
                     dismissLoader()
                     Toast.makeText(this,apiResponse.error!!.message,Toast.LENGTH_SHORT).show()
                 }
                 }
         }catch (ex:Exception) {
            ex.printStackTrace();
        }
     }

    private fun updateCustomerValidation() {
        try {
            val validation_model = customerValidationValidationDetails
            val d = Log.d("Customerval_res->", validation_model.toString())

            if(validation_model[0].Error.isNullOrEmpty()){

                var intent_cre = Intent(this, VisitExists::class.java)
                intent_cre.putExtra("name",validation_model[0].Name)
                intent_cre.putExtra("id",validation_model[0].Id)
                intent_cre.putExtra("phone",validation_model[0].Phone)
                intent_cre.putExtra("address",validation_model[0].Address)
                intent_cre.putExtra("email",validation_model[0].Email_c)

                startActivity(intent_cre)
                finish()

            } else{
                error_msg = validation_model[0].Error
                var intent = Intent(this, VisitCreation::class.java)
                intent.putExtra("error_msg",error_msg)
                intent.putExtra("phone",binding.enternumber.text.toString())
                startActivity(intent)
                finish()
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun visitcreationapiCall(visitcreationReq: VisitorCreationRequest) {

        try {
            homeViewModel.SendVisitorCreationListData(
                visitcreationReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }


    fun ConsumeVisitCreationData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeVisitCreationData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is VisitorCreationResponse) {
                                val visitor_id = it.Id
                                Toast.makeText(this,"Visitor Created Successfully!", Toast.LENGTH_SHORT).show()
                                finish()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }
}


