package com.absyz.kotlin.activities.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.absyz.kotlin.R
import com.absyz.kotlin.adapters.FilterColorPalletAdapter
import com.absyz.kotlin.adapters.FilterDetailsAdapter
import com.absyz.kotlin.adapters.InstockDetailsAdapter
import com.absyz.kotlin.adapters.ProductCategoryDetailsAdapter
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.ProductCategoryDetailActivityBinding
import com.absyz.kotlin.fragments.BaseFragment
import com.absyz.kotlin.model.RequestModel.FilterProductListRequest
import com.absyz.kotlin.model.RequestModel.FilterRequestModel
import com.absyz.kotlin.model.RequestModel.InstockRequestModel
import com.absyz.kotlin.model.RequestModel.ProductCategoryDetailsRequestModel
import com.absyz.kotlin.model.ResponseModel.*
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel
import com.monsterbrain.recyclerviewtableview.FilterCategoryAdapter
import com.monsterbrain.recyclerviewtableview.FilterMaterialAdapter
import kotlinx.android.synthetic.main.activity_filter_actiivty.*
import kotlinx.android.synthetic.main.cartlist_cview.view.*
import kotlinx.android.synthetic.main.filter_layout_item.view.*
import kotlinx.android.synthetic.main.filter_layout_item.view.close_btn

class ProductCategoryDetailActivity: BaseActivity()  {

    lateinit var binding: ProductCategoryDetailActivityBinding

    var category_id: String=""
    var category_name: String = ""
    var instock: String = "TRUE"
    var subcategoryval : String = ""
    var materialval : String = ""
    var dropdown : Boolean = false
    var materialdropdown : Boolean = false
    var color_selected : Boolean = false
    var colorname : String = "ffffff"

    lateinit var homeViewModel: HomeViewModel

    private var productCategoryDetails: ArrayList<ProductCategoryDetails> = ArrayList()
    private var instockDetails: ArrayList<InstockProductDetails> = ArrayList()

    var productcatDetailsPostString: ProductCategoryDetailsRequestModel?= null
    var instockDetailsPost : InstockRequestModel?= null

    var filterdetails : FilterRequestModel?= null

    private var filterProductListDetails: ArrayList<FilterProductDetails> = ArrayList()
    var filterProductRequest : FilterProductListRequest?= null

    var subcategorydetails : ArrayList<FilterProductSubCategory> = ArrayList()
    var materialdetails : ArrayList<FilterMaterial> = ArrayList()
    var colordetails : ArrayList<FilterColors> = ArrayList()

    var spinner_category : ArrayList<String> = ArrayList()
    var spinner_material : ArrayList<String> = ArrayList()

    var pagenumer : String = "1"
    var current_pg : String = ""
    var last_pg : String = ""

    var selected_subcat : String = ""
    var selected_material : String = ""
    var selected_color : String = ""
    var filter_instock : String = "False"
    var minprice : String = ""
    var maxprice : String = ""
    var minheight : String = ""
    var maxheight : String = ""
    var minwidth : String = ""
    var maxwidth : String = ""
    var mindepth : String = ""
    var maxdepth : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.product_category_detail_activity)

        category_id = intent.getStringExtra("id").toString()
        category_name = intent.getStringExtra("catname").toString()

        binding.fragmentTitle.text = category_name

        (application as ApplicationController).applicationComponent.inject(this)

        //View model instance
        homeViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        //Wishlist APi details
        val layoutManager = GridLayoutManager(this, 4)
        binding.targetRV.setLayoutManager(layoutManager);
        val layoutManagerInstock = GridLayoutManager(this, 4)
        binding.instockRV.setLayoutManager(layoutManagerInstock);
        val layoutManagerFilter = GridLayoutManager(this, 4)
        binding.filterRV.setLayoutManager(layoutManagerFilter);

        val colorlayoutManager = GridLayoutManager(this, 5)
        binding.filterNavigationview.filtercontent_layout.color_rv.setLayoutManager(colorlayoutManager);

        binding.filterNavigationview.filtercontent_layout.category_rv.layoutManager = LinearLayoutManager(this)
        binding.filterNavigationview.filtercontent_layout.material_rv.layoutManager = LinearLayoutManager(this)

        //Observer
        homeViewModel!!.getProductCategoryDetailsResponse()?.observe(this, { this.consumeProductCategoryDetailsResponse(it as ApiResponse<*>) })
        //Instock Observer
        homeViewModel!!.getInstockProductDetailsResponse()?.observe(this, { this.consumeinStockDetailsResponse(it as ApiResponse<*>) })
        //FilterDetail Observer
        homeViewModel!!.getFilterDetailsResponse()?.observe(this, { this. consumeFilterDetailsResponse(it as ApiResponse<*>) })
        //Filtered Products Observer
        homeViewModel!!.getFilteredProductsResponse()?.observe(this, { this.consumeFilteredProductResponse(it as ApiResponse<*>) })

        binding.instockCheck.setOnCheckedChangeListener { buttonView, isChecked ->
            //Call instock api
            if (isChecked){
                instockDetailsPost = InstockRequestModel(
                    inStockValue = instock,
                    productCategoryId = category_id
                )
                sendInstockAPi(instockDetailsPost!!)
            }else{
                //Api Call
                productcatDetailsPostString = ProductCategoryDetailsRequestModel(
                    productCategoryId = category_id
                )
                sendAPi(productcatDetailsPostString!!,pagenumer)            }

        }

            //Api Call
            productcatDetailsPostString = ProductCategoryDetailsRequestModel(
                productCategoryId = category_id
            )
            sendAPi(productcatDetailsPostString!!,pagenumer)

            filterdetails = FilterRequestModel(
                productCategoryId = category_id
            )
            sendFilter(filterdetails!!)


//        val toggle = ActionBarDrawerToggle(this,binding.filterdrawerlayout,toolbar,R.string.open,R.string.close)
//        toggle.isDrawerIndicatorEnabled = true
//        binding.filterdrawerlayout.addDrawerListener(toggle)
//        toggle.syncState()
        //binding.filterNavigationview.setNavigationItemSelectedListener (this)
        binding.filterIcon.setOnClickListener{
            binding.filterdrawerlayout.openDrawer(GravityCompat.END)
        }

        binding.burgerMenu.setOnClickListener() {
            onBackPressed()
            finish()
        }

    }

    private fun sendAPi(productcatDetailsPostString: ProductCategoryDetailsRequestModel,pagenumber: String) {

        try {
            homeViewModel.ProductCategoryDetailsData(
                productcatDetailsPostString,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeProductCategoryDetailsResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeProductCategoryDetailsResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is ProductCategoryDetailsResponseModel) {
                                productCategoryDetails.clear()
                                productCategoryDetails.addAll(it.ProductDetails!!)
                                current_pg = it.currentPage
                                last_pg = it.lastPage.toString()
                                updateProductCategoryDetailsList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun updateProductCategoryDetailsList() {
        try {
            if(productCategoryDetails.size <= 0){
                binding.nodata.visibility = View.VISIBLE
                binding.targetRV.visibility = View.GONE
            } else if(productCategoryDetails.size > 0){
                binding.nodata.visibility = View.GONE
                binding.filterRV.visibility = View.GONE
                binding.instockRV.visibility = View.GONE
                binding.targetRV.visibility = View.VISIBLE
                val productCatAdapter = ProductCategoryDetailsAdapter(this,utility!!)
                binding.targetRV.adapter = productCatAdapter
                productCatAdapter?.updateList(productCategoryDetails)

                productCatAdapter.setOnItemCLickListerner(object : ProductCategoryDetailsAdapter.onItemClickListener{
                    override fun onItemCLick(position: Int) {
                        //Toast.makeText(this@ProductCategoryDetailActivity, productCategoryDetails[position].Name, Toast.LENGTH_SHORT).show()

                        val intent_detail = Intent(this@ProductCategoryDetailActivity, ProductDetailActivity::class.java)
                        //                   intent_detail.putExtra("name",productCategoryDetails[position].Name)
                        intent_detail.putExtra("id",productCategoryDetails[position].Id)
                        intent_detail.putExtra("cat_id",category_id)
                        intent_detail.putExtra("from_activity_name","product_category_details")

//                    intent_detail.putExtra("producttype",productCategoryDetails[position].ProductType)
//                    intent_detail.putExtra("imageId",productCategoryDetails[position].ProductImage)
//                    intent_detail.putExtra("category",productCategoryDetails[position].ProductSubCategory)
//                    intent_detail.putExtra("dimentions",productCategoryDetails[position].Dimentions)
//                    intent_detail.putExtra("desc",productCategoryDetails[position].ProductDescription)
//                    intent_detail.putExtra("price",productCategoryDetails[position].Price.toString())
//                    intent_detail.putExtra("qty",productCategoryDetails[position].Quantity.toString())

                        startActivity(intent_detail)
                    }
                } )
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun sendInstockAPi(instockPost: InstockRequestModel) {

        try {
            homeViewModel.InstockProductDetailsData(
                instockPost,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeinStockDetailsResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeinStockDetailsResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is InstockResponseModel) {
                                instockDetails.clear()
                                instockDetails.addAll(it.ProductDetails)
                                current_pg = it.currentPage
                                last_pg = it.lastPage.toString()
                                updateInstockDetailsList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun updateInstockDetailsList() {
        try {
            if(instockDetails.size <= 0){
                binding.nodata.visibility = View.VISIBLE
                binding.instockRV.visibility = View.GONE
            } else if(instockDetails.size > 0) {
                binding.nodata.visibility = View.GONE
                binding.filterRV.visibility = View.GONE
                binding.targetRV.visibility = View.GONE
                binding.instockRV.visibility = View.VISIBLE
                val instockdetailsAdapter = InstockDetailsAdapter(this, utility!!)
                binding.instockRV.adapter = instockdetailsAdapter
                instockdetailsAdapter?.updateList(instockDetails)

                instockdetailsAdapter.setOnItemCLickListerner(object :
                    InstockDetailsAdapter.onItemClickListener {
                    override fun onItemCLick(position: Int) {
                        //Toast.makeText(this@ProductCategoryDetailActivity, productCategoryDetails[position].Name, Toast.LENGTH_SHORT).show()

                        val intent_detail = Intent(
                            this@ProductCategoryDetailActivity,
                            ProductDetailActivity::class.java
                        )
                        //                   intent_detail.putExtra("name",productCategoryDetails[position].Name)
                        intent_detail.putExtra("id", instockDetails[position].Id)
                        intent_detail.putExtra("from_activity_name", "product_category_details")

                        startActivity(intent_detail)
                    }
                })
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun sendFilter(filterReq: FilterRequestModel) {

        try {
            homeViewModel.FilterDetailsData(
                filterReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeFilterDetailsResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeFilterDetailsResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is FilterDropDownResponseModel) {
                                subcategorydetails.addAll(it.ProductSubCategories)
                                materialdetails.addAll(it.Materials)
                                colordetails.addAll(it.Colours)
                                updateFilterDetailsList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun updateFilterDetailsList() {
        try {

            //Sub category
            for(i in subcategorydetails.indices) {
                spinner_category.add(subcategorydetails[i].Type)
            }
            binding.filterNavigationview.filtercontent_layout.prodview.setOnClickListener {
                if(dropdown.equals(false)){
                    dropdown = true
                    binding.filterNavigationview.filtercontent_layout.category_dropdown.visibility = View.VISIBLE

                    //binding.filterNavigationview.filtercontent_layout.dropdown_icon.setBackgroundResource(R.drawable.spinner_up);

                    val filterCategoryAdapter = FilterCategoryAdapter(this,utility!!)
                    binding.filterNavigationview.filtercontent_layout.category_rv.adapter = filterCategoryAdapter
                    filterCategoryAdapter?.updateList(spinner_category)

                    filterCategoryAdapter.setOnItemCLickListerner(object : FilterCategoryAdapter.onItemClickListener{
                        override fun onItemCLick(position: Int) {
                            //Toast.makeText(this@ProductCategoryDetailActivity, colordetails[position].Colour, Toast.LENGTH_SHORT).show()
                            selected_subcat = spinner_category[position].toString()
                            binding.filterNavigationview.filtercontent_layout.prod_item.text = spinner_category[position]
                            binding.filterNavigationview.filtercontent_layout.category_dropdown.visibility = View.GONE

                        }
                    } )
                } else if(dropdown.equals(true)){
                    dropdown = false
                    binding.filterNavigationview.filtercontent_layout.category_dropdown.visibility = View.GONE

                }

            }

            binding.filterNavigationview.filtercontent_layout.spinnericon.setOnClickListener {
                if(dropdown.equals(false)){
                    dropdown = true
                    binding.filterNavigationview.filtercontent_layout.category_dropdown.visibility = View.VISIBLE

                    //binding.filterNavigationview.filtercontent_layout.dropdown_icon.setBackgroundResource(R.drawable.spinner_up);

                    val filterCategoryAdapter = FilterCategoryAdapter(this,utility!!)
                    binding.filterNavigationview.filtercontent_layout.category_rv.adapter = filterCategoryAdapter
                    filterCategoryAdapter?.updateList(spinner_category)

                    filterCategoryAdapter.setOnItemCLickListerner(object : FilterCategoryAdapter.onItemClickListener{
                        override fun onItemCLick(position: Int) {
                            //Toast.makeText(this@ProductCategoryDetailActivity, colordetails[position].Colour, Toast.LENGTH_SHORT).show()
                            selected_subcat = spinner_category[position].toString()
                            binding.filterNavigationview.filtercontent_layout.prod_item.text = spinner_category[position]
                            binding.filterNavigationview.filtercontent_layout.category_dropdown.visibility = View.GONE

                        }
                    } )
                } else if(dropdown.equals(true)){
                    dropdown = false
                    binding.filterNavigationview.filtercontent_layout.category_dropdown.visibility = View.GONE

                }
            }

            /*if (binding.filterNavigationview.filtercontent_layout.prod_item != null) {
           val adapter = ArrayAdapter(this,
               android.R.layout.simple_spinner_item, spinner_category)
           binding.filterNavigationview.filtercontent_layout.prod_item.adapter = adapter
                selected_subcat = ""
           binding.filterNavigationview.filtercontent_layout.prod_item.onItemSelectedListener = object :
               AdapterView.OnItemSelectedListener {
               override fun onItemSelected(parent: AdapterView<*>,
                                           view: View, position: Int, id: Long) {
                   selected_subcat = spinner_category[position]
                    //Toast.makeText(this@ProductCategoryDetailActivity, spinner_category[position], Toast.LENGTH_SHORT).show()
               }

               override fun onNothingSelected(parent: AdapterView<*>) {
                   // write code to perform some action
                   selected_subcat = ""

               }
           }
       }*/

            //Material
            for(i in materialdetails.indices) {
                spinner_material.add(materialdetails[i].Material)
            }

            binding.filterNavigationview.filtercontent_layout.matview.setOnClickListener {
                if(materialdropdown.equals(false)){
                    materialdropdown = true
                    //binding.filterNavigationview.filtercontent_layout.dropdown_icon.setBackgroundResource(R.drawable.spinner_up);

                    binding.filterNavigationview.filtercontent_layout.material_dropdown.visibility = View.VISIBLE

                    val filterMaterialAdapter = FilterMaterialAdapter(this,utility!!)
                    binding.filterNavigationview.filtercontent_layout.material_rv.adapter = filterMaterialAdapter
                    filterMaterialAdapter?.updateList(spinner_material)

                    filterMaterialAdapter.setOnItemCLickListerner(object : FilterMaterialAdapter.onItemClickListener{
                        override fun onItemCLick(position: Int) {
                            //Toast.makeText(this@ProductCategoryDetailActivity, colordetails[position].Colour, Toast.LENGTH_SHORT).show()
                            selected_material = spinner_material[position]
                            binding.filterNavigationview.filtercontent_layout.material_item.text = spinner_material[position]
                            binding.filterNavigationview.filtercontent_layout.material_dropdown.visibility = View.GONE

                        }
                    } )
                } else if(materialdropdown.equals(true)){
                    materialdropdown = false
                    binding.filterNavigationview.filtercontent_layout.material_dropdown.visibility = View.GONE

                }

            }

            binding.filterNavigationview.filtercontent_layout.spinnericon2.setOnClickListener {
                if(materialdropdown.equals(false)){
                    materialdropdown = true
                    //binding.filterNavigationview.filtercontent_layout.dropdown_icon.setBackgroundResource(R.drawable.spinner_up);

                    binding.filterNavigationview.filtercontent_layout.material_dropdown.visibility = View.VISIBLE

                    val filterMaterialAdapter = FilterMaterialAdapter(this,utility!!)
                    binding.filterNavigationview.filtercontent_layout.material_rv.adapter = filterMaterialAdapter
                    filterMaterialAdapter?.updateList(spinner_material)

                    filterMaterialAdapter.setOnItemCLickListerner(object : FilterMaterialAdapter.onItemClickListener{
                        override fun onItemCLick(position: Int) {
                            //Toast.makeText(this@ProductCategoryDetailActivity, colordetails[position].Colour, Toast.LENGTH_SHORT).show()
                            selected_material = spinner_material[position]
                            binding.filterNavigationview.filtercontent_layout.material_item.text = spinner_material[position]
                            binding.filterNavigationview.filtercontent_layout.material_dropdown.visibility = View.GONE

                        }
                    } )
                } else if(materialdropdown.equals(true)){
                    materialdropdown = false
                    binding.filterNavigationview.filtercontent_layout.material_dropdown.visibility = View.GONE

                }
            }

            /*if (binding.filterNavigationview.filtercontent_layout.material_item != null) {
                val adapter = ArrayAdapter(this,
                    android.R.layout.simple_spinner_item, spinner_material)
                binding.filterNavigationview.filtercontent_layout.material_item.adapter = adapter
                selected_material = ""
                binding.filterNavigationview.filtercontent_layout.material_item.onItemSelectedListener = object :
                    AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(parent: AdapterView<*>,
                                                view: View, position: Int, id: Long) {
                        selected_material = spinner_material[position]
                        //Toast.makeText(this@ProductCategoryDetailActivity, spinner_material[position], Toast.LENGTH_SHORT).show()
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {
                        // write code to perform some action
                        selected_material = ""
                    }
                }
            }*/

            //Color
            val filterColorPalletAdapter = FilterColorPalletAdapter(this,utility!!)
            binding.filterNavigationview.filtercontent_layout.color_rv.adapter = filterColorPalletAdapter
            filterColorPalletAdapter?.updateList(colordetails)

            filterColorPalletAdapter.setOnItemCLickListerner(object : FilterColorPalletAdapter.onItemClickListener{
                override fun onItemCLick(position: Int) {
                    //Toast.makeText(this@ProductCategoryDetailActivity, colordetails[position].Colour, Toast.LENGTH_SHORT).show()
                    if(colorname.equals("ffffff")){
                        colorname = "34b347"
                        selected_color = colordetails[position].Colour
                        filterColorPalletAdapter.changebgcolor( colorname)
                    }else if(colorname.equals("34b347")){
                        selected_color = ""
                        colorname = "ffffff"
                        filterColorPalletAdapter.changebgcolor(colorname)
                    }

                }
            } )

            //Apply filter btn
            binding.filterNavigationview.filtercontent_layout.apply_btn.setOnClickListener{
                minprice = binding.filterNavigationview.filtercontent_layout.min_val.text.toString()
                maxprice = binding.filterNavigationview.filtercontent_layout.max_val.text.toString()
                minheight = binding.filterNavigationview.filtercontent_layout.height_min_val.text.toString()
                maxheight = binding.filterNavigationview.filtercontent_layout.height_max_val.text.toString()

                if(binding.filterNavigationview.filtercontent_layout.instock_check.isChecked){
                    filter_instock = "True"
                }
                filterProductRequest = FilterProductListRequest(
                    productCategoryId = category_id,
                    productSubCategory = selected_subcat,
                    material = selected_material,
                    colourCode = selected_color,
                    instock = filter_instock,
                    minPrice = minprice,
                    maxPrice = maxprice,
                    minHight = minheight,
                    maxHight = maxheight,
                    minWidth = binding.filterNavigationview.filtercontent_layout.width_min_val.text.toString(),
                    maxWidth = binding.filterNavigationview.filtercontent_layout.width_max_val.text.toString(),
                    minDepth = binding.filterNavigationview.filtercontent_layout.depth_min_val.text.toString(),
                    maxDepth = binding.filterNavigationview.filtercontent_layout.depth_max_val.text.toString()

                )
                sendFilterProductList(filterProductRequest!!)
                binding.filterdrawerlayout.closeDrawers()
            }

            //Reset button click
            binding.filterNavigationview.filtercontent_layout.reset_btn.setOnClickListener {
                //Toast.makeText(this@ProductCategoryDetailActivity, "Reset button", Toast.LENGTH_SHORT).show()
                //Add reset values here
                selected_subcat  = ""
                selected_material = ""
                filter_instock = "False"
                minprice  = ""
                maxprice  = ""
                minheight  = ""
                maxheight  = ""
                minwidth = ""
                maxwidth  = ""
                mindepth = ""
                maxdepth = ""

                if(binding.filterNavigationview.filtercontent_layout.instock_check.isChecked){
                    filter_instock = "false"
                }

                binding.filterNavigationview.filtercontent_layout.min_val.setText(minprice)
                binding.filterNavigationview.filtercontent_layout.max_val.setText(maxprice)
                binding.filterNavigationview.filtercontent_layout.height_min_val.setText(minheight)
                binding.filterNavigationview.filtercontent_layout.height_max_val.setText(maxheight)
                binding.filterNavigationview.filtercontent_layout.width_min_val.setText(minwidth)
                binding.filterNavigationview.filtercontent_layout.width_max_val.setText(maxwidth)
                binding.filterNavigationview.filtercontent_layout.depth_min_val.setText(mindepth)
                binding.filterNavigationview.filtercontent_layout.depth_max_val.setText(maxdepth)
                binding.filterNavigationview.filtercontent_layout.prod_item.text = ""
                binding.filterNavigationview.filtercontent_layout.material_item.text = ""

                    selected_color = ""
                    colorname = "ffffff"
                    filterColorPalletAdapter.changebgcolor(colorname)

            }
            binding.filterNavigationview.filtercontent_layout.close_btn.setOnClickListener {
                binding.filterdrawerlayout.closeDrawers()
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun sendFilterProductList(filterProductListReq: FilterProductListRequest) {

        try {
            homeViewModel.FilteredProductsData(
                filterProductListReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeFilteredProductResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeFilteredProductResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is FilterProductListResponse) {
                                filterProductListDetails.clear()
                                filterProductListDetails.addAll(it.ProductDetails)
                                //Toast.makeText(this,"Filter Applied Successfully!",Toast.LENGTH_SHORT).show()
                                updateFilterProductList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun updateFilterProductList() {
        try {
            if(filterProductListDetails.size <= 0){
                binding.nodata.visibility = View.VISIBLE
                binding.filterRV.visibility = View.GONE
            } else if(filterProductListDetails.size > 0) {
                binding.nodata.visibility = View.GONE
                binding.filterRV.visibility = View.VISIBLE
                binding.targetRV.visibility = View.GONE
                binding.instockRV.visibility = View.GONE
                val filterDetailAdapter = FilterDetailsAdapter(this, utility!!)
                binding.filterRV.adapter = filterDetailAdapter
                filterDetailAdapter?.updateList(filterProductListDetails)

                filterDetailAdapter.setOnItemCLickListerner(object :
                    FilterDetailsAdapter.onItemClickListener {
                    override fun onItemCLick(position: Int) {
                        //Toast.makeText(this@ProductCategoryDetailActivity, productCategoryDetails[position].Name, Toast.LENGTH_SHORT).show()

                        val intent_detail = Intent(
                            this@ProductCategoryDetailActivity,
                            ProductDetailActivity::class.java
                        )
                        //intent_detail.putExtra("name",productCategoryDetails[position].Name)
                        intent_detail.putExtra("id", productCategoryDetails[position].Id)
                        intent_detail.putExtra("from_activity_name", "product_category_details")

                        startActivity(intent_detail)
                    }
                })
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

}

