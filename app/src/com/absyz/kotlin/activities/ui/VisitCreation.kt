package com.absyz.kotlin.activities.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.absyz.kotlin.R
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.CreatevisitValidationCreateBinding
import com.absyz.kotlin.model.RequestModel.CustomerCheckInRequest
import com.absyz.kotlin.model.RequestModel.CustomerRegistrationRequest
import com.absyz.kotlin.model.ResponseModel.*
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel

class VisitCreation : BaseActivity() {

    lateinit var binding: CreatevisitValidationCreateBinding

    lateinit var homeviewModel: HomeViewModel
    lateinit var customerRegistrationRequest: CustomerRegistrationRequest

    var name: String = ""
    var error_msg: String = ""
    var phone : String = ""
    var session_active = ""

    private var customerCheckinReq : CustomerCheckInRequest ?= null

    private var customerDetails: ArrayList<RecentCustomerDetails> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.createvisit_validation_create)

            binding.enternumber2.visibility = View.VISIBLE
            binding.entername.visibility = View.VISIBLE
            binding.enteremail.visibility = View.VISIBLE
            binding.enteraddress.visibility = View.VISIBLE
            binding.createcheckinBtn.visibility = View.VISIBLE

        (application as ApplicationController).applicationComponent.inject(this)
        //View model instance
        homeviewModel =
            ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        //Observer
        homeviewModel!!.getCustomerRegistrationResponse()
            ?.observe(this, { this.ConsumeCustomerRegistartionData(it as ApiResponse<*>) })

        //Customer checkin
        homeviewModel.getcustomerCheckInResponse()
            ?.observe(this, { this.ConsumeCustomerCheckInData(it as ApiResponse<*>) })

        //Recent Customer Observer
        homeviewModel.getRecentCustomerResponse()
            ?.observe(this, { this.ConsumeRecentCustomerData(it as ApiResponse<*>) })


        error_msg = intent.getStringExtra("error_msg").toString()
        phone = intent.getStringExtra("phone").toString()

        binding.enternumber2.text = phone


        binding.createcheckinBtn.setOnClickListener {
                //Call validation creation api

            customerRegistrationRequest = CustomerRegistrationRequest(
                mobileNumber  = phone,
                name = binding.entername.text.toString(),
                customerEmail =  binding.enteremail.text.toString(),
                customerAddress = binding.enteraddress.text.toString(),
                salespersonId = AppConstants.SalesPersonID
            )
            apiCall(customerRegistrationRequest)

            //Call Customer checkin
            customerCheckinReq = CustomerCheckInRequest(
                mobileNumber = phone
            )
            customerapiCall(customerCheckinReq!!)

            //Call recent customer
            recentCustomerapiCall()
        }

    }

    private fun customerapiCall(customercheckInReq: CustomerCheckInRequest) {

        try {
            homeviewModel.SendcustomerCheckInData(
                customercheckInReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeCustomerCheckInData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeCustomerCheckInData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is CustomerCheckInResponse) {
                                val customer_name = it.Name
                                //Toast.makeText(this,"Checked In Successfully!", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun recentCustomerapiCall() {

        try {
            homeviewModel.recentCustomerDetails(
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }


    private fun apiCall(customerRegistrationRequest: CustomerRegistrationRequest) {
        try {
            homeviewModel.customerRegistration(this,
                customerRegistrationRequest,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }
    }

    fun ConsumeCustomerRegistartionData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeCustomerRegistartionData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is CustomerRegistrationResponse ) {
                                name = it.Name
                                session_active = it.Active_c
                                val intent = Intent(this, LandingpageActivity::class.java)
                                startActivity(intent)
                                Toast.makeText(this,"Successfully Registered!",Toast.LENGTH_SHORT).show()
                                finish()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,apiResponse.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    fun ConsumeRecentCustomerData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeRecentCustomerData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is RecentLoginCustomerResponse) {
                                customerDetails.clear()
                                customerDetails.addAll(it.CustomerDetails!!)
                                updateRecentCustomerDetails()
                                //Toast.makeText(this, recentCustomerdetails?.get(0)?.Name.toString(),Toast.LENGTH_SHORT).show()

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,apiResponse.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateRecentCustomerDetails() {
        try {
            val recentCustomerModel = customerDetails[0]
            //recent_customer_name = recentCustomerdetails?.get(0)?.Name.toString()
            AppConstants.Customer_Name = recentCustomerModel!!.Name
            AppConstants.CustomerID = recentCustomerModel!!.Id
            //Toast.makeText(this,AppConstants.Customer_Name,Toast.LENGTH_SHORT).show()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

}
