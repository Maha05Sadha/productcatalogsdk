package com.absyz.kotlin.activities.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.absyz.kotlin.R
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.CreatevisitValidationExistBinding
import com.absyz.kotlin.model.RequestModel.CustomerCheckInRequest
import com.absyz.kotlin.model.ResponseModel.*
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel

class VisitExists : BaseActivity() {

    lateinit var binding: CreatevisitValidationExistBinding

    var cust_id : String = ""
    var cust_name : String = ""
    var cust_email : String = ""
    var cust_phone : String = ""
    var cust_address : String = ""
    var recent_customer_name : String = ""

    var data_val : Int = 1
    private var customerValidationValidationDetails: List<CustomerValidationDetailsModel>? = null

    lateinit var homeViewModel: HomeViewModel

    private var customerCheckinReq : CustomerCheckInRequest ?= null

    private var customerDetails: ArrayList<RecentCustomerDetails> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.createvisit_validation_exist)

        (application as ApplicationController).applicationComponent.inject(this)
        //View model instance
        homeViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        //Visitor Creation Observer
        homeViewModel.getcustomerCheckInResponse()
            ?.observe(this, { this.ConsumeCustomerCheckInData(it as ApiResponse<*>) })

        //Recent Customer Observer
        homeViewModel.getRecentCustomerResponse()
            ?.observe(this, { this.ConsumeRecentCustomerData(it as ApiResponse<*>) })

        binding.createvisitExist.visibility = View.VISIBLE
        binding.createVisitTit1.visibility = View.VISIBLE
        binding.validationIcon.visibility = View.VISIBLE
        binding.enternumber1.visibility = View.VISIBLE
        binding.nameTit.visibility = View.VISIBLE
        binding.nameVal.visibility = View.VISIBLE
        binding.emailTit.visibility = View.VISIBLE
        binding.emailVal.visibility = View.VISIBLE
        binding.addressTit.visibility = View.VISIBLE
        binding.addressVal.visibility = View.VISIBLE
        binding.checkinBtn.visibility = View.VISIBLE

        //val dataModel = customerValidationDetails?.get(data_val)
        cust_id = intent.getStringExtra("id").toString()
        cust_name = intent.getStringExtra("name").toString()
        cust_phone = intent.getStringExtra("phone").toString()
        cust_email = intent.getStringExtra("email").toString()
        cust_address = intent.getStringExtra("address").toString()


        binding.enternumber1.text = cust_phone
        binding.nameVal.text = cust_name
        binding.emailVal.text = cust_email
        binding.addressVal.text = cust_address

        customerCheckinReq = CustomerCheckInRequest(
            mobileNumber = cust_phone
        )
        apiCall(customerCheckinReq!!)

        binding.checkinBtn.setOnClickListener {
            //Call Recent Customer API
            recentCustomerapiCall()
            val intent = Intent(this, LandingpageActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun apiCall(customercheckInReq: CustomerCheckInRequest) {

        try {
            homeViewModel.SendcustomerCheckInData(
                customercheckInReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun recentCustomerapiCall() {

        try {
            homeViewModel.recentCustomerDetails(
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeCustomerCheckInData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeCustomerCheckInData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is CustomerCheckInResponse) {
                                val customer_name = it.Name
                                //Toast.makeText(this,"Checked In Successfully!", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    fun ConsumeRecentCustomerData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeRecentCustomerData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is RecentLoginCustomerResponse) {
                                customerDetails.clear()
                                customerDetails.addAll(it.CustomerDetails!!)
                                updateRecentCustomerDetails()
                                //Toast.makeText(this, recentCustomerdetails?.get(0)?.Name.toString(),Toast.LENGTH_SHORT).show()

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,apiResponse.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateRecentCustomerDetails() {
        try {
            val recentCustomerModel = customerDetails[0]
            //recent_customer_name = recentCustomerdetails?.get(0)?.Name.toString()
            AppConstants.Customer_Name = recentCustomerModel!!.Name
            AppConstants.CustomerID = recentCustomerModel!!.Id
            //Toast.makeText(this,AppConstants.Customer_Name,Toast.LENGTH_SHORT).show()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

}
