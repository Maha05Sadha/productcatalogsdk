package com.absyz.kotlin.activities.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.NavUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.absyz.kotlin.R
import com.absyz.kotlin.adapters.ComparisonAdapter
import com.absyz.kotlin.adapters.WishlistAdapter
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.ActivityAccountUpdateBinding
import com.absyz.kotlin.fragments.AccountFragment
import com.absyz.kotlin.fragments.BaseFragment
import com.absyz.kotlin.model.RequestModel.*
import com.absyz.kotlin.model.ResponseModel.*
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel
import com.monsterbrain.recyclerviewtableview.AccountsRelatedWishlistAdapter

class AccountUpdateActivity : BaseActivity(), AccountsRelatedWishlistAdapter.ItemClickListener  {

    lateinit var binding: ActivityAccountUpdateBinding

    var acc_name: String = ""
    var acc_phone: String = ""
    var acc_email: String = ""
    var acc_state: String = ""
    var acc_city: String = ""
    var acc_id: String=""
    var address: String = ""
    var wishlist_count : Int = 0
    var name: String = ""
    var cart_count : Int = 0

    lateinit var homeViewModel: HomeViewModel

    private var customerWishlistDetails: ArrayList<RelatedWishlistDetails> = ArrayList()

    lateinit var customerRelatedRequestModel: CustomerRelatedRequestModel
    lateinit var accountsUpdateRequestModel: AccountsUpdateRequestModel

    var addtocartRequest: AddtoCartRequest?= null
    var removewishlistReq : RemoveWishlistRequest?= null

    private lateinit var wishlistAdapter: AccountsRelatedWishlistAdapter

    private var customerDetails: ArrayList<RecentCustomerDetails> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_account_update)

        acc_name = intent.getStringExtra("name").toString()
        acc_phone = intent.getStringExtra("phone").toString()
        acc_email = intent.getStringExtra("email").toString()
        //acc_state = intent.getStringExtra("state").toString()
        //acc_city = intent.getStringExtra("city").toString()
        address = intent.getStringExtra("address").toString()
        acc_id = intent.getStringExtra("id").toString()

        //address = acc_city + ", "+ acc_state

        binding.fragmentTitle.text = acc_name

        binding.accNameVal.setText(acc_name)
        binding.accNumVal.setText(acc_phone)
        binding.accEmailVal.setText(acc_email)
        binding.accAddressVal.setText(address)

        (application as ApplicationController).applicationComponent.inject(this)
        //View model instance
        homeViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        //Recent Customer Observer
        homeViewModel.getRecentCustomerResponse()?.observe(this, { this.ConsumeRecentCustomerData(it as ApiResponse<*>) })
        //Call Recent Customer APi
        recentCustomerapiCall()

        //Wishlist APi details
        binding.wishlistRV.layoutManager = LinearLayoutManager(this)
        //Observer
        homeViewModel!!.getCustomerRelatedWishlistResponse()?.observe(this, { this.ConsumeCustomerRelatedWishlistData(it as ApiResponse<*>) })

        customerRelatedRequestModel = CustomerRelatedRequestModel(
            customerID = acc_id
        )
        apiCall(customerRelatedRequestModel)

        //Accounts Update API
        //Observer
        homeViewModel!!.getCAccountsUpdateResponse()
            ?.observe(this, { this.ConsumeAccountsUpdateData(it as ApiResponse<*>) })

        binding.updateBtn.setOnClickListener() {
            accountsUpdateRequestModel = AccountsUpdateRequestModel(
                customerId  = acc_id,
                customerName = binding.accNameVal.text.toString(),
                mobileNumber =  binding.accNumVal.text.toString(),
                customerAddress = binding.accAddressVal.text.toString(),
                customerEmail = binding.accEmailVal.text.toString()
                )

            updateAccountapiCall(accountsUpdateRequestModel)
        }

        /*Add to cart api*/
        homeViewModel!!.getAddtoCartResponse()?.observe(this, { this.consumeAddtoCartResponse(it as ApiResponse<*>) })

        /*Remove wishlist api*/
        homeViewModel.getRemoveWishlistResponse().observe(this, { this.consumeRemoveWishlistResponse(it as ApiResponse<*>) })

        binding.burgerMenu.setOnClickListener() {
            onBackPressed()
            finish()
        }

        binding.cartIcon.setOnClickListener{
            val intent_detail = Intent(this@AccountUpdateActivity, CartActivity::class.java)
            startActivity(intent_detail)
            finish()
        }

    }

    private fun updateAccountapiCall(accountsUpdateRequestModel: AccountsUpdateRequestModel) {
        try {
            homeViewModel.accountsUpdation(this,
                accountsUpdateRequestModel,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }
    }

    private fun apiCall(customerRelatedRequestModel: CustomerRelatedRequestModel) {

        try {
            homeViewModel.customerRelatedWishlist(this,
                customerRelatedRequestModel,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeCustomerRelatedWishlistData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeCustomerRelatedWishlistData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is CustomerRelatedWishlistResponse) {
                                customerWishlistDetails.clear()
                                customerWishlistDetails.addAll(it.WishListDetails!!)
                                updateCustomerwishList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //5 Toast.makeText(this,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateCustomerwishList() {
        try {
            wishlist_count = customerWishlistDetails.size
            binding.wishlistCount.text = wishlist_count.toString()
            if(customerWishlistDetails.size.equals(0)){
                binding.wishlistRV.visibility = View.GONE
                binding.nodata.visibility = View.VISIBLE
            } else {
                wishlistAdapter = AccountsRelatedWishlistAdapter(this,utility!!,this){index -> deleteItem(index)}
                binding.wishlistRV.adapter = wishlistAdapter
                wishlistAdapter?.updateList(customerWishlistDetails)
                wishlistAdapter.setOnItemCLickListerner(object : AccountsRelatedWishlistAdapter.onItemClickListener{
                    override fun onItemCLick(position: Int) {
                        //Toast.makeText(this@AccountUpdateActivity, customerWishlistDetails[position].Name, Toast.LENGTH_SHORT).show()

                        val intent_detail = Intent(this@AccountUpdateActivity, ProductDetailActivity::class.java)
//                        intent_detail.putExtra("name",customerWishlistDetails[position].Name)
                          intent_detail.putExtra("id",customerWishlistDetails[position].Id)
                          intent_detail.putExtra("from_activity_name","accounts_wishlist")
//                        intent_detail.putExtra("producttype",customerWishlistDetails[position].ProductType)
//                        intent_detail.putExtra("imageId",customerWishlistDetails[position].ProductImage)
//                        intent_detail.putExtra("category",customerWishlistDetails[position].Category)
//                        intent_detail.putExtra("dimentions",customerWishlistDetails[position].Dimentions)
//                        intent_detail.putExtra("desc",customerWishlistDetails[position].ProductDescription)
//                        intent_detail.putExtra("price",customerWishlistDetails[position].Price.toString())
//                        intent_detail.putExtra("qty",customerWishlistDetails[position].Quantity.toString())

                        startActivity(intent_detail)
                    }
                } )

            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

//    fun addToCart(index: Int){
//        //cart_id = cartProductdetails[index].Id
//
//        addtocartRequest = AddtoCartRequest(
//            customerId = AppConstants.CustomerID,
//            productId = customerWishlistDetails[index].Id,
//            colourCode = customerWishlistDetails[index].Colour,
//            colourName = customerWishlistDetails[index].ColourName,
//            location = customerWishlistDetails[index].ProductLocation,
//            productImage = customerWishlistDetails[index].ProductImage,
//            prodQuantity = "1",
//            productTotalQuantity = customerWishlistDetails[index].Quantity.toString()
//        )
//        callAddtoCartapi(addtocartRequest!!)
//    }

    private fun callAddtoCartapi(addtocartReq: AddtoCartRequest) {

        try {
            homeViewModel.AddtoCartData(
                addtocartReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeAddtoCartResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeAddtoCartResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is AddtoCartResponse) {
                                //addtocartAttribute.addAll(listOf(it.attributes))
                                //cartproductId = it.Id
                                var comp_Message = it.Message
                                if(comp_Message.equals("The product is out of stock.")){
                                    wishlistAdapter.addtocart("no")
                                    Toast.makeText(this,"This product is out of stock!",Toast.LENGTH_SHORT).show()
                                } else {
                                    //cartProductIDList.add(cartproductId)
                                    wishlistAdapter.addtocart("yes")

                                    var cartval : Int = 0
                                    cartval = AppConstants.cartItemCount + 1
                                    //Toast.makeText(this,"The product is now added to the cart.",Toast.LENGTH_SHORT).show()
                                    binding.cartCount.text = cartval.toString()
                                    //binding.addtocart.visibility = View.GONE
                                    //binding.addedcartlayout.visibility = View.VISIBLE
                                    Toast.makeText(this,"Product successfully added to cart!",Toast.LENGTH_SHORT).show()

                                }
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //4 Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    fun ConsumeAccountsUpdateData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeAccountsUpdateData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is AccountsUpdateResponseModel) {
                                name = it.Name
                                Toast.makeText(this,"Successfully Updated!",Toast.LENGTH_SHORT).show()
                                finish()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    // 3 Toast.makeText(this,apiResponse.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun recentCustomerapiCall() {

        try {
            homeViewModel.recentCustomerDetails(
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeRecentCustomerData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeRecentCustomerData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is RecentLoginCustomerResponse) {
                                customerDetails.clear()
                                customerDetails.addAll(it.CustomerDetails!!)
                                updateRecentCustomerDetails()
                                //Toast.makeText(this, recentCustomerdetails?.get(0)?.Name.toString(),Toast.LENGTH_SHORT).show()

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //2 Toast.makeText(this,apiResponse.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateRecentCustomerDetails() {
        try {
            val recentCustomerModel = customerDetails[0]
            //recent_customer_name = recentCustomerdetails?.get(0)?.Name.toString()
            AppConstants.Customer_Name = recentCustomerModel!!.Name
            AppConstants.CustomerID = recentCustomerModel!!.Id
            AppConstants.cartItemCount = recentCustomerModel!!.CartListCount
            //Toast.makeText(this,AppConstants.Customer_Name,Toast.LENGTH_SHORT).show()

            cart_count = recentCustomerModel!!.CartListCount
            binding.cartCount.text = cart_count.toString()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun wishlistRemoveapiCall(wishlistRemoveRequest: RemoveWishlistRequest) {

        try {
            homeViewModel.RemoveWishlistData(
                wishlistRemoveRequest,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeRemoveWishlistResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeRemoveWishlistResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is RemoveWishlistResponse) {
                                var comp_Message = it.Message
                                Toast.makeText(this,it.Message,Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //1 Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    fun deleteItem(index: Int){
        var wishlist_id = customerWishlistDetails[index].Id

        removewishlistReq = RemoveWishlistRequest(
            wishlistId = wishlist_id
        )
        wishlistRemoveapiCall(removewishlistReq!!)
        customerWishlistDetails.removeAt(index)
        wishlistAdapter.setItems(customerWishlistDetails)
        AppConstants.compareItemCount = customerWishlistDetails.size
        binding.wishlistCount.text = AppConstants.compareItemCount.toString()

    }

    override fun onItemClick(item: RelatedWishlistDetails) {
        addtocartRequest = AddtoCartRequest(
            customerId = AppConstants.CustomerID,
            productId = item.Id,
            colourCode = item.Colour,
            colourName = item.ColourName,
            location = item.ProductLocation,
            productImage = item.ProductImage,
            prodQuantity = "1",
            productTotalQuantity = item.Quantity.toString()
        )
        callAddtoCartapi(addtocartRequest!!)
    }
}

