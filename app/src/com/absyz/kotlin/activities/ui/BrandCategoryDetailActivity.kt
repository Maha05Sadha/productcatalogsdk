package com.absyz.kotlin.activities.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.absyz.kotlin.R
import com.absyz.kotlin.adapters.*
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.ProductCategoryDetailActivityBinding
import com.absyz.kotlin.fragments.BaseFragment
import com.absyz.kotlin.model.RequestModel.*
import com.absyz.kotlin.model.ResponseModel.*
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.activity_filter_actiivty.*
import kotlinx.android.synthetic.main.filter_layout_item.view.*

class BrandCategoryDetailActivity: BaseActivity() {

    lateinit var binding: ProductCategoryDetailActivityBinding

    lateinit var homeViewModel: HomeViewModel

    private var productBrandDetails: ArrayList<ProductBrandDetails> = ArrayList()
    private var instockDetails: ArrayList<InstockProductDetails> = ArrayList()

    var brandPostString: BrandProductsRequest? = null
    var instockDetailsPost: InstockRequestModel? = null

    var brand_id: String = ""
    var brand_name: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.product_category_detail_activity)

        brand_id = intent.getStringExtra("id").toString()
        brand_name = intent.getStringExtra("name").toString()

        binding.fragmentTitle.text = brand_name

        (application as ApplicationController).applicationComponent.inject(this)

        //View model instance
        homeViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        //Wishlist APi details
        val layoutManager = GridLayoutManager(this, 4)
        binding.targetRV.setLayoutManager(layoutManager);
        binding.filterIcon.visibility = View.GONE
        binding.filterTxt.visibility = View.GONE
        binding.instockCheck.visibility = View.GONE

        //Observer
        homeViewModel!!.getBrandCategoryDetailsResponse()
            ?.observe(this, { this.consumeBrandCategoryDetailsResponse(it as ApiResponse<*>) })

        //Api Call
        brandPostString = BrandProductsRequest(
            productBrandId = brand_id
        )
        sendAPi(brandPostString!!)

        binding.burgerMenu.setOnClickListener() {
            onBackPressed()
            finish()
        }

    }

    private fun sendAPi(brandProdReq: BrandProductsRequest) {

        try {
            homeViewModel.BrandCategoryDetailsData(
                brandProdReq,
                "Bearer " + AppConstants.ACCESS_TOKEN
            )
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeBrandCategoryDetailsResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeBrandCategoryDetailsResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is BrandProductsResponse) {
                                productBrandDetails.clear()
                                productBrandDetails.addAll(it.ProductDetails!!)
                                updateBrandCategoryDetailsList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    //1 Toast.makeText(this, response.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun updateBrandCategoryDetailsList() {
        try {
            if (productBrandDetails.size <= 0) {
                binding.nodata.visibility = View.VISIBLE
                binding.targetRV.visibility = View.GONE
            } else if (productBrandDetails.size > 0) {
                binding.nodata.visibility = View.GONE
                binding.filterRV.visibility = View.GONE
                binding.instockRV.visibility = View.GONE
                binding.targetRV.visibility = View.VISIBLE
                val productBrandAdapter = ProductBrandDetailsAdapter(this, utility!!)
                binding.targetRV.adapter = productBrandAdapter
                productBrandAdapter?.updateList(productBrandDetails)

                productBrandAdapter.setOnItemCLickListerner(object :
                    ProductBrandDetailsAdapter.onItemClickListener {
                    override fun onItemCLick(position: Int) {
                        //Toast.makeText(this@ProductCategoryDetailActivity, productCategoryDetails[position].Name, Toast.LENGTH_SHORT).show()

                        val intent_detail = Intent(
                            this@BrandCategoryDetailActivity,
                            ProductDetailActivity::class.java
                        )
                        //                   intent_detail.putExtra("name",productCategoryDetails[position].Name)
                        intent_detail.putExtra("id", productBrandDetails[position].Id)

//                    intent_detail.putExtra("producttype",productCategoryDetails[position].ProductType)
//                    intent_detail.putExtra("imageId",productCategoryDetails[position].ProductImage)
//                    intent_detail.putExtra("category",productCategoryDetails[position].ProductSubCategory)
//                    intent_detail.putExtra("dimentions",productCategoryDetails[position].Dimentions)
//                    intent_detail.putExtra("desc",productCategoryDetails[position].ProductDescription)
//                    intent_detail.putExtra("price",productCategoryDetails[position].Price.toString())
//                    intent_detail.putExtra("qty",productCategoryDetails[position].Quantity.toString())

                        startActivity(intent_detail)
                    }
                })
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }


}

