package com.absyz.kotlin.activities.ui

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.absyz.kotlin.R
import com.absyz.kotlin.adapters.ColorPalletAdapter
import com.absyz.kotlin.adapters.ColorPalletAdapter2
import com.absyz.kotlin.adapters.ComparisonAdapter
import com.absyz.kotlin.dagger.ApplicationController
import com.absyz.kotlin.databinding.ProductDetailsBinding
import com.absyz.kotlin.fragments.BaseFragment
import com.absyz.kotlin.model.RequestModel.*
import com.absyz.kotlin.model.ResponseModel.*
import com.checkin.helpers.AppConstants
import com.checkin.network.ApiResponse
import com.checkin.network.ApiStatus
import com.checkin.viewmodel.HomeViewModel
import com.monsterbrain.recyclerviewtableview.AddressAdapter
import com.monsterbrain.recyclerviewtableview.AddressAdapter2
import kotlinx.android.synthetic.main.product_details.*
import okhttp3.ResponseBody
import org.json.JSONObject

class ProductDetailActivity : BaseActivity(), CompoundButton.OnCheckedChangeListener {

    lateinit var binding: ProductDetailsBinding

    var prod_id: String = ""
    var from_activity_name: String = ""

    lateinit var homeViewModel: HomeViewModel

    private var productViewDetails: ArrayList<ProductViewDetails> = ArrayList()
    private var productColorCodes: ArrayList<ProductColorCodes> = ArrayList()
    private var productStorageList: ArrayList<ProductStorageList> = ArrayList()

    private var addtocartAttribute: ArrayList<AddtoCartAttribute> = ArrayList()
    private var addtocompareAttribute: ArrayList<AddtoCompareAttribute> = ArrayList()

    private var cartProductIDList: ArrayList<String> = ArrayList()
    private var compareProductIDList: ArrayList<String> = ArrayList()

    private var addressDetails: ArrayList<AddressDetails> = ArrayList()
    private var stockvalueDetails: ArrayList<StockValueDetails> = ArrayList()
    private var colorcodeDetails: ArrayList<ColorCodeDetails> = ArrayList()
    private var productimagesDetails: ArrayList<ColorPorductImageDetails> = ArrayList()
    private var colorNamesDetails: ArrayList<ColorNameDetails> = ArrayList()

    private var colorcodeLocationDetails: ArrayList<ColorCodeDetails> = ArrayList()

    private var customerDetails: ArrayList<RecentCustomerDetails> = ArrayList()

    private var compareProductdetails: ArrayList<ComparisonDetails> = ArrayList()

    var productViewPostString: ProductViewCallOutRequest?= null
    var productColorpostString : ProductColorViewRequest?= null
    var addtocartRequest: AddtoCartRequest?= null
    var addtocompareRequest: AddtoCompareRequest?= null
    var removecomparisonRequest : RemoveComparisonRequest?= null
    var wishlistCreationRequest : WishlistCreationRequest ?= null
    var wishlistRemoveRequest : WishlistRemoveRequest ?= null
    var compareprodReq: ComparisonRequest?= null


    val addressList = ArrayList<String>()
    val qtyList = ArrayList<Int>()
    val full_qtylist = ArrayList<Int>()

    val coloraddressList = ArrayList<String>()
    val colorqtyList = ArrayList<Int>()
    val colorImageidList = ArrayList<String>()
    val messageList = ArrayList<String>()

    var imageId: String = ""
    var imageId1: String = ""
    var imageId2: String = ""
    var imageId3: String = ""
    var imageId4: String = ""

    var stock : String = ""
    var instock_val : String = ""
    var comp_Message : String = ""
    var stock_val : Int = 0

    var image_url : String = ""
    var image_url1 : String = ""
    var image_url2 : String = ""
    var image_url3 : String = ""
    var image_url4 : String = ""


    var cartproductId : String = ""
    var compareproductId : String = ""

    var productcategory_id : String = ""

    var colorname: String = ""
    var colorcode : String = ""
    var selected_imageid : String = ""
    var selected_address : String = ""
    var selected_qty : String = "1"
    var wishlist_id : String = ""
    var dropdown : Boolean = false

    private var cartProductdetails: ArrayList<ProductCartDetails> = ArrayList()
    var cartprodReq: CartProductRequest?= null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.product_details)

        prod_id = intent.getStringExtra("id").toString()
        from_activity_name = intent.getStringExtra("from_activity_name").toString()
        productcategory_id = intent.getStringExtra("cat_id").toString()

        (application as ApplicationController).applicationComponent.inject(this)

        val layoutManager = GridLayoutManager(this, 5)
        binding.colorRv.setLayoutManager(layoutManager);
        val layoutManager2 = GridLayoutManager(this, 5)
        binding.colorRv2.setLayoutManager(layoutManager2);

        binding.addressRv.layoutManager = LinearLayoutManager(this)
        binding.addressRv2.layoutManager = LinearLayoutManager(this)

        //View model instance
        homeViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        //Observer
        /*product view callout api*/
        homeViewModel.getProductViewCAlloutResponse().observe(this, { this.consumeProductViewCallOutResponse(it as ApiResponse<*>) })
        /*Colors view details api*/
        homeViewModel.getProductColorDetailsResponse().observe(this, { this.consumeProductColorViewResponse(it as ApiResponse<*>) })
        /*location view details api*/
        homeViewModel.getProductLocationDetailsResponse().observe(this, { this.consumeProductLocationViewResponse(it as ApiResponse<*>) })
        /*Add to cart api*/
        homeViewModel.getAddtoCartResponse().observe(this, { this.consumeAddtoCartResponse(it as ApiResponse<*>) })
        /*Add to compare api*/
        homeViewModel.getAddtoCompareResponse().observe(this, { this.consumeAddtoCompareResponse(it as ApiResponse<*>) })
        /*Add to Remove comparison list api*/
        homeViewModel.getRemoveCompareResponse().observe(this, { this.consumeRemoveCompareResponse(it as ApiResponse<*>) })
        //Recent Customer Observer
        homeViewModel.getRecentCustomerResponse()?.observe(this, { this.ConsumeRecentCustomerData(it as ApiResponse<*>) })
        /*Create Wishlist api*/
        homeViewModel.getWishlistCreationDetailsResponse().observe(this, { this.consumeWishlistCreationResponse(it as ApiResponse<*>) })
        /*Remove wishlist api*/
        homeViewModel.getRemoveWishlistResponse().observe(this, { this.consumeRemoveWishlistResponse(it as ApiResponse<*>) })
        //Observer
        homeViewModel!!.getCompareDetailsResponse()?.observe(this, { this.ConsumeComparisonProductData(it as ApiResponse<*>) })


        //Call Recent Customer APi
        recentCustomerapiCall()

        //Api Call
        productViewPostString = ProductViewCallOutRequest(
            productId = prod_id
        )
        callProductViewapi(productViewPostString!!)

        //Observer
        homeViewModel!!.getCartProductDetailsResponse()?.observe(this, { this.ConsumeCartProductData(it as ApiResponse<*>) })

        cartprodReq = CartProductRequest(
            customerId = AppConstants.CustomerID
        )
        apiCall(cartprodReq!!)

        binding.likeToggle.setOnCheckedChangeListener(this)

        binding.burgerMenu.setOnClickListener() {
            finish()
            onBackPressed()
        }

        compareprodReq = ComparisonRequest(
            customerId = AppConstants.CustomerID
        )
        apiCall(compareprodReq!!)

        binding.addtocart.setOnClickListener{
            //if(instock_val.equals(true)){
                    if(AppConstants.CustomerID.isNullOrEmpty()){
                    Toast.makeText(this,"Please Register a Customer!",Toast.LENGTH_SHORT).show()
                } else {
                    addtocartRequest = AddtoCartRequest(
                        customerId = AppConstants.CustomerID,
                        productId = prod_id,
                        colourCode = colorcode,
                        colourName = colorname,
                        location = selected_address,
                        productImage = selected_imageid,
                        prodQuantity = selected_qty,
                        productTotalQuantity = stock_val.toString()
                    )
                    callAddtoCartapi(addtocartRequest!!)
                }
//            } else{
//                Toast.makeText(this,"This Product is currently Out of Stock!",Toast.LENGTH_SHORT).show()
//
//            }

        }

        binding.addtocompareBtn.setOnClickListener{
            if(AppConstants.CustomerID.isNullOrEmpty()){
                Toast.makeText(this,"Please Register a Customer!",Toast.LENGTH_SHORT).show()
            } else{
                try {
                    addtocompareRequest = AddtoCompareRequest(
                        customerId = AppConstants.CustomerID,
                        productId = prod_id,
                        productCategoryId = productcategory_id,
                        colourCode = colorcode,
                        colourName = colorname,
                        location = selected_address,
                        productImage = selected_imageid

                    )
                    callAddtoCompareapi(addtocompareRequest!!)

                } catch (ex : NullPointerException){
                    ex.printStackTrace()
                    Log.e(BaseFragment.TAG, "${ex.message}")
                }

            }
        }

        binding.floatComparison.setOnClickListener {
            val intent_detail =
                Intent(this@ProductDetailActivity, ComparisonActivity::class.java)
            intent_detail.putExtra("proc_cat_id", productcategory_id)
            startActivity(intent_detail)
            finish()
        }

        binding.cartIcon.setOnClickListener{
            val intent_detail = Intent(this@ProductDetailActivity, CartActivity::class.java)
            startActivity(intent_detail)
            finish()
        }

    }

    private fun callProductViewapi(productcalloutreqBody: ProductViewCallOutRequest) {

        try {
            homeViewModel.ProductViewCalloutData(
                productcalloutreqBody,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeProductViewCallOutResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeProductViewCallOutResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is ProductViewCalloutResponse) {
                                productViewDetails.addAll(it.ProductDetails)
                                productColorCodes.addAll(it.ProductColourCodes)
                                productStorageList.addAll(it.ProductStorageList)

                                updateProductViewCalloutList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun updateProductViewCalloutList() {
        try {
            //Product View Details
            imageId = productViewDetails[0].ProductImage
            image_url = AppConstants.BaseImageURL+imageId+"/VersionData"
            imageId1 = productViewDetails[0].ProductImage1
            image_url1 = AppConstants.BaseImageURL+imageId1+"/VersionData"
            imageId2 = productViewDetails[0].ProductImage2
            image_url2 = AppConstants.BaseImageURL+imageId2+"/VersionData"
            imageId3 = productViewDetails[0].ProductImage3
            image_url3 = AppConstants.BaseImageURL+imageId3+"/VersionData"
            imageId4 = productViewDetails[0].ProductImage4
            image_url4 = AppConstants.BaseImageURL+imageId4+"/VersionData"

            utility?.loadGlide(this,binding.productMainImg,image_url,0)
            selected_imageid = productViewDetails[0].ProductImage
            utility?.loadGlide(this,binding.imageView1,image_url1,0)
            utility?.loadGlide(this,binding.imageView2,image_url2,0)
            utility?.loadGlide(this,binding.imageView3,image_url3,0)
            utility?.loadGlide(this,binding.imageView4,image_url4,0)

            //Image Expand
            binding.imgExpandIcon.setOnClickListener {
                binding.alertView.visibility = View.VISIBLE
                binding.imgExpandCard.visibility = View.VISIBLE

                image_url = AppConstants.BaseImageURL+selected_imageid+"/VersionData"
                utility?.loadGlide(this,binding.expandedImg,image_url,0)

                binding.closeImage.setOnClickListener {
                    binding.alertView.visibility = View.GONE
                    binding.imgExpandCard.visibility = View.GONE
                }
            }

            //Load multiple images
//            utility?.loadGlide(this,binding.imageView1,image_url,0)
//            utility?.loadGlide(this,binding.imageView2,image_url,0)
//            utility?.loadGlide(this,binding.imageView3,image_url,0)
//            utility?.loadGlide(this,binding.imageView4,image_url,0)

            binding.prodName.text = productViewDetails[0].Name
            binding.categoryTit.text = productViewDetails[0].ProductSubCategory
            //binding.prodId.text = productViewDetails[0].Id
            binding.prodDimen.text = productViewDetails[0].Dimentions
            binding.prodDesc.text = productViewDetails[0].ProductDescription
            binding.price.text = productViewDetails[0].Price.toString()
            if(productViewDetails[0].ColourCode.isNullOrEmpty()){
                colorcode = "D6A318"
            } else{
                colorcode = productViewDetails[0].ColourCode
            }

            if(productViewDetails[0].ColourName.isNullOrEmpty()){
                colorname = "Galliano"
            } else{
                colorname = productViewDetails[0].ColourName
            }

            instock_val = productViewDetails[0].IsProductAvailable.toString()
            if (productViewDetails[0].IsProductAvailable.equals(true)) {
                binding.prodStock.text = "INSTOCK"
                binding.prodStock.setTextColor(ContextCompat.getColor(this, R.color.validation_green));
            } else if (productViewDetails[0].IsProductAvailable.equals(false)) {
                binding.prodStock.text = "NOSTOCK"
                binding.prodStock.setTextColor(ContextCompat.getColor(this, R.color.validation_red));
            }

            if(productStorageList[0].Address.isNullOrEmpty()){
                selected_address = "JNTU , Hyderabad"
            } else{
                selected_address = productStorageList[0].Address
            }

            //selected_address = productStorageList[0].Address
            binding.address.text = selected_address
            binding.storeInfoProdcount.text = productStorageList[0].StockValue.toString()

            if(productStorageList[0].StockValue<=2){
                binding.storeInfo1.setTextColor(Color.parseColor("#ff0000"))
                binding.storeInfo3.setTextColor(Color.parseColor("#ff0000"))
                binding.storeInfoProdcount.setTextColor(Color.parseColor("#ff0000"))
            } else{
                binding.storeInfo1.setTextColor(Color.parseColor("#757575"))
                binding.storeInfo3.setTextColor(Color.parseColor("#757575"))
                binding.storeInfoProdcount.setTextColor(Color.parseColor("#757575"))
            }

            for(i in productStorageList.indices) {
                addressList.add(productStorageList[i].Address)
                qtyList.add(productStorageList[i].StockValue)
            }

            //Address List
            binding.addressView.setOnClickListener{
                if(dropdown.equals(false)){
                    dropdown = true
                    binding.addressCard.visibility = View.VISIBLE
                } else if (dropdown.equals(true)){
                    dropdown = false
                    binding.addressCard.visibility = View.GONE

                }
            }
            binding.addressRv.visibility = View.VISIBLE
            binding.addressRv2.visibility = View.GONE
            val addresslistAdapter = AddressAdapter(this,utility!!)
            binding.addressRv.adapter = addresslistAdapter
            addresslistAdapter?.updateList(productStorageList)

            //binding.qtyVal.text = productStorageList[0].StockValue.toString()
            if(productStorageList[0].StockValue.equals(0)){
                stock_val = 1
            } else{
                stock_val = productStorageList[0].StockValue
            }
            //stock_val = productStorageList[0].StockValue
            full_qtylist.clear()
            if(stock_val > 0){
                for(i in 1..stock_val) {
                    full_qtylist.add(i)
                }
            } else{
                full_qtylist.add(0)
            }

            //Quantity Spinner
            /*if (binding.quantity != null) {
                val adapter = ArrayAdapter(this,
                    android.R.layout.simple_spinner_item, full_qtylist)
                binding.quantity.adapter = adapter

                binding.quantity.onItemSelectedListener = object :
                    AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(parent: AdapterView<*>,
                                                view: View, position: Int, id: Long) {
                        selected_qty = full_qtylist[position].toString()
                    }
                    override fun onNothingSelected(parent: AdapterView<*>) {
                        // write code to perform some action
                    }
                }
            }*/
            addresslistAdapter.setOnItemCLickListerner(object : AddressAdapter.onItemClickListener{
                override fun onItemCLick(position: Int) {
                    //Toast.makeText(this@ProductDetailActivity, productStorageList[position].Address, Toast.LENGTH_SHORT).show()
                    //selected_address = productStorageList[position].Address
                    if(productStorageList[position].Address.isNullOrEmpty()){
                        selected_address = "JNTU , Hyderabad"
                    } else{
                        selected_address = productStorageList[position].Address
                    }
                    binding.address.text = selected_address
                    binding.storeInfoProdcount.text = productStorageList[position].StockValue.toString()
                    //binding.qtyVal.text = productStorageList[position].StockValue.toString()

                    stock_val = productStorageList[position].StockValue
                    full_qtylist.clear()
                    if(stock_val > 0){
                        for(i in 1..stock_val) {
                            full_qtylist.add(i)
                        }
                    } else{
                        full_qtylist.add(0)
                    }
                    binding.addressCard.visibility = View.GONE

                    if(productStorageList[position].StockValue<=2){
                        binding.storeInfo1.setTextColor(Color.parseColor("#ff0000"))
                        binding.storeInfo3.setTextColor(Color.parseColor("#ff0000"))
                        binding.storeInfoProdcount.setTextColor(Color.parseColor("#ff0000"))
                    } else{
                        binding.storeInfo1.setTextColor(Color.parseColor("#757575"))
                        binding.storeInfo3.setTextColor(Color.parseColor("#757575"))
                        binding.storeInfoProdcount.setTextColor(Color.parseColor("#757575"))
                    }
                }
            } )

            //Quantity Spinner
            if (binding.quantity != null) {
                val adapter = ArrayAdapter(this,
                    android.R.layout.simple_spinner_item, full_qtylist)
                binding.quantity.adapter = adapter

                binding.quantity.onItemSelectedListener = object :
                    AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(parent: AdapterView<*>,
                                                view: View, position: Int, id: Long) {
                        selected_qty = full_qtylist[position].toString()
                    }
                    override fun onNothingSelected(parent: AdapterView<*>) {
                        // write code to perform some action
                    }
                }
            }


            //Color Pallet
            binding.colorpalletView.setOnClickListener(){
                binding.colorpalletLayout.visibility = View.VISIBLE

                if(productColorCodes.size <=0){
                    binding.emptypallet.visibility =View.VISIBLE
                    binding.colorsViewConstrain.visibility = View.GONE
                } else if(productColorCodes.size >0){
                    binding.colorsViewConstrain.visibility = View.VISIBLE
                    binding.colorViewCurrent.setOnClickListener {
                        //colorname = productViewDetails[0].ColourName
                        if(productViewDetails[0].ColourName.isNullOrEmpty()){
                            colorname = "Galliano"
                        } else{
                            colorname = productViewDetails[0].ColourName
                        }
                        imageUpdater(productViewDetails[0].ColourCode,productViewDetails[0].ProductImage)
                    }
                    binding.emptypallet.visibility =View.GONE
                }

            }

            val colorpalletAdapter = ColorPalletAdapter(this,utility!!)
            binding.colorRv.visibility = View.VISIBLE
            binding.colorRv2.visibility = View.GONE
            binding.colorRv.adapter = colorpalletAdapter
            colorpalletAdapter?.updateList(productColorCodes)

            colorpalletAdapter.setOnItemCLickListerner(object : ColorPalletAdapter.onItemClickListener{
                override fun onItemCLick(position: Int) {
                    //  Toast.makeText(this@ProductDetailActivity, productColorCodes[position].ProductColourCode, Toast.LENGTH_SHORT).show()
                    //colorcode = productColorCodes[position].ProductColourCode
                    if(productColorCodes[position].ProductColourCode.isNullOrEmpty()){
                        colorcode = "D6A318"
                    } else{
                        colorcode = productColorCodes[position].ProductColourCode
                    }
                    if(productColorCodes[position].ProductColourName.isNullOrEmpty()){
                        colorname = "Galliano"
                    } else{
                        colorname = productColorCodes[position].ProductColourName
                    }
                    //colorname = productColorCodes[position].ProductColourName
                    binding.colorpallet.setBackgroundColor(Color.parseColor("#"+productColorCodes[position].ProductColourCode))
                    //colorcode = productColorCodes[position].ProductColourCode
                    binding.colorpalletLayout.visibility = View.GONE
//                    //Call colors api
                    productColorpostString = ProductColorViewRequest(
                        productId = prod_id,
                        colourCode = colorcode,
                        location = ""
                    )
                    callColorViewapi(productColorpostString!!)

                }
            } )

            binding.colorpallet.setBackgroundColor(Color.parseColor("#"+productViewDetails[0].ColourCode))
            binding.colorpalletVal.setBackgroundColor(Color.parseColor("#"+productViewDetails[0].ColourCode))


        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun callColorViewapi(productcolorreqBody: ProductColorViewRequest) {

        try {
            homeViewModel.ProductColorDetailsData(
                productcolorreqBody,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeProductColorViewResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeProductColorViewResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is ProductColorViewDetailResponse) {
                                addressDetails.clear()
                                addressDetails.addAll(it.Addresses)
                                stockvalueDetails.clear()
                                stockvalueDetails.addAll(it.StockValues)
                                colorcodeDetails.clear()
                                colorcodeDetails.addAll(it.ColourCodes)
                                productimagesDetails.clear()
                                productimagesDetails.addAll(it.ProductImages)
                                colorNamesDetails.clear()
                                colorNamesDetails.addAll(it.ColourNames)

                                updateProductColorList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun updateProductColorList(){

        //Color Pallet
        imageUpdater(colorcodeDetails[0].ColourCode,productimagesDetails[0].ProductImage)
        //colorname = colorNamesDetails[0].ColourName
        if(colorNamesDetails[0].ColourName.isNullOrEmpty()){
            colorname = "Galliano"
        } else{
            colorname = colorNamesDetails[0].ColourName
        }

        //Address List
        binding.addressView.setOnClickListener{
            if(dropdown.equals(false)){
                dropdown = true
                binding.addressCard.visibility = View.VISIBLE
            } else if (dropdown.equals(true)){
                dropdown = false
                binding.addressCard.visibility = View.GONE
            }
        }
        binding.addressRv.visibility = View.GONE
        binding.addressRv2.visibility = View.VISIBLE

        val addresslistAdapter2 = AddressAdapter2(this,utility!!)
        binding.addressRv2.adapter = addresslistAdapter2
        addresslistAdapter2.updateList(addressDetails)
        addresslistAdapter2.updateStockList(stockvalueDetails)

        addresslistAdapter2.setOnItemCLickListerner(object : AddressAdapter2.onItemClickListener{
            override fun onItemCLick(position: Int) {
                //Toast.makeText(this@ProductDetailActivity,addressDetails[position].Address,Toast.LENGTH_SHORT).show()
                //selected_address = addressDetails[position].Address
                if(addressDetails[position].Address.isNullOrEmpty()){
                    selected_address = "Gechibowli, Hyderabad"
                } else{
                    selected_address = addressDetails[position].Address
                }
                binding.address.text = addressDetails[position].Address
                binding.storeInfoProdcount.text = stockvalueDetails[position].Quantity.toString()
                //binding.qtyVal.text = stockvalueDetails[position].Quantity.toString()

                stock_val = stockvalueDetails[position].Quantity
                full_qtylist.clear()

                for(i in 1..stock_val) {
                    full_qtylist.add(i)
                }

                binding.addressCard.visibility = View.GONE

                if(stockvalueDetails[position].Quantity<=2){
                    binding.storeInfo1.setTextColor(Color.parseColor("#ff0000"))
                    binding.storeInfo3.setTextColor(Color.parseColor("#ff0000"))
                    binding.storeInfoProdcount.setTextColor(Color.parseColor("#ff0000"))
                } else{
                    binding.storeInfo1.setTextColor(Color.parseColor("#757575"))
                    binding.storeInfo3.setTextColor(Color.parseColor("#757575"))
                    binding.storeInfoProdcount.setTextColor(Color.parseColor("#757575"))
                }

                //Call colors api
                productColorpostString = ProductColorViewRequest(
                    productId = prod_id,
                    colourCode = "",
                    location = addressDetails[position].Address
                )
                callLocationViewapi(productColorpostString!!)

            }
        } )
        //Quantity Spinner
        if (binding.quantity != null) {
            val adapter = ArrayAdapter(this,
                android.R.layout.simple_spinner_item, full_qtylist)
            binding.quantity.adapter = adapter

            binding.quantity.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>,
                                            view: View, position: Int, id: Long) {
                    selected_qty = full_qtylist[position].toString()
                }
                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }
        //
//        binding.colorpalletView.setOnClickListener(){
//            binding.colorpalletLayout.visibility = View.VISIBLE
//
//            if(colorcodeDetails.size <=0){
//                binding.emptypallet.visibility =View.VISIBLE
//                binding.colorsViewConstrain.visibility = View.GONE
//            } else if(colorcodeDetails.size >0){
//                binding.colorsViewConstrain.visibility = View.VISIBLE
//                binding.colorViewCurrent.setOnClickListener {
//                    imageUpdater(productViewDetails[0].ColourCode,productViewDetails[0].ProductImage)
//                }
//                binding.emptypallet.visibility =View.GONE
//            }
//
//        }
//        val colorpalletAdapter2 = ColorPalletAdapter2(this,utility!!)
//        binding.colorRv.visibility = View.GONE
//        binding.colorRv2.visibility = View.VISIBLE
//        binding.colorRv2.adapter = colorpalletAdapter2
//        colorpalletAdapter2?.updateList(colorcodeDetails)
//
//        colorpalletAdapter2.setOnItemCLickListerner(object : ColorPalletAdapter2.onItemClickListener{
//            override fun onItemCLick(position: Int) {
//                //  Toast.makeText(this@ProductDetailActivity, productColorCodes[position].ProductColourCode, Toast.LENGTH_SHORT).show()
//                binding.colorpallet.setBackgroundColor(Color.parseColor("#"+colorcodeDetails[position].ColourCode))
//                binding.colorpalletLayout.visibility = View.GONE
//                imageUpdater(colorcodeDetails[position].ColourCode,productimagesDetails[position].ProductImage)
//
//            }
//        } )
        //

    }

    private fun callLocationViewapi(productcolorreqBody: ProductColorViewRequest) {

        try {
            homeViewModel.ProductColorDetailsData(
                productcolorreqBody,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeProductLocationViewResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeProductLocationViewResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is ProductColorViewDetailResponse) {
                                colorcodeLocationDetails.clear()
                                colorcodeLocationDetails.addAll(it.ColourCodes)

                                updateProductLocationList()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun updateProductLocationList(){

        binding.colorpalletView.setOnClickListener(){
            binding.colorpalletLayout.visibility = View.VISIBLE

            if(colorcodeLocationDetails.size <=0){
                binding.emptypallet.visibility =View.VISIBLE
                binding.colorsViewConstrain.visibility = View.GONE
            } else if(colorcodeLocationDetails.size >0){
                binding.colorsViewConstrain.visibility = View.VISIBLE
                binding.colorViewCurrent.setOnClickListener {
                    imageUpdater(productViewDetails[0].ColourCode,productViewDetails[0].ProductImage)
                }
                binding.emptypallet.visibility =View.GONE
            }

        }
        val colorpalletAdapter2 = ColorPalletAdapter2(this,utility!!)
        binding.colorRv.visibility = View.GONE
        binding.colorRv2.visibility = View.VISIBLE
        binding.colorRv2.adapter = colorpalletAdapter2
        colorpalletAdapter2?.updateList(colorcodeLocationDetails)

        colorpalletAdapter2.setOnItemCLickListerner(object : ColorPalletAdapter2.onItemClickListener{
            override fun onItemCLick(position: Int) {
                //  Toast.makeText(this@ProductDetailActivity, productColorCodes[position].ProductColourCode, Toast.LENGTH_SHORT).show()
                binding.colorpallet.setBackgroundColor(Color.parseColor("#"+colorcodeDetails[position].ColourCode))
                //colorcode = colorcodeDetails[position].ColourCode
                if(colorcodeDetails[position].ColourCode.isNullOrEmpty()){
                    colorcode = "D6A318"
                } else{
                    colorcode = colorcodeDetails[position].ColourCode
                }
                //colorname = colorNamesDetails[position].ColourName
                if(colorNamesDetails[position].ColourName.isNullOrEmpty()){
                    colorname = "Galliano"
                } else{
                    colorname = colorNamesDetails[position].ColourName
                }
                binding.colorpalletLayout.visibility = View.GONE
                imageUpdater(colorcodeDetails[position].ColourCode,productimagesDetails[position].ProductImage)

            }
        } )


    }

    private fun callAddtoCartapi(addtocartReq: AddtoCartRequest) {

        try {
            homeViewModel.AddtoCartData(
                addtocartReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeAddtoCartResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeAddtoCartResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is AddtoCartResponse) {
                                //addtocartAttribute.addAll(listOf(it.attributes))
                                //cartproductId = it.Id
                                comp_Message = it.Message
                                if(comp_Message.equals("The product is out of stock.")){
                                    Toast.makeText(this,"This product is out of stock!",Toast.LENGTH_SHORT).show()
                                } else {
                                    //cartProductIDList.add(cartproductId)
                                    var cartval : Int = 0
                                    cartval = AppConstants.cartItemCount + 1
                                    //Toast.makeText(this,"The product is now added to the cart.",Toast.LENGTH_SHORT).show()
                                    binding.cartCount.text = cartval.toString()
                                    binding.addtocart.visibility = View.GONE
                                    binding.addedcartlayout.visibility = View.VISIBLE
                                }

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun callAddtoCompareapi(addtocompareReq: AddtoCompareRequest) {

        try {
            homeViewModel.AddtoComapreData(
                addtocompareReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeAddtoCompareResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeAddtoCompareResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is AddtoCompareResponse) {
//                                messageList.add(it.message)
                                comp_Message = it.Message
                                //Toast.makeText(this,it.Message,Toast.LENGTH_SHORT).show()
                                binding.alertView.visibility = View.VISIBLE
                                binding.alertCard.visibility = View.VISIBLE
                                binding.alertMsg.text = comp_Message

                                if(comp_Message.equals("This product is already added to the comparison list.")){
                                    binding.okBtn.visibility = View.GONE
                                    binding.cancelBtn.setOnClickListener {
                                        binding.alertView.visibility = View.GONE
                                        binding.alertCard.visibility = View.GONE
                                    }

                                    binding.floatComparison.visibility= View.VISIBLE


                                } else if(comp_Message.equals("Product is added to the comparison list.")){
                                    binding.okBtn.setOnClickListener {
                                        binding.alertView.visibility = View.GONE
                                        binding.alertCard.visibility = View.GONE

                                        binding.addtocompareBtn.visibility = View.GONE
                                        binding.addedcomparelayout.visibility = View.VISIBLE

                                        binding.floatComparison.visibility= View.VISIBLE

                                    }
                                    binding.cancelBtn.setOnClickListener {
                                        binding.alertView.visibility = View.GONE
                                        binding.alertCard.visibility = View.GONE

                                        binding.addtocompareBtn.visibility = View.GONE
                                        binding.addedcomparelayout.visibility = View.VISIBLE
                                    }

                                    binding.floatComparison.visibility= View.VISIBLE


                                } else if(comp_Message.equals("Do you want to remove existing products?")){
                                    binding.okBtn.setOnClickListener {
                                        binding.alertView.visibility = View.GONE
                                        binding.alertCard.visibility = View.GONE
                                        //Call Remove comparison list api
                                        removecomparisonRequest = RemoveComparisonRequest(
                                            customerId = AppConstants.CustomerID
                                        )
                                        removeapiCall(removecomparisonRequest!!)

                                    }
                                    binding.cancelBtn.setOnClickListener {
                                        binding.alertView.visibility = View.GONE
                                        binding.alertCard.visibility = View.GONE
                                    }
                                }

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun removeapiCall(removeReq: RemoveComparisonRequest) {

        try {
            homeViewModel.RemoveComapreData(
                removeReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }


    private fun consumeRemoveCompareResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeRemoveCompareResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is RemoveComparisonResponse) {
//                                messageList.add(it.message)
                                comp_Message = it.Message
                                Toast.makeText(this,it.Message,Toast.LENGTH_SHORT).show()
                                binding.alertView.visibility = View.VISIBLE
                                binding.alertCard.visibility = View.VISIBLE
                                binding.alertMsg.text = comp_Message

                                 if(comp_Message.equals("Deleted Comparison List")){
                                    binding.okBtn.setOnClickListener {
                                        binding.alertView.visibility = View.GONE
                                        binding.alertCard.visibility = View.GONE

                                        //Call Add to compare api
                                        addtocompareRequest = AddtoCompareRequest(
                                            customerId = AppConstants.CustomerID,
                                            productId = prod_id,
                                            productCategoryId = productcategory_id,
                                            colourCode = colorcode,
                                            colourName = colorname,
                                            location = selected_address,
                                            productImage = selected_imageid
                                        )
                                        callAddtoCompareapi(addtocompareRequest!!)

                                    }
                                    binding.cancelBtn.setOnClickListener {
                                        binding.alertView.visibility = View.GONE
                                        binding.alertCard.visibility = View.GONE
                                    }
                                }

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun apiCall(cartprodReq: CartProductRequest) {

        try {
            homeViewModel.CartProductDetails(
                cartprodReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeCartProductData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeCartProductData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is CartProductResponse) {
                                cartProductdetails.addAll(it.ProductCartDetails!!)
                                AppConstants.cartItemCount = cartProductdetails.size
                                binding.cartCount.text = AppConstants.cartItemCount.toString()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    fun imageUpdater(selectedcolorcode: String, imgId : String){

        binding.colorpallet.setBackgroundColor(Color.parseColor("#"+selectedcolorcode))
        colorcode = selectedcolorcode
        binding.colorpalletLayout.visibility = View.GONE

        //Product View Details
        selected_imageid = imgId
        imageId = imgId
        image_url = AppConstants.BaseImageURL+imageId+"/VersionData"
        utility?.loadGlide(this,binding.productMainImg,image_url,0)

        utility?.loadGlide(this,binding.imageView1,image_url,0)
        utility?.loadGlide(this,binding.imageView2,image_url,0)
        utility?.loadGlide(this,binding.imageView3,image_url,0)
        utility?.loadGlide(this,binding.imageView4,image_url,0)


        //Load multiple images
//        utility?.loadGlide(this,binding.imageView1,image_url,0)
//        utility?.loadGlide(this,binding.imageView2,image_url,0)
//        utility?.loadGlide(this,binding.imageView3,image_url,0)
//        utility?.loadGlide(this,binding.imageView4,image_url,0)
    }

    private fun recentCustomerapiCall() {

        try {
            homeViewModel.recentCustomerDetails(
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeRecentCustomerData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeRecentCustomerData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is RecentLoginCustomerResponse) {
                                customerDetails.addAll(it.CustomerDetails!!)
                                updateRecentCustomerDetails()
                                //Toast.makeText(this, recentCustomerdetails?.get(0)?.Name.toString(),Toast.LENGTH_SHORT).show()

                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,apiResponse.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }

    private fun updateRecentCustomerDetails() {
        try {
            val recentCustomerModel = customerDetails[0]
            //recent_customer_name = recentCustomerdetails?.get(0)?.Name.toString()
            AppConstants.Customer_Name = recentCustomerModel!!.Name
            AppConstants.CustomerID = recentCustomerModel!!.Id
            AppConstants.cartItemCount = recentCustomerModel!!.CartListCount
            //Toast.makeText(this,AppConstants.Customer_Name,Toast.LENGTH_SHORT).show()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun wishlistCreationapiCall(wishlistCreationRequest: WishlistCreationRequest) {

        try {
            homeViewModel.WishlistCreationDetailsData(
                wishlistCreationRequest,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeWishlistCreationResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeWishlistCreationResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is WishlistCreationResponse) {
                                wishlist_id = it.Id
                                Toast.makeText(this,"Successfully added to Wishlist!",Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
        when (p0?.id) {
            R.id.like_toggle -> if(p1.equals(true)){
//                wishlistCreationRequest = WishlistCreationRequest(
//                    productId = prod_id,
//                    customerId = AppConstants.CustomerID
//                )
                //wishlistCreationapiCall(wishlistCreationRequest!!)
            } else -> if(p1.equals(false)){
//                wishlistRemoveRequest = WishlistRemoveRequest(
//                    wishlistId = wishlist_id
//                )
//                wishlistRemoveapiCall(wishlistRemoveRequest!!)
            }
            //displayToast(message = "Toggle Button Drawable State is Filled? $p1")
            //else -> displayToast(message = "Toggle Button State is checked? $p1")
        }
    }

    fun displayToast(context: Context = this, message: String, duration: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(context, message, duration).show()
    }

    private fun wishlistRemoveapiCall(wishlistRemoveRequest: RemoveWishlistRequest) {

        try {
            homeViewModel.RemoveWishlistData(
                wishlistRemoveRequest,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    private fun consumeRemoveWishlistResponse(response: ApiResponse<*>) {
        Log.e(BaseFragment.TAG, "consumeRemoveWishlistResponse()")
        try {
            when (response.apiStatus) {

                ApiStatus.LOADING -> {
                    showLoader()
                }

                ApiStatus.SUCCESS -> {
                    dismissLoader()
                    response?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is RemoveWishlistResponse) {
                                comp_Message = it.Message
                                Toast.makeText(this,it.Message,Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,response.error!!.message,Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e(BaseFragment.TAG, "consumeResponse() Exception - ${ex.message}")
        }

    }

    private fun apiCall(compareprodReq: ComparisonRequest) {

        try {
            homeViewModel.CompareProductDetails(
                compareprodReq,
                "Bearer " + AppConstants.ACCESS_TOKEN)
        } catch (ex: Exception) {
            ex.printStackTrace();
        }

    }

    fun ConsumeComparisonProductData(apiResponse: ApiResponse<*>) {
        Log.e(TAG, "ConsumeCartProductData()")
        try{
            when (apiResponse.apiStatus) {
                ApiStatus.LOADING -> showLoader()

                ApiStatus.SUCCESS ->{
                    dismissLoader()
                    apiResponse?.let { apiResponse ->
                        apiResponse.data?.let {
                            if (it is ComparisonResponse) {
                                compareProductdetails.addAll(it.ProductComparisonDetails)
                                //AppConstants.compareItemCount = compareProductdetails.size
                                if(compareProductdetails.size > 0){
                                    binding.floatComparison.visibility= View.VISIBLE
                                }
                            }
                        }
                    }
                }

                ApiStatus.ERROR -> {
                    dismissLoader()
                    Toast.makeText(this,apiResponse.error!!.message, Toast.LENGTH_SHORT).show()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace();
        }
    }
}



//Address Spinner
/*if (binding.address != null) {
    val adapter = ArrayAdapter(this,
        android.R.layout.simple_spinner_item, addressList)
    binding.address.adapter = adapter

    binding.address.onItemSelectedListener = object :
        AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>,
                                    view: View, position: Int, id: Long) {
            binding.qtyVal.text = qtyList[position].toString()
            binding.storeInfoProdcount.text = qtyList[position].toString()
            if(qtyList[position]<=2){
                binding.storeInfo1.setTextColor(Color.parseColor("#ff0000"))
                binding.storeInfo3.setTextColor(Color.parseColor("#ff0000"))
                binding.storeInfoProdcount.setTextColor(Color.parseColor("#ff0000"))
            } else{
                binding.storeInfo1.setTextColor(Color.parseColor("#757575"))
                binding.storeInfo3.setTextColor(Color.parseColor("#757575"))
                binding.storeInfoProdcount.setTextColor(Color.parseColor("#757575"))
            }

        }

        override fun onNothingSelected(parent: AdapterView<*>) {
            // write code to perform some action
        }
    }
}*/
